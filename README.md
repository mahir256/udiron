# Udiron
<pre>
A succession of words not amenable to division into metrical feet is called Prose.
Chronicle and Tale are its two varieties. Of these Chronicle, we are told,—
Is what is narrated by the Hero himself exclusively; the other by the Hero as well as by any other person.
The showing forth of one's own merits is not here, in view of his being a recorder of events that have actually occurred, a blemish.
This restriction, however, is not observed, in as much as there [in Ākhyāyikā] also other persons narrate.
That another person narrates or he himself does it—what kind of a ground for distinction is this?
</pre>
—Kāvyādarśa of Daņḍin, i. 23-25 (tr. S. K. Belvalkar)

## About

***The base system is currently being modified to increase code quality and documentation! Check out [this revision](https://gitlab.com/mahir256/udiron/-/tree/1d542daa14eef6a627c33e474c32b65a39e7b3d3) for the state of this repository prior to these modifications!***

Udiron
(from the Bengali pronunciation উদীরণ of the Sanskrit [उदीरण](http://www.sanskrit-linguistics.org/dcs/index.php?contents=lemma&IDWord=56551) "communicating, saying, speaking")
is a natural language generation system that uses Wikidata lexemes as primitives to construct syntax trees based on the Universal Dependencies (UD) annotation scheme.

(The author of this project is willing to relinquish the name Udiron to the set of renderers that will actually be in use on the Abstract Wikipedia, whether or not derived from what is developed here.)

## Setup

You should first [set up tfsl](https://gitlab.wikimedia.org/toolforge-repos/twofivesixlex/)--visit the "Setup" section of that library's README for further steps--before proceeding with the following.

[Clone this repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository):

```
git clone https://gitlab.com/mahir256/udiron
```

The cloned repository should now reside in some folder, which may vary depending on where you performed the clone. Let's call that folder `/path/to/udiron` (or `C:\path\to\udiron` on a Windows system).

Make sure the cloned repository ends up in your PYTHONPATH. The simplest way to do this, [for a Unix (Linux/Mac OS/BSD) system](https://stackoverflow.com/q/3402168/), is below, where the folder path `/path/to/udiron` must be substituted with the actual folder into which Udiron was cloned:

```
export PYTHONPATH=$PYTHONPATH:/path/to/udiron
```

([On Windows this process is a bit different.](https://stackoverflow.com/q/3701646))

## Use

[Find the auto-generated documentation of Udiron here](https://gitlab.com/mahir256/udiron/-/wikis/home).

## Licensing

All code herein is Apache 2.0 unless otherwise stated below.
