"""Holds constants for use with Norwegian.

Most constants here are currently from Bokmål,
on the assumption that the equivalent Nynorsk lexemes/senses can be readily obtained.
"""

from tfsl.interfaces import LSid

vaere_identity = LSid("L303822-S4")
vaere_quality = LSid("L303822-S3")
ha_possession = LSid("L303824-S2")

det_article = LSid("L656361-S1")
en_indefinite = LSid("L312231-S1")
det_determiner = LSid("L656359-S1")

det_subject = LSid("L656360-S1")
det_object = LSid("L656360-S2")

jeg_pronoun = LSid("L305843-S1")
du_pronoun = LSid("L305844-S1")
vi_pronoun = LSid("L305846-S1")
dere_pronoun = LSid("L305847-S1")
det_pronoun = LSid("L305851-S1")
en_impersonal = LSid("L594619-S1")

at_subordinator = LSid("L448782-S1")
saa_lenge_while = LSid("L1321725-S1")
saa_lenge_aslongas = LSid("L1321725-S2")
efter_after = LSid("L614530-S1")
foer_before = LSid("L631083-S1")

her_here = LSid("L448857-S1")

i_in = LSid("L448787-S1")
i_inside = LSid("L448787-S2")

linking_s = LSid("L589898-S1")
linking_e = LSid("L589899-S1")
