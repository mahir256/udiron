"""Holds functions for processing Catenae with the language set to Norwegian."""

from typing import TYPE_CHECKING

from tfsl import langs

from udiron.langs import register
from udiron.langs.mul.to_str import capitalize_first_token, surface_join_spaces

if TYPE_CHECKING:
    import udiron.base.interfaces as i
    from udiron import CatenaConfig, CatenaZipper


@register("DevelopForm", langs.nb_)
@register("DevelopForm", langs.nn_)
def develop_form_nb(catena_in: "CatenaZipper") -> "i.SelectFormsOutput":
    """In the absence of exactly one form matching the inflection set provided, modifies the inflection set and Catena configuration to yield an appropriate form.

    Args:
        catena_in: Catena from which to develop a form.

    Returns:
        Possibly modified inflection set and config from that Catena.
    """
    inflections, config = catena_in.get_inflections(), catena_in.get_config()
    # TODO: do something?
    return inflections, config


@register("SurfaceJoin", langs.nb_)
@register("SurfaceJoin", langs.nn_)
def surface_join_nb(tokens_in: "i.OutputEntryListIndexed", config: "CatenaConfig") -> "i.OutputEntryList":
    """Joins the tokens provided.

    Args:
        tokens_in: Initial list of OutputEntries with indices.
        config: Additional configuration.

    Returns:
        New joined OutputEntryList.
    """
    if tokens_in[0][0].components[0].language in [langs.nb_, langs.nn_]:
        tokens_in = capitalize_first_token(tokens_in)
    return surface_join_spaces(tokens_in)
