"""Holds functions for manipulating Norwegian syntax trees."""

import tfsl.interfaces as tfsli
from tfsl import langs

import udiron.base.constants as c
import udiron.base.utils as u
from udiron import C_, CatenaZipper, FunctionConfig
from udiron.base.interfaces import register_language_fallbacks
from udiron.langs.mul import simple_inflect_for_gender

register_language_fallbacks(langs.nn_, [langs.nb_])


def mark_with_conjunction(
    complement: CatenaZipper,
    conjunction: CatenaZipper,
    others: FunctionConfig,
) -> CatenaZipper:
    """Marks a complement with a (subordinating) conjunction.

    * ⓘ Q117978803 § 9 🗏 368

    Args:
        complement: Complement in question.
        conjunction: Conjunction in question.
        others: Additional function configuration.

    Returns:
        Complement with conjunction attached.
    """
    return complement.attach_leftmost(conjunction, c.subordinate_conjunction)


def mark_with_preposition(
    complement: CatenaZipper,
    preposition: CatenaZipper,
    others: FunctionConfig,
) -> CatenaZipper:
    """Marks a complement with a preposition.

    * ⓘ Q117978803 § 7.3.12 🗏 331
    * ⓘ Q117978803 § 7.3.19 🗏 334

    Args:
        complement: Complement in question.
        preposition: Preposition in question.
        others: Additional function configuration.

    Returns:
        Complement with preposition attached.
    """
    return complement.attach_leftmost(preposition, c.adposition)


def attach_subject_to_predicate(
        predicate: CatenaZipper,
        argument: CatenaZipper,
        rel: tfsli.Qid,
        others: FunctionConfig,
) -> CatenaZipper:
    """Attaches the provided argument to the predicate.

    * ⓘ Q117978803 § 10.3.1 🗏 401

    Args:
        predicate: Predicate in question.
        argument: Argument in question.
        rel: Syntactic relationship of the argument to the predicate.
        others: Information that could override this function's behavior, such as whether the argument should be focused.

    Returns:
        Predicate with the argument attached.
    """
    return predicate.attach_leftmost(argument, rel)


def attach_object_to_predicate(
        predicate: CatenaZipper,
        argument: CatenaZipper,
        rel: tfsli.Qid,
        others: FunctionConfig,
) -> CatenaZipper:
    """Attaches the provided argument to the predicate.

    * ⓘ Q117978803 § 10.3.4.2 🗏 407

    Args:
        predicate: Predicate in question.
        argument: Argument in question.
        rel: Syntactic relationship of the argument to the predicate.
        others: Information that could override this function's behavior, such as whether the argument should be focused.

    Returns:
        Predicate with the argument attached.
    """
    return predicate.attach_rightmost(argument, rel)


def attach_adverbial_to_predicate(
        predicate: CatenaZipper,
        argument: CatenaZipper,
        config: FunctionConfig,
) -> CatenaZipper:
    """Chooses an appropriate relation to use for attach_argument_to_predicate with respect to the adverbial argument.

    Args:
        predicate: Predicate in question.
        argument: Adverbial in question.
        config: Any provided configuration.

    Returns:
        Predicate with the adverbial attached.
    """
    return predicate.attach_rightmost(argument, c.adverbial)


def make_indefinite_instance(
    nominal: CatenaZipper,
    article: CatenaZipper,
    config: FunctionConfig,
) -> CatenaZipper:
    """Makes a classificatory nominal indefinite, usually by attaching en/ei/et.

    * ⓘ Q117978803 § 1.9 🗏 52

    Args:
        nominal: Nominal in question.
        article: Article to attach.
        config: Additional function configuration.

    Returns:
        Nominal with article attached.
    """
    article = simple_inflect_for_gender(article, nominal)
    return nominal.attach_leftmost(article, c.indefinite_article)


def attach_predicative_complement(
    subject: CatenaZipper,
    complement: CatenaZipper,
    linking_verb: CatenaZipper,
    config: FunctionConfig,
) -> CatenaZipper:
    """Attaches a subject to an attribute.

    * ⓘ Q117978803 § 10.3.5 🗏 408

    Args:
        subject: Subject in question.
        complement: Attribute in question.
        linking_verb: (Copular) verb used in the resulting sentence.
        config: Additional function configuration.

    Returns:
        Attribute with a subject linked to it.
    """
    complement = complement.attach_leftmost(linking_verb, c.copula)
    return complement.attach_leftmost(subject, c.subject)


def use_present_form(
    predicate: CatenaZipper,
    config: FunctionConfig,
) -> CatenaZipper:
    """Adjusts a predicate to use a simple present tense form of a verb.

    * ⓘ Q117978803 § 5.3.3 🗏 232

    Args:
        predicate: Predicate in question.
        config: Any provided configuration.

    Returns:
        Predicate with simple present tense form of a verb.
    """
    if predicate.has_rel(c.copula):
        copula = predicate.rel(c.copula)
        copula = copula.add_inflections([c.present])
        _, predicate = copula.pop()
        return predicate
    return predicate.add_inflections([c.present])


def use_preterite_form(
    predicate: CatenaZipper,
    config: FunctionConfig,
) -> CatenaZipper:
    """Adjusts a predicate to use a simple preterite form of a verb.

    * ⓘ Q117978803 § 5.3.5 🗏 238

    Args:
        predicate: Predicate in question.
        config: Any provided configuration.

    Returns:
        Predicate with simple present tense form of a verb.
    """
    if predicate.has_rel(c.copula):
        copula = predicate.rel(c.copula)
        copula = copula.add_inflections([c.preterite])
        _, predicate = copula.pop()
        return predicate
    return predicate.add_inflections([c.preterite])


def inflect_for_tense_aspect_mood(
        predicate: CatenaZipper,
        config: FunctionConfig,
) -> CatenaZipper:
    """Adds inflections to verbs and verb phrases.

    * ⓘ Q117978803 § 5 🗏 203

    Args:
        predicate: The verb or verb phrase to be inflected.
        config: Any provided configuration.

    Returns:
        The verb or verb phrase with inflections added.
    """
    # TODO: finish according to reference
    utterance_time = u.get_as_timestamp_or_fallback(config, C_.now)
    occurrence_time = u.get_as_timestamp_or_fallback(config, C_.occurrence_time, utterance_time)
    situation_time = u.get_as_timestamp_or_fallback(config, C_.reference_time, utterance_time)

    if occurrence_time.date() < situation_time.date():
        predicate = use_preterite_form(predicate, config)
    else:
        predicate = use_present_form(predicate, config)

    return predicate
