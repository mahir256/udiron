"""Holds constants for use with Finnish."""

from tfsl.interfaces import LSid

import udiron.base.constants as c

ei_connegative = LSid("L4545-S1")

nominal_cases = {
    c.nominative, c.genitive, c.partitive,
    c.inessive, c.elative, c.illative,
    c.adessive, c.ablative, c.allative,
    c.essive, c.translative, c.abessive,
    c.instructive, c.comitative, c.accusative,
}
