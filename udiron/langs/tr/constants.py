"""Holds constants for use with Turkish."""

from tfsl.interfaces import Lid, LSid, Qid

import udiron.base.constants as c

biz_pronoun = LSid("L294424-S1")
biz_pronoun_lexeme = Lid("L294424")
sen_pronoun = LSid("L294422-S1")
sen_pronoun_lexeme = Lid("L294422")
ben_pronoun = LSid("L294421-S2")
ben_pronoun_lexeme = Lid("L294421")
siz_pronoun = LSid("L294425-S1")
siz_pronoun_lexeme = Lid("L294425")

lar_plural = LSid("L941749-S1")
i_accusative = LSid("L1011033-S1")
a_dative = LSid("L1011040-S1")
in_genitive = LSid("L1011017-S1")
da_locative = LSid("L1149451-S1")
dan_ablative = LSid("L1149452-S1")
i_accusative_lexeme = Lid("L1011033")
a_dative_lexeme = Lid("L1011040")
in_genitive_lexeme = Lid("L1011017")
da_locative_lexeme = Lid("L1149451")
dan_ablative_lexeme = Lid("L1149452")
possessive_affixes = {
    (c.first_person, c.singular): LSid("L1217034-S1"),
    (c.second_person, c.singular): LSid("L1217035-S1"),
    (c.third_person, c.singular): LSid("L1010944-S1"),
    (c.first_person, c.plural): LSid("L1217036-S1"),
    (c.second_person, c.plural): LSid("L1217037-S1"),
    (c.third_person, c.plural): LSid("L1217038-S1"),
}

case_markers = {i_accusative_lexeme, a_dative_lexeme, in_genitive_lexeme, da_locative_lexeme, dan_ablative_lexeme}
"""* ⓘ Q116446388 § 8.1.3 🗏 70"""

ar_aorist = LSid("L1151394-S1")
di_past = LSid("L941731-S1")
di_past_lexeme = Lid("L941731")
sa_conditional = LSid("L1158886-S1")
sa_conditional_lexeme = LSid("L1158886")
mis_past = LSid("L941739-S1")
yor_continuous = LSid("L1151564-S1")
makta_continuous = LSid("L1203295-S1")
acak_future = LSid("L1158885-S1")
a_optative = LSid("L1232561-S1")
a_optative_lexeme = Lid("L1232561")

inca_when = LSid("L1205685-S1")

group1 = {
    (c.first_person, c.singular): LSid("L1232527-S1"),
    (c.second_person, c.singular): LSid("L1232525-S1"),
    (c.first_person, c.plural): LSid("L1232526-S1"),
    (c.second_person, c.plural): LSid("L1151717-S1"),
}

group2 = {
    (c.first_person, c.singular): LSid("L1151562-S1"),
    (c.second_person, c.singular): LSid("L1232522-S1"),
    (c.first_person, c.plural): LSid("L1232521-S1"),
    (c.second_person, c.plural): LSid("L1232523-S1"),
}

group3 = {
    (c.first_person, c.singular): LSid("L1202947-S1"),
    (c.second_person, c.singular): LSid("L1232522-S1"),
    (c.first_person, c.plural): LSid("L1232530-S1"),
    (c.second_person, c.plural): LSid("L1232523-S1"),
}

group4 = {
    (c.third_person, c.singular): LSid("L1202954-S1"),
    (c.second_person, c.plural): LSid("L1232517-S1"),
    (c.third_person, c.plural): LSid("L1232518-S1"),
}
group4_2p_alt = LSid("L1232516-S1")  # TODO: what to do with this alternation?

group1_person_preceders = {di_past_lexeme, sa_conditional_lexeme}
"""* ⓘ Q116446388 § 8.4 🗏 88"""
voiceless_consonants = {"p", "t", "k", "f", "h", "ç", "ş", "s"}
"""* ⓘ Q116446388 § 6.1.2 🗏 44"""

front_rounded_vowels = {"ü", "ö", "ô"}
front_unrounded_vowels = {"i", "e"}
back_unrounded_vowels = {"a", "ı", "â"}
back_rounded_vowels = {"u", "o", "û"}
explicitly_palatal_vowels = {"â", "û", "ô"}
front_vowels = front_unrounded_vowels | front_rounded_vowels
back_vowels = back_unrounded_vowels | back_rounded_vowels
vowels = {
    c.front_rounded_vowel: front_rounded_vowels,
    c.front_unrounded_vowel: front_unrounded_vowels,
    c.back_unrounded_vowel: back_unrounded_vowels,
    c.back_rounded_vowel: back_rounded_vowels,
    c.front_vowel: front_vowels,
    c.back_vowel: back_vowels,
}
vowel_set = front_vowels | back_vowels
vowel_types = {c.front_vowel, c.front_rounded_vowel, c.front_unrounded_vowel, c.back_vowel, c.back_rounded_vowel, c.back_unrounded_vowel}

consonants_tr = {"B", "C", "Ç", "D", "F", "G", "Ğ", "H", "J", "K", "L", "M", "N", "P", "R", "S", "Ş", "T", "V", "Y", "Z"}
consonants_ota = {"ب", "پ", "ت", "ث", "ج", "چ", "ح", "خ", "د", "ذ", "ر", "ز", "ژ", "س", "ش", "ص", "ض", "ط", "ظ", "ع", "غ", "ف", "ق", "ك", "گ", "ڭل", "م", "ن"}
uppercase_vowels_tr = {"A", "E", "I", "O", "U", "İ", "Ö", "Ü"}
lowercase_vowels_tr = {"a", "e", "i", "o", "u", "ı", "ü", "ö"}
vowels_ota = {"ی", "ا", "ه", "و"}
all_uppercase_characters_tr = consonants_tr | uppercase_vowels_tr
all_vowel_characters_tr = uppercase_vowels_tr | lowercase_vowels_tr
all_consonant_characters_tr = consonants_tr | {c.lower() for c in consonants_tr}

su_water_lexeme = Lid("L8412")

marked_cases = {c.accusative, c.dative, c.locative, c.ablative, c.genitive}
"""* ⓘ Q116446388 § 14.3.3 🗏 173 ‟ Turkish has five case suffixes (8.1.3), which mark respectively the accusative, dative, locative, ablative and genitive cases.@en"""
irregular_datives = {ben_pronoun_lexeme, sen_pronoun_lexeme}
"""Nouns with irregular datives (that is, not just the noun + (y)A)."""
irregular_genitives = {ben_pronoun_lexeme, biz_pronoun_lexeme, su_water_lexeme}
"""Nouns with irregular genitives (that is, not just the noun + (n)In)."""

on_front = LSid("L1215393-S1")
arka_back = LSid("L1216115-S1")
ic_inside = LSid("L1159242-S1")
dis_outside = LSid("L1216116-S1")
ust_top = LSid("L1216117-S1")
alt_bottom = LSid("L1216227-S1")
yan_side = LSid("L1163281-S1")
karsi_opposite = LSid("L1216228-S1")
ara_space = LSid("L1160581-S1")
etraf_surroundings = LSid("L1216231-S1")
cevre_surroundings = LSid("L1216232-S1")
ote_beyond = LSid("L1216233-S1")

suffix_initial_sny_deletion = Qid("Q124077093")
palatalized_lateral = Qid("Q124150931")
palatalized_velar_stop = Qid("Q124150943")
