"""Holds functions for processing Catenae with the language set to Turkish."""

import re
from typing import NamedTuple, Optional, Sequence, Tuple

import tfsl.interfaces as tfsli
from tfsl import Language, LexemeFormLike, LexemeLike, Statement, langs

import udiron.base.constants as c
import udiron.base.interfaces as i
import udiron.langs.tr.constants as ctr
from udiron import CatenaConfig, CatenaZipper
from udiron.base.functionconfig import C_
from udiron.base.outputentry import OutputEntry, OutputEntryComponent
from udiron.langs import register
from udiron.langs.mul.to_str import (capitalize_first_token,
                                     join_compound_right, surface_join_spaces)

# Some useful constants for later.
# TODO: deal with words where spelling doesn't correspond with pronunciation
consonant_regex_tr = re.compile(r"[" + "".join(ctr.consonants_tr) + "".join(ctr.consonants_tr).lower() + r"]")
vowel_regex_tr = re.compile(r"[" + "".join(ctr.uppercase_vowels_tr) + "".join(ctr.lowercase_vowels_tr) + r"]")
syllable_regex_tr = re.compile(consonant_regex_tr.pattern + r"?" + vowel_regex_tr.pattern + consonant_regex_tr.pattern + r"{,2}")

# TODO: consider maintaining a hidden layer of tr representations when
#       stringifying ota text, so that form choices are consistent between the two?
consonant_regex_ota = re.compile(r"[" + "".join(ctr.consonants_ota) + r"]")
vowel_regex_ota = re.compile(r"[" + "".join(ctr.vowels_ota) + r"]")
syllable_regex_ota = re.compile(consonant_regex_ota.pattern + r"?" + vowel_regex_ota.pattern + r"?" + consonant_regex_ota.pattern + r"{,2}")


@register("DevelopForm", langs.tr_)
def develop_form_tr(catena_in: CatenaZipper) -> i.SelectFormsOutput:
    """In the absence of exactly one form matching the inflection set provided, modifies the inflection set and Catena configuration to yield an appropriate form.

    Args:
        catena_in: Catena from which to develop a form.

    Returns:
        Possibly modified inflection set and config from that Catena.
    """
    inflections, config = catena_in.get_inflections(), catena_in.get_config()
    if catena_in.get_category() in {c.noun, c.pronoun, c.proper_noun}:
        lexical_representation_forms = catena_in.get_forms([c.lexical_representation])
        tr_lemma = catena_in.get_lemma(langs.tr_)
        if lexical_representation_forms != [] and tr_lemma.text[-1] in {"L", "l"}:
            config = config.set(C_.wikibase_form, lexical_representation_forms[0].id or "")
        inflections = frozenset([c.nominative])
    return inflections, config


def ends_in_open_syllable_tr(surface: str) -> bool:
    """Checks for a coda in a final syllable.

    Args:
        surface: Surface representation of a morpheme.

    Returns:
        Whether the surface representation provided ends in a vowel.
    """
    return surface[-1] in ctr.vowel_set


def ends_in_voiceless_consonant_tr(surface: str) -> bool:
    """Checks the final consonant (if any) of a morpheme.

    Args:
        surface: Surface representation of a morpheme.

    Returns:
        Whether the surface representation provided ends in a voiceless consonant.
    """
    return surface[-1] in ctr.voiceless_consonants


def last_syllable_contains_vowel_type_tr(surface: str, vowel_type: tfsli.Qid) -> bool:
    """Checks the vowel quality of a final syllable.

    Args:
        surface: Surface representation of a morpheme in tr.
        vowel_type: Vowel type to check for.

    Returns:
        Whether the last syllable in that surface representation contains a vowel of a particular type.
    """
    if len(surface) > 0 and surface[-1] in ctr.vowel_set:
        return surface[-1] in ctr.vowels[vowel_type]
    elif len(surface) > 1 and surface[-2] in ctr.vowel_set:
        return surface[-2] in ctr.vowels[vowel_type]
    elif len(surface) > 2:
        return surface[-3] in ctr.vowels[vowel_type]
    return False


def is_monosyllable_tr(surface: str) -> bool:
    """Checks the number of syllables in a morpheme.

    Args:
        surface: Surface representation of a morpheme in tr.

    Returns:
        Whether that surface representation is exactly one syllable.
    """
    return syllable_regex_tr.fullmatch(surface) is not None


def observes_large_vowel_harmony_tr(surface: str) -> bool:
    """Checks for vowel harmony in a morpheme.

    Args:
        surface: Surface representation of a morpheme in tr.

    Returns:
        Whether that surface representation contains only front vowels or contains only back vowels.
    """
    vowel_quality = ""
    for char in surface:
        if char in ctr.front_vowels and vowel_quality == "":
            vowel_quality = "front"
        elif char in ctr.back_vowels and vowel_quality == "":
            vowel_quality = "back"
        elif (char in ctr.front_vowels and vowel_quality == "back") or (char in ctr.back_vowels and vowel_quality == "front"):
            return False
    return True


def ends_with_palatalized_lateral_tr(surface: str) -> bool:
    """Checks the nucleus and coda of a final syllable.

    Args:
        surface: Surface representation of a morpheme in tr.

    Returns:
        Whether that surface representation ends in a palatalized lateral consonant.
    """
    return surface[-1] in {"L", "l"} and (
        (surface[-2] in ctr.explicitly_palatal_vowels) or not observes_large_vowel_harmony_tr(surface)
    )


def ends_with_palatalized_velar_stop_tr(surface: str) -> bool:
    """Checks the nucleus and coda of a final syllable.

    Args:
        surface: Surface representation of a morpheme in tr.

    Returns:
        Whether that surface representation ends in a palatalized velar stop.
    """
    return surface[-1] in {"K", "G", "k", "g"} and (
        (surface[-2] in ctr.explicitly_palatal_vowels) or not observes_large_vowel_harmony_tr(surface)
    )


def all_forms_begin_with_consonant_tr(lexeme: LexemeLike) -> bool:
    """Checks the onsets of all forms of a lexeme.

    Args:
        lexeme: Lexeme in question.

    Returns:
        Whether every form of the lexeme in question begins with a consonant.
    """
    return all((form[langs.tr_].text[0] not in ctr.all_vowel_characters_tr) for form in lexeme.get_forms())


class MorphemeBoundaryHandlerTr(NamedTuple):
    """Finds the right form of a lexeme in tr to use given the string immediately preceding it and that string's components.

    Attributes:
        left_surface: Surface representation of the left OutputEntry.
        rightmost_left_component: Rightmost component in the left OutputEntry.
        leftmost_center_component: Leftmost component in the center OutputEntry.
        leftmost_right_component: Leftmost component in the right OutputEntry.
    """
    left_surface: str
    rightmost_left_component: OutputEntryComponent
    leftmost_center_component: OutputEntryComponent
    leftmost_right_component: Optional[OutputEntryComponent]

    @classmethod
    def new(
        cls,
        left_entry: OutputEntry,
        center_entry: OutputEntry,
        right_entry: OutputEntry,
    ) -> "MorphemeBoundaryHandlerTr":
        """Sets up the MorphemeBoundaryHandlerTr.

        Args:
            left_entry: Current left OutputEntry.
            center_entry: Current left OutputEntry.
            right_entry: Current left OutputEntry.

        Returns:
            New MorphemeBoundaryHandlerTr.

        Todo:
            what to do if the center entry has more than one component?
        """
        return MorphemeBoundaryHandlerTr(
            left_entry.surface,
            left_entry.components[-1],
            center_entry.components[0],
            None if len(right_entry.components) == 0 else right_entry.components[0],
        )


def check_p11951_has_parts(
        left_surface: str,
        feature_statement: Statement,
        rightmost_form: LexemeFormLike,
) -> bool:
    """Checks the 'has part' qualifiers on 'appears after phonological feature'.

    Args:
        left_surface: Surface representation of the left OutputEntry.
        feature_statement: P11951 statement in question.
        rightmost_form: Current form on the rightmost left component.

    Returns:
        Whether the rightmost form has the feature noted on the statement.
    """
    has_part_values = [_.get_itemvalue().get_qid() for _ in feature_statement[c.has_part]]
    all_conditions_met = True
    for part in has_part_values:
        if part in ctr.vowel_types:
            all_conditions_met = all_conditions_met and last_syllable_contains_vowel_type_tr(left_surface, part)
        elif part == c.voiceless_final_consonant:
            all_conditions_met = all_conditions_met and ends_in_voiceless_consonant_tr(left_surface)
        elif part == c.voiced_final_consonant:
            all_conditions_met = all_conditions_met and not ends_in_voiceless_consonant_tr(left_surface)
        elif part == ctr.suffix_initial_sny_deletion:
            all_conditions_met = all_conditions_met and rightmost_form.haswbstatement(c.has_part, ctr.suffix_initial_sny_deletion)
        elif part == ctr.palatalized_lateral:
            all_conditions_met = all_conditions_met and ends_with_palatalized_lateral_tr(left_surface)
        elif part == ctr.palatalized_velar_stop:
            all_conditions_met = all_conditions_met and ends_with_palatalized_velar_stop_tr(left_surface)
    return all_conditions_met


def check_p11951_does_not_have_parts(
        left_surface: str,
        feature_statement: Statement,
        rightmost_form: LexemeFormLike,
) -> bool:
    """Checks the 'does not have part' qualifiers on 'appears after phonological feature'.

    Args:
        left_surface: Surface representation of the left OutputEntry.
        feature_statement: P11951 statement in question.
        rightmost_form: Current form on the rightmost left component.

    Returns:
        Whether the rightmost form has the feature noted on the statement.
    """
    all_conditions_met = True
    does_not_have_part_values = [_.get_itemvalue().get_qid() for _ in feature_statement[c.does_not_have_part]]
    for not_part in does_not_have_part_values:
        if not_part == ctr.suffix_initial_sny_deletion:
            all_conditions_met = all_conditions_met and not rightmost_form.haswbstatement(c.has_part, ctr.suffix_initial_sny_deletion)
        elif not_part == ctr.palatalized_lateral:
            all_conditions_met = all_conditions_met and not ends_with_palatalized_lateral_tr(left_surface)
        elif not_part == ctr.palatalized_velar_stop:
            all_conditions_met = all_conditions_met and not ends_with_palatalized_velar_stop_tr(left_surface)
    return all_conditions_met


def examine_p11950_statements_tr(
        current_form: LexemeFormLike,
        leftmost_right_component: OutputEntryComponent,
) -> bool:
    """Checks the values of the 'appears before phonological feature' statements on the current form.

    Args:
        current_form: Form of a lexeme to check.
        leftmost_right_component: Leftmost component in the right OutputEntry.

    Returns:
        Whether the component has any feature on the current form's P11950 statements.

    Todo:
        find a better item for 'non-suffix'
    """
    all_conditions_met = True
    for before_feature_statement in current_form[c.before_feature]:
        before_feature = before_feature_statement.get_itemvalue().get_qid()
        if before_feature == c.suffix:
            all_conditions_met = all_conditions_met and leftmost_right_component.lexeme.category == c.suffix
        elif before_feature == c.word:
            all_conditions_met = all_conditions_met and leftmost_right_component.lexeme.category != c.suffix
    return all_conditions_met


def check_p11950_feature_conditions(
    leftmost_center_lexeme: LexemeLike,
    before_feature_statement: Statement,
) -> bool:
    """Checks the P11950 statements of a lexeme.

    Args:
        leftmost_center_lexeme: Leftmost lexeme in the center OutputEntry.
        before_feature_statement: P11950 statement being looked at here.

    Returns:
        Whether the value of the 'appears before phonological feature' statement is appropriate here.
    """
    before_feature = before_feature_statement.get_itemvalue().get_qid()
    if (before_feature == c.suffix and leftmost_center_lexeme.category != c.suffix) or (before_feature == c.word and leftmost_center_lexeme.category == c.suffix):
        return False
    return True


def check_p11950_has_parts(
    leftmost_center: str,
    before_feature_statement: Statement,
) -> bool:
    """Checks the 'has part' qualifiers on the 'appears before phonological feature' statement provided.

    Args:
        leftmost_center: Surface representation of the leftmost center component.
        before_feature_statement: P11950 statement to check.

    Returns:
        Whether the surface representation has the feature noted in the P11950 statement.
    """
    has_part_values = [_.get_itemvalue().get_qid() for _ in before_feature_statement[c.has_part]]
    all_conditions_met = True
    for part in has_part_values:
        if part == c.initial_consonant:
            all_conditions_met = all_conditions_met and leftmost_center[0] in ctr.all_consonant_characters_tr
        elif part == c.initial_vowel:
            all_conditions_met = all_conditions_met and leftmost_center[0] not in ctr.all_consonant_characters_tr
    return all_conditions_met


def select_left_form_tr(
    rightmost_left_lexeme: LexemeLike,
    leftmost_center_lexeme: LexemeLike,
    leftmost_center: str,
) -> LexemeFormLike:
    """Selects a form to use from the left noun lexeme based on the 'appears before phonological feature' statements on its forms.

    Args:
        rightmost_left_lexeme: Lexeme in the rightmost left component.
        leftmost_center_lexeme: Lexeme in the leftmost center component.
        leftmost_center: Surface representation of the leftmost center lexeme.

    Returns:
        Lexeme form to use.
    """
    for form in rightmost_left_lexeme.get_forms():
        for before_feature_statement in form[c.before_feature]:
            if check_p11950_feature_conditions(leftmost_center_lexeme, before_feature_statement) and check_p11950_has_parts(leftmost_center, before_feature_statement):
                return form
    return next(form for form in rightmost_left_lexeme.get_forms())


def final_k_to_soft_g_tr(left_token: str, right_token: str) -> str:
    """Softens a final 'k' to 'ğ' when followed by a vowel at a morpheme boundary.

    * ⓘ Q116446388 § 2.1(v) 🗏 16

    Args:
        left_token: Token before the morpheme boundary.
        right_token: Token after the morpheme boundary.

    Returns:
        Left token possibly with a softened final consonant.
    """
    if left_token[-1] == "k" and right_token[0] in ctr.vowel_set:
        left_token = left_token[:-1] + "ğ"
    return left_token


def proper_noun_apostrophe_if_needed(left_token: str, rightmost_left_component: OutputEntryComponent) -> str:
    """Attaches an apostrophe to the end of the left token if it comes from a proper noun.

    * ⓘ Q116446388 🗏 xxxix

    Args:
        left_token: Surface representation of the left token.
        rightmost_left_component: Rightmost component in the left OutputEntry.

    Returns:
        Left token surface representation possibly with an apostrophe added.
    """
    if rightmost_left_component.lexeme.category == c.proper_noun:
        left_token = left_token + "'"
    return left_token


def assemble_component_tr(
    left_surface: str,
    current_form: LexemeFormLike,
    rightmost_left_component: OutputEntryComponent,
    leftmost_center_component: OutputEntryComponent,
) -> Tuple[OutputEntryComponent, str]:
    """Produces a new rightmost left component and surface form for the current OutputEntry.

    Args:
        left_surface: Surface representation of the left component.
        current_form: Form to find a representation for.
        rightmost_left_component: Rightmost component in the left OutputEntry.
        leftmost_center_component: Leftmost component in the center OutputEntry.

    Returns:
        Selection of a component and its surface representation.
    """
    right_token = current_form[langs.tr_].text

    if rightmost_left_component.lexeme.category != c.suffix:
        # TODO: identify a circumstance when the left lexeme comes from an OutputEntry with multiple components
        left_form = select_left_form_tr(rightmost_left_component.lexeme, leftmost_center_component.lexeme, right_token)
        new_rightmost_component = rightmost_left_component.clone(form=left_form)
        left_token = left_form[langs.tr_].text
        if left_surface[0] in ctr.all_uppercase_characters_tr:
            left_token = left_token[0].upper() + left_token[1:]
    else:
        new_rightmost_component = rightmost_left_component
        left_token = left_surface

    left_token = final_k_to_soft_g_tr(left_token, right_token)

    left_token = proper_noun_apostrophe_if_needed(left_token, rightmost_left_component)

    return new_rightmost_component, left_token + right_token + " "


def verify_p11951_statement_qualifiers_tr(
        handler: MorphemeBoundaryHandlerTr,
        left_surface: str,
        current_form: LexemeFormLike,
        feature_statement: Statement,
) -> Tuple[OutputEntryComponent, str]:
    """Checks the qualifiers of the P11951 statement provided and, if they are appropriate, adds the provided left surface form to a new output component.

    Args:
        handler: Container of OutputEntryComponents.
        left_surface: Surface representation of the left component.
        current_form: Form to find a representation for.
        feature_statement: P11951 statement to check qualifiers for.

    Returns:
        Selection of a component and its surface representation.
    """
    _, rightmost_left_component, leftmost_center_component, leftmost_right_component = handler
    rightmost_form = rightmost_left_component.form

    all_conditions_met = check_p11951_has_parts(left_surface, feature_statement, rightmost_form)
    all_conditions_met = all_conditions_met and check_p11951_does_not_have_parts(left_surface, feature_statement, rightmost_form)
    if leftmost_right_component:
        all_conditions_met = all_conditions_met and examine_p11950_statements_tr(current_form, leftmost_right_component)

    if all_conditions_met:
        return assemble_component_tr(left_surface, current_form, rightmost_left_component, leftmost_center_component)
    return rightmost_left_component, ""


def examine_p11953_statements_tr(
        handler: MorphemeBoundaryHandlerTr,
        target_forms: Sequence[LexemeFormLike],
) -> Tuple[Optional[OutputEntryComponent], str]:
    """Checks the "appears after lexeme form" statements in the list of target forms.

    Args:
        handler: Container of OutputEntryComponents.
        target_forms: Forms to check for P11953.

    Returns:
        Selection of a component and its surface representation.
    """
    left_surface, rightmost_left_component, _, _ = handler
    for form in target_forms:
        for after_form_statement in form[c.after_form]:
            target_form = after_form_statement.get_itemvalue().get_lfid()
            if target_form == rightmost_left_component.form.id:
                return rightmost_left_component, left_surface.rstrip(" ") + form[langs.tr_].text + " "
    return None, ""


def check_p11951_feature_conditions(
        left_surface: str,
        after_feature_statement: Statement,
) -> bool:
    """Checks the conditions associated with a particular value of "appears after phonological feature".

    Args:
        left_surface: Surface representation of the left component.
        after_feature_statement: P11951 statement to check.

    Returns:
        Whether the surface representation satisfies the P11951 condition given.
    """
    after_feature = after_feature_statement.get_itemvalue().get_qid()
    if after_feature == c.syllable:
        return True
    elif after_feature == c.open_syllable:
        return ends_in_open_syllable_tr(left_surface)
    elif after_feature == c.closed_syllable:
        return not ends_in_open_syllable_tr(left_surface)
    elif after_feature == c.monosyllabic_word:
        return is_monosyllable_tr(left_surface)
    elif after_feature == c.polysyllable:
        return not is_monosyllable_tr(left_surface)
    return False


def examine_p11951_statements_tr(
        handler: MorphemeBoundaryHandlerTr,
        target_forms: Sequence[LexemeFormLike],
) -> Tuple[Optional[OutputEntryComponent], str]:
    """Checks the "appears after phonological feature" statements in the list of target forms.

    Args:
        handler: Container of OutputEntryComponents.
        target_forms: Forms to check for P11951.

    Returns:
        Selection of a component and its surface representation.
    """
    left_surface, _, _, _ = handler
    left_surface = left_surface.rstrip(" ")
    for form in target_forms:
        for after_feature_statement in form[c.after_feature]:
            if check_p11951_feature_conditions(left_surface, after_feature_statement):
                returned_component, possible_form = verify_p11951_statement_qualifiers_tr(
                    handler, left_surface, form, after_feature_statement,
                )
                if possible_form != "":
                    return returned_component, possible_form
    return None, ""


def pick_center_component_tr(handler: MorphemeBoundaryHandlerTr) -> Tuple[OutputEntryComponent, str]:
    """Selects a form for a component given information about what's to its left and what's to its right.

    Args:
        handler: Container of OutputEntryComponents.

    Returns:
        Selection of a center component and the surface representation of that component's form.
    """
    left_surface, rightmost_left_component, leftmost_center_component, _ = handler
    target_forms = leftmost_center_component.lexeme.get_forms(leftmost_center_component.inflections)

    output_component, output_surface = examine_p11953_statements_tr(handler, target_forms)
    if output_component is not None:
        return output_component, output_surface

    output_component, output_surface = examine_p11951_statements_tr(handler, target_forms)
    if output_component is not None:
        return output_component, output_surface

    if len(target_forms) == 1:
        return rightmost_left_component, left_surface.rstrip(" ") + target_forms[0][langs.tr_].text + " "
    return rightmost_left_component, left_surface


def vh_left(
        left_entry: Optional[OutputEntry],
        center_entry: OutputEntry,
        right_entry: Optional[OutputEntry],
) -> Tuple[OutputEntry, OutputEntry, OutputEntry]:
    """Contracts certain combinations of morphemes where a particular form to use of one morpheme depends on the morpheme immediately preceding it.

    Args:
        left_entry: Current left OutputEntry.
        center_entry: Current center OutputEntry.
        right_entry: Current right OutputEntry.

    Returns:
        OutputEntries reflecting any applications of affixation vowel harmony.
    """
    new_right_entry = right_entry or OutputEntry.new()
    if left_entry is None:
        return OutputEntry.new(), center_entry, new_right_entry
    _, left_components, _, _ = left_entry
    _, center_components, _, _ = center_entry

    # TODO: reinsert ota logic
    handler = MorphemeBoundaryHandlerTr.new(left_entry, center_entry, new_right_entry)
    new_rightmost_left_component, new_left_surface = pick_center_component_tr(handler)

    new_left_components = left_components[:-1] + [new_rightmost_left_component]
    new_left_components = new_left_components + center_components

    new_center_output_entry = OutputEntry(new_left_surface, new_left_components)
    return OutputEntry.new(), new_center_output_entry, new_right_entry


@register("CollectFormsHooks", langs.tr_)
def set_hooks_tr(form_text: str, language: Language, config: CatenaConfig) -> Tuple[Optional[i.SurfaceTransformHook], CatenaConfig]:
    """Sets hooks used when performing surface transformations.

    Args:
        form_text: Surface text of the current OutputEntry.
        language: Language being rendered into.
        config: Additional function configuration.

    Returns:
        Hook to be run during the surface transformation step and modified Catena configuration.
    """
    transform_hook = None
    if (compound_setting := config.get(C_.compounding)) is not None:
        if compound_setting == c.vowel_harmony:
            transform_hook = vh_left
        elif compound_setting == c.forwards:
            transform_hook = join_compound_right
    return transform_hook, CatenaConfig.new()


@register("SurfaceJoin", langs.tr_)
def surface_join_tr(tokens_in: i.OutputEntryListIndexed, config: CatenaConfig) -> i.OutputEntryList:
    """Joins the tokens provided.

    Args:
        tokens_in: Initial list of OutputEntries with indices.
        config: Additional configuration.

    Returns:
        New joined OutputEntryList.
    """
    if tokens_in[0][0].components[0].language == langs.tr_:
        tokens_in = capitalize_first_token(tokens_in)
    return surface_join_spaces(tokens_in)
