"""Holds functions for manipulating Turkish syntax trees."""

from typing import TYPE_CHECKING, Optional, Tuple

import tfsl.interfaces as tfsli
from tfsl import Language, langs

import udiron.base.constants as c
import udiron.base.interfaces as i
import udiron.base.utils as u
import udiron.langs.mul
import udiron.langs.tr.constants as ctr
import udiron.langs.tr.to_str
from udiron.base.functionconfig import C_
from udiron.base.interfaces import register_language_fallbacks
from udiron.langs.mul import zip_language

if TYPE_CHECKING:
    from udiron import CatenaZipper, FunctionConfig

register_language_fallbacks(langs.ota_, [langs.tr_])


def attach_suffix(
        word: "CatenaZipper",
        suffix: "CatenaZipper",
        rel: tfsli.Qid,
) -> "CatenaZipper":
    """Attaches a (usually inflectional) suffix to the provided word or phrase.

    * ⓘ Q116446388 § 6 🗏 43 ‟ The main word formation process in Turkish is suffixation, the formation of a new word by attaching an affix to the right of a root.@en

    Args:
        word: Word or phrase to add suffix to.
        suffix: Desired added suffix.
        rel: Relationship of the suffix to its object.

    Returns:
        Word with the suffix attached.
    """
    return word.attach_rightmost(suffix, rel)


def add_adjectival_modifier(
        attribute: "CatenaZipper",
        noun: "CatenaZipper",
        rel: tfsli.Qid = c.adj_attribute,
) -> "CatenaZipper":
    """Attaches an attributive word or phrase to a noun or noun phrase.

    * ⓘ Q116446388 § 14.1 🗏 162 ‟ All modifiers in the Turkish noun phrase, however complex, precede the head.@en

    Args:
        attribute: Attributive word or phrase modifying a noun or noun phrase.
        noun: Noun or noun phrase being modified.
        rel: Syntactic relationship between the attribute and the noun.

    Returns:
        Noun with the attribute added.
    """
    return noun.attach_left_of_root(attribute, rel)


def get_suffix(
        suffix: i.Zippable,
        language: Language,
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Convenience function that adds a configuration specifying that the form of the suffix will need to change for phonological reasons.

    Args:
        suffix: Lexeme or sense ID for the suffix in question.
        language: Language to assign to the returned suffix.
        config: Any provided configuration.

    Returns:
        Suffix with the provided lexeme/sense ID.
    """
    desired_suffix = zip_language(suffix, language, config)
    if suffix in list(ctr.possessive_affixes.values()):
        desired_suffix = desired_suffix.add_to_config(C_.compounding(c.vowel_harmony))
    else:
        desired_suffix = desired_suffix.add_to_config(C_.compounding(c.vowel_harmony))
    return desired_suffix


def add_suffix(
        entity: "CatenaZipper",
        suffix: i.Zippable,
        config: "FunctionConfig",
        rel: tfsli.Qid = c.inflectional_suffix,
) -> "CatenaZipper":
    """Convenience function that attaches a suffix to an entity.

    Args:
        entity: Word or phrase to which a suffix will be attached.
        suffix: Lexeme or sense ID for the suffix in question.
        config: Any provided configuration.
        rel: Relationship of the suffix to the entity it is attached to.

    Returns:
        Word or phrase with the suffix attached.
    """
    return attach_suffix(entity, get_suffix(suffix, entity.get_language(), config), rel)


def mark_with_dative(
        noun: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Marks the provided noun or noun phrase with the dative suffix.

    * ⓘ Q116446388 § 8.1.3 🗏 70

    Args:
        noun: Noun or noun phrase in question.
        config: Any provided configuration.

    Returns:
        Noun or noun phrase with the dative suffix.
    """
    if noun.lexeme_id() in ctr.irregular_datives:
        return noun.add_inflections([c.dative])
    else:
        return add_suffix(noun, ctr.a_dative, config, c.case_marker)


def is_dative_marked(
        entity: "CatenaZipper",
) -> bool:
    """Checks a case marking.

    Args:
        entity: Some noun or noun phrase.

    Returns:
        Whether the entity has a dative case marker attached.
    """
    try:
        return entity.rel(c.case_marker).lexeme_id() == ctr.a_dative_lexeme
    except StopIteration:
        return False


def mark_with_accusative(
        noun: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Marks the provided noun or noun phrase with the accusative suffix.

    * ⓘ Q116446388 § 8.1.3 🗏 70

    Args:
        noun: Noun or noun phrase in question.
        config: Any provided configuration.

    Returns:
        Noun or noun phrase with the dative suffix.
    """
    return add_suffix(noun, ctr.i_accusative, config, c.case_marker)


def is_accusative_marked(
        entity: "CatenaZipper",
) -> bool:
    """Checks a case marking.

    Args:
        entity: Some noun or noun phrase.

    Returns:
        Whether the entity has an accusative case marker attached.
    """
    try:
        return entity.rel(c.case_marker).lexeme_id() == ctr.i_accusative_lexeme
    except StopIteration:
        return False


def mark_with_ablative(
        noun: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Marks the provided noun or noun phrase with the ablative suffix.

    * ⓘ Q116446388 § 8.1.3 🗏 70

    Args:
        noun: Noun or noun phrase in question.
        config: Any provided configuration.

    Returns:
        Noun or noun phrase with the ablative suffix.
    """
    return add_suffix(noun, ctr.dan_ablative, config, c.case_marker)


def is_ablative_marked(
        entity: "CatenaZipper",
) -> bool:
    """Checks a case marking.

    Args:
        entity: Some noun or noun phrase.

    Returns:
        Whether the entity has an ablative case marker attached.
    """
    try:
        return entity.rel(c.case_marker).lexeme_id() == ctr.dan_ablative_lexeme
    except StopIteration:
        return False


def mark_with_locative(
        noun: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Marks the provided noun or noun phrase with the locative suffix.

    * ⓘ Q116446388 § 8.1.3 🗏 70

    Args:
        noun: Noun or noun phrase in question.
        config: Any provided configuration.

    Returns:
        Noun or noun phrase with the locative suffix.
    """
    return add_suffix(noun, ctr.da_locative, config, c.case_marker)


def is_locative_marked(
        entity: "CatenaZipper",
) -> bool:
    """Checks a case marking.

    Args:
        entity: Some noun or noun phrase.

    Returns:
        Whether the entity has a locative case marker attached.
    """
    try:
        return entity.rel(c.case_marker).lexeme_id() == ctr.da_locative_lexeme
    except StopIteration:
        return False


def mark_with_genitive(
        noun: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Marks the provided noun or noun phrase with the genitive suffix.

    * ⓘ Q116446388 § 8.1.3 🗏 70

    Args:
        noun: Noun or noun phrase in question.
        config: Any provided configuration.

    Returns:
        Noun or noun phrase with the genitive suffix.
    """
    if noun.lexeme_id() in ctr.irregular_genitives:
        return noun.add_inflections([c.genitive])
    else:
        return add_suffix(noun, ctr.in_genitive, config, c.case_marker)


def is_genitive_marked(
        entity: "CatenaZipper",
) -> bool:
    """Checks a case marking.

    Args:
        entity: Some noun or noun phrase.

    Returns:
        Whether the entity has a genitive case marker attached.
    """
    try:
        return entity.rel(c.case_marker).lexeme_id() == ctr.in_genitive_lexeme
    except StopIteration:
        return False


def remove_case_markings(
        noun: "CatenaZipper",
) -> "CatenaZipper":
    """Removes case markings (as well as information about them).

    It is intended to undo the effects of mark_with_{accusative,dative,genitive,ablative,locative} above.

    Args:
        noun: Noun or noun phrase to remove case markings from.

    Returns:
        Noun or noun phrase with case markings removed.
    """
    return noun.remove_inflections(ctr.marked_cases).detach_rel(c.case_marker)


def attach_argument_to_predicate(
        predicate: "CatenaZipper",
        argument: "CatenaZipper",
        rel: tfsli.Qid,
        others: "FunctionConfig",
) -> "CatenaZipper":
    """Attaches the provided argument to the predicate.

    * ⓘ Q116446388 § 23.1.1 🗏 389

    Args:
        predicate: Predicate in question.
        argument: Argument in question.
        rel: Syntactic relationship of the argument to the predicate.
        others: Information that could override this function's behavior, such as whether the argument should be focused.

    Returns:
        Predicate with the argument attached.

    Todo:
        fix positions of other arguments
    """
    if others.get(C_.focus) is not None:
        return predicate.attach_left_of_root(argument, rel)

    if rel == c.subject or rel == c.modal_adverb:
        return predicate.attach_leftmost(argument, rel)
    else:
        return predicate.attach_left_of_root(argument, rel)


def attach_adverbial_to_predicate(
        predicate: "CatenaZipper",
        argument: "CatenaZipper",
        others: "FunctionConfig",
) -> "CatenaZipper":
    """Chooses an appropriate relation to use for attach_argument_to_predicate with respect to the adverbial argument.

    Args:
        predicate: Predicate to be modified.
        argument: Adverbial to be attached.
        others: Additional function configuration.

    Returns:
        Predicate with adverbial attached in the right place.
    """
    if is_locative_marked(argument):
        return attach_argument_to_predicate(predicate, argument, c.stative_location_complement, others)
    elif is_dative_marked(argument):
        return attach_argument_to_predicate(predicate, argument, c.destination_location_complement, others)
    elif is_ablative_marked(argument):
        return attach_argument_to_predicate(predicate, argument, c.source_location_complement, others)
    return attach_argument_to_predicate(predicate, argument, c.location_complement, others)


def find_grammatical_person(
        subject: "CatenaZipper",
) -> tfsli.Qid:
    """Finds a grammatical person.

    Args:
        subject: Catena whose lexeme possibly contains 'grammatical person' statements.

    Returns:
        Qid for the first 'grammatical person' statement value, or 'third person' if no statement is found.
    """
    try:
        person_inflection = udiron.langs.mul.get_grammatical_person(subject)
    except StopIteration:
        return c.third_person
    else:
        return person_inflection


def inflect_for_person(
        predicate: "CatenaZipper",
        subject: "CatenaZipper",
) -> "CatenaZipper":
    """Adds a person inflection to the provided predicate according to the provided subject.

    Args:
        predicate: Predicate in question.
        subject: Subject in question.

    Returns:
        Predicate with a grammatical person inflection added.
    """
    return predicate.add_inflections([find_grammatical_person(subject)])


def find_grammatical_number(
        subject: "CatenaZipper",
) -> tfsli.Qid:
    """Finds a grammatical number.

    Args:
        subject: Catena whose lexeme possibly contains 'grammatical number' statements.

    Returns:
        Qid for the first 'grammatical number' statement value, or either 'singular' or 'plural' depending on the presence of suffix '-lar' or whether 'plural' is in the subject's inflection set.
    """
    try:
        person_inflection = udiron.langs.mul.get_grammatical_number(subject)
    except StopIteration:
        if c.plural in subject.get_inflections() or subject.has_rel(c.number_inflection):
            return c.plural
        return c.singular
    else:
        return person_inflection


def inflect_for_number(
        predicate: "CatenaZipper",
        subject: "CatenaZipper",
) -> "CatenaZipper":
    """Adds a number inflection to the provided predicate according to the provided subject.

    Args:
        predicate: Predicate in question.
        subject: Subject in question.

    Returns:
        Predicate with a grammatical number inflection added.
    """
    return predicate.add_inflections([find_grammatical_number(subject)])


def get_person_suffix(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> Optional["CatenaZipper"]:
    """Obtains a person suffix based on the final affix of the predicate to which it attaches.

    * ⓘ Q116446388 § 21.2.1 🗏 327

    Args:
        predicate: Predicate whose affixes are analyzed.
        config: Any provided configuration.

    Returns:
        Appropriate person suffix.
    """
    person, number = get_person_number_from_predicate(predicate)
    try:
        final_affix = predicate.rightmost()
    except IndexError:
        if predicate.has_inflections([c.imperative]):
            try:
                suffix_lexeme = ctr.group4[person, number]
            except KeyError:
                return None
        else:
            try:
                suffix_lexeme = ctr.group2[person, number]
            except KeyError:
                return None
    else:
        if final_affix.lexeme_id() in ctr.group1_person_preceders:
            try:
                suffix_lexeme = ctr.group1[person, number]
            except KeyError:
                return None
        elif final_affix.lexeme_id() == ctr.a_optative_lexeme:
            try:
                suffix_lexeme = ctr.group3[person, number]
            except KeyError:
                return None  # TODO: handle replacement of optative in third-person case
        else:
            try:
                suffix_lexeme = ctr.group2[person, number]
            except KeyError:
                return None
    return get_suffix(suffix_lexeme, predicate.get_language(), config)


def get_person_number_from_predicate(
        predicate: "CatenaZipper",
) -> Tuple[tfsli.Qid, tfsli.Qid]:
    """Retrieves the person and number inflections added by inflect_for_person and inflect_for_number to the provided predicate.

    Args:
        predicate: Predicate in question.

    Returns:
        Person inflection followed by number inflection.
    """
    try:
        person = next(infl for infl in predicate.get_inflections() if infl in c.three_person_set)
    except StopIteration:
        person = c.third_person
    try:
        number = next(infl for infl in predicate.get_inflections() if infl in c.two_number_set)
    except StopIteration:
        number = c.singular
    return person, number


def add_person_suffix(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds a person suffix to the predicate based on configuration information in the predicate.

    * ⓘ Q116446388 § 8.4 🗏 87

    Args:
        predicate: Verb or verb phrase with some inflection data on it.
        config: Any provided configuration.

    Returns:
        The same verb or verb phrase with a person suffix.
    """
    person_suffix = get_person_suffix(predicate, config)
    if person_suffix is None:
        return predicate
    else:
        return predicate.attach_rightmost(person_suffix, c.inflectional_suffix)


def form_perfective_past(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds the inflection -di to a predicate.

    * ⓘ Q116446388 § 21.2.1 🗏 327

    Args:
        predicate: Predicate in question.
        config: Configuration controlling this addition.

    Returns:
        Predicate with indications of past tense added.
    """
    # TODO: fix based on reference
    predicate = add_suffix(predicate, ctr.di_past, config)
    return predicate


def form_relative_future(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds the inflection -acak to a predicate.

    * ⓘ Q116446388 § 21.2.3 🗏 329

    Args:
        predicate: Predicate in question.
        config: Configuration controlling this addition.

    Returns:
        Predicate with indications of future tense added.
    """
    # TODO: fix based on reference
    predicate = add_suffix(predicate, ctr.acak_future, config)
    return predicate


def form_relative_imperfective(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds the inflection -makta to a predicate.

    * ⓘ Q116446388 § 21.3.2 🗏 332

    Args:
        predicate: Predicate in question.
        config: Configuration controlling this addition.

    Returns:
        Predicate with indications of imperfective aspect added.
    """
    # TODO: fix based on reference
    predicate = add_suffix(predicate, ctr.makta_continuous, config)
    return predicate


def inflect_for_tense_aspect_mood(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds inflections to verbs and verb phrases.

    * ⓘ Q116446388 § 21 🗏 325

    Args:
        predicate: The verb or verb phrase to be inflected.
        config: Any provided configuration.

    Returns:
        The verb or verb phrase with inflections added.
    """
    # TODO: finish according to reference
    utterance_time = u.get_as_timestamp_or_fallback(config, C_.now)
    occurrence_time = u.get_as_timestamp_or_fallback(config, C_.occurrence_time)
    situation_time = u.get_as_timestamp_or_fallback(config, C_.reference_time, utterance_time)

    if occurrence_time.date() < situation_time.date():
        predicate = form_perfective_past(predicate, config)
    elif occurrence_time.date() > situation_time.date():
        predicate = form_relative_future(predicate, config)
    else:
        predicate = form_relative_imperfective(predicate, config)

    return add_person_suffix(predicate, config)


def assemble_possessive_suffix(
        possessor: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Assembles a possessive suffix given the possessor to which it applies.

    * ⓘ Q116446388 § 8.1.2 🗏 69

    Args:
        possessor: Possessor in question.
        config: Any provided configuration.

    Returns:
        Appropriately inflected possessive affix.
    """
    appropriate_affix = ctr.possessive_affixes[find_grammatical_person(possessor), find_grammatical_number(possessor)]
    return get_suffix(appropriate_affix, possessor.get_language(), config)


def possessive_noun_phrase(
        complement: "CatenaZipper",
        base: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """* ⓘ Q116446388 § 17.3 🗏 249

    Args:
        complement: Catena indicating the possessor.
        base: Catena indicating the possessum.
        config: Configuration controlling this formation.

    Returns:
        Catena yielding a genitive relationship between possessor and possessum.
    """
    complement = mark_with_genitive(complement, config)
    base_possession = assemble_possessive_suffix(complement, config)
    base = base.attach_left_of_root(complement, c.complement)
    base = base.attach_right_of_root(base_possession, c.possessive_affix)
    return base


def possessive_marked_postpositional_phrase(
        complement: "CatenaZipper",
        base: "CatenaZipper",
        case: tfsli.Qid,
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Assembles a possessive-marked postpositional phrase.

    If the complement requires a genitive case marking, this marking
    needs to be added first before using this function.

    * ⓘ Q116446388 § 17.3 🗏 249

    Args:
        complement: Actual object of the postpositional phrase.
        base: Base noun which indicates the actual postpositional relation.
        case: Item ID representing the case to attach to the base.
        config: Any provided configuration.

    Returns:
        Assembled postpositional phrase.
    """
    base_possession = assemble_possessive_suffix(complement, config)
    base = base.attach_left_of_root(complement, c.complement)
    if case == c.ablative:
        base = mark_with_ablative(base, config)
    elif case == c.locative:
        base = mark_with_locative(base, config)
    elif case == c.dative:
        base = mark_with_dative(base, config)
    base = base.attach_right_of_root(base_possession, c.possessive_affix)
    return base
