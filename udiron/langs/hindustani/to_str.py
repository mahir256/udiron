"""Holds functions for processing Catenae with the language set to Hindustani."""

from typing import TYPE_CHECKING

from tfsl import langs

import udiron.base.constants as c
import udiron.base.interfaces as i
from udiron.langs import register
from udiron.langs.mul.to_str import surface_join_spaces

if TYPE_CHECKING:
    from udiron import CatenaConfig, CatenaZipper


@register("DevelopForm", langs.hi_)
def develop_form_hindustani(catena_in: "CatenaZipper") -> i.SelectFormsOutput:
    """In the absence of exactly one form matching the inflection set provided, modifies the inflection set and Catena configuration to yield an appropriate form.

    Args:
        catena_in: Catena from which to develop a form.

    Returns:
        Possibly modified inflection set and config from that Catena.
    """
    inflections, config = catena_in.get_inflections(), catena_in.get_config()
    if catena_in.get_category() in {c.noun, c.proper_noun} and not any(elem in inflections for elem in {c.direct_case, c.oblique}):
        inflections = inflections | {c.direct_case}
    return inflections, config


@register("SurfaceJoin", langs.hi_)
def surface_join_hindustani(tokens_in: i.OutputEntryListIndexed, config: "CatenaConfig") -> i.OutputEntryList:
    """Joins the tokens provided.

    Args:
        tokens_in: Initial list of OutputEntries with indices.
        config: Additional configuration.

    Returns:
        New joined OutputEntryList.
    """
    return surface_join_spaces(tokens_in)
