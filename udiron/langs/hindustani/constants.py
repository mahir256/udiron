"""Holds constants for use with Hindustani."""

from tfsl.interfaces import LSid

ka_genitive = LSid("L408913-S1")
