"""Holds functions for manipulating syntax trees in specific languages.

__init__.py in particular holds the mappings from languages to different functions
for different stages in the Catena rendering process.
"""

import importlib
from typing import TYPE_CHECKING, Any, Callable, List, MutableMapping, Tuple, TypeVar

from tfsl import Language
from typing_extensions import ParamSpec

import udiron.base.interfaces as i
from udiron.base.interfaces import get_language_fallbacks

if TYPE_CHECKING:
    from types import ModuleType

__target_str_mappings__ = {}
__target_str_mapping_types__ = {}

RegisterP = ParamSpec("RegisterP")
RegisterT = TypeVar("RegisterT")

__all__ = ["bn", "fi", "hindustani", "mul", "norwegian", "persian", "punjabi", "tr"]


def __getattr__(name: str) -> "ModuleType":
    """Imports a language's syntax handling functions on demand.

    Args:
        name: Submodule name containing the desired functions.

    Returns:
        Submodule itself.

    Raises:
        AttributeError: if the submodule is not listed in __all__.
    """
    if name in __all__:
        return importlib.import_module("." + name, __name__)
    raise AttributeError(f"module {__name__!r} has no attribute {name!r}")


def import_language(name: str) -> Tuple["ModuleType", "ModuleType"]:
    """Imports both a language's syntax handling and its stringization functionality.

    Args:
        name: Name of the subfolder to draw these modules from.

    Returns:
        Modules for a language's syntax handling and its stringization functionality.

    Todo:
        see if __getattr__ needs some changes
    """
    importlib.import_module(".mul.to_str", __name__)
    lang_module = importlib.import_module("." + name, __name__)
    to_str_module = importlib.import_module("." + name + ".to_str", __name__)
    return lang_module, to_str_module


def register_mapping(mapping_name: str, target_mapping: MutableMapping[Any, Any], target_mapping_types: List[type]) -> None:
    """Registers a mapping and the types of the indices into that mapping.

    Args:
        mapping_name: Name of the mapping in question.
        target_mapping: The mapping itself.
        target_mapping_types: Types of the values used to index into the mapping.
    """
    __target_str_mappings__[mapping_name] = target_mapping
    __target_str_mapping_types__[mapping_name] = target_mapping_types


def register(mapping_name: str, *indices: Any) -> Callable[[Callable[RegisterP, RegisterT]], Callable[RegisterP, RegisterT]]:
    """Registers a method for a particular language.

    Args:
        mapping_name: Name of the mapping to register a method for.
        indices: Indices into the mapping.

    Returns:
        Actual function performing the registration.

    Raises:
        AssertionError: if the number of indices provided doesn't match the number of indices needed for the object being registered.
    """
    target_mapping = __target_str_mappings__[mapping_name]
    target_mapping_types = __target_str_mapping_types__[mapping_name]
    if len(target_mapping_types) != len(indices):
        raise AssertionError("Registering a method with wrong number of indices")

    def output_method(method_in: Callable[RegisterP, RegisterT]) -> Callable[RegisterP, RegisterT]:
        """Performs the actual registration of a method.

        Args:
            method_in: Method to register.

        Returns:
            The same method.
        """
        current_mapping = target_mapping
        for current_index in indices[:-1]:
            current_mapping = current_mapping[current_index]
        current_mapping[indices[-1]] = method_in
        return method_in
    return output_method


__selectform__: i.SelectFormsMapping = {}
register_mapping("SelectForms", __selectform__, [Language])


def get_select_forms(lang: Language) -> i.SelectForms:
    """Obtains a function for selecting the form of a Catena in the provided language.

    Args:
        lang: Language to find a form selector for.

    Returns:
        Appropriate SelectForms.

    Raises:
        KeyError: if no SelectForms was found for the given language.
    """
    for language_to_try in get_language_fallbacks(lang):
        try:
            return __selectform__[language_to_try]
        except KeyError:
            continue
    raise KeyError(f"No SelectForms defined for language {lang}")


__developform__: i.DevelopFormMapping = {}
register_mapping("DevelopForm", __developform__, [Language])


def get_develop_form(lang: Language) -> i.DevelopForm:
    """Obtains a function for developing the form of a Catena in the provided language.

    Args:
        lang: Language to find a form developer for.

    Returns:
        Appropriate DevelopForm.

    Raises:
        KeyError: if no DevelopForm was found for the given language.
    """
    for language_to_try in get_language_fallbacks(lang):
        try:
            return __developform__[language_to_try]
        except KeyError:
            continue
    raise KeyError(f"No DevelopForm defined for language {lang}")


__collectformshooks__: i.CollectFormsHooksMapping = {}
register_mapping("CollectFormsHooks", __collectformshooks__, [Language])


def get_collect_forms_hooks(lang: Language) -> i.CollectFormsHooks:
    """Obtains a function for setting the hooks in the OutputEntry resulting from collecting a form in the provided language.

    Args:
        lang: Language to find a form collection hook setter for.

    Returns:
        Appropriate CollectFormsHooks.

    Raises:
        KeyError: if no CollectFormsHooks was found for the given language.
    """
    for language_to_try in get_language_fallbacks(lang):
        try:
            return __collectformshooks__[language_to_try]
        except KeyError:
            continue
    raise KeyError(f"No CollectFormsHooks defined for language {lang}")


__buildform__: i.CollectFormsMapping = {}
register_mapping("CollectForms", __buildform__, [Language])


def get_build_form(lang: Language) -> i.CollectForms:
    """Obtains a function for building the form of a Catena in the provided language.

    Args:
        lang: Language to find a form collector for.

    Returns:
        Appropriate CollectForms.

    Raises:
        KeyError: if no CollectForms was found for the given language.
    """
    for language_to_try in get_language_fallbacks(lang):
        try:
            return __buildform__[language_to_try]
        except KeyError:
            continue
    raise KeyError(f"No CollectForms defined for language {lang}")


__surfacejoin__: i.SurfaceJoinMapping = {}
register_mapping("SurfaceJoin", __surfacejoin__, [Language])


def get_surface_join(lang: Language) -> i.SurfaceJoin:
    """Obtains a function for joining a list of surface forms in the provided language.

    Args:
        lang: Language to find a surface join for.

    Returns:
        Appropriate SurfaceJoin.

    Raises:
        KeyError: if no SurfaceJoin was found for the given language.
    """
    for language_to_try in get_language_fallbacks(lang):
        try:
            return __surfacejoin__[language_to_try]
        except KeyError:
            continue
    raise KeyError(f"No SurfaceJoin defined for language {lang}")


__surfacetransform__: i.SurfaceTransformMapping = {}
register_mapping("SurfaceTransform", __surfacetransform__, [Language])


def get_surface_transform(lang: Language) -> i.SurfaceTransform:
    """Obtains a function for transforming a list of surface forms in the provided language.

    Args:
        lang: Language to find a surface transform for.

    Returns:
        Appropriate SurfaceTransform.

    Raises:
        KeyError: if no SurfaceTransform was found for the given language.
    """
    for language_to_try in get_language_fallbacks(lang):
        try:
            return __surfacetransform__[language_to_try]
        except KeyError:
            continue
    raise KeyError(f"No SurfaceTransform defined for language {lang}")
