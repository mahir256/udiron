"""Holds functions for manipulating Malay syntax trees."""

from tfsl import langs

from udiron.base.interfaces import register_language_fallbacks

register_language_fallbacks(langs.ms_arab_, [langs.ms_])
