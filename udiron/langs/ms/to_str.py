"""Holds functions for processing Catenae with the language set to Malay."""

from typing import TYPE_CHECKING

from tfsl import langs

import udiron.base.constants as c
import udiron.base.interfaces as i
from udiron.langs import register
from udiron.langs.mul.to_str import capitalize_first_token, surface_join_spaces

if TYPE_CHECKING:
    from udiron import CatenaConfig, CatenaZipper


@register("DevelopForm", langs.ms_)
def develop_form_ms(catena_in: "CatenaZipper") -> i.SelectFormsOutput:
    """In the absence of exactly one form matching the inflection set provided, modifies the inflection set and Catena configuration to yield an appropriate form.

    Args:
        catena_in: Catena from which to develop a form.

    Returns:
        Possibly modified inflection set and config from that Catena.

    Todo:
        do something here?
    """
    inflections, config = catena_in.get_inflections(), catena_in.get_config()
    return inflections, config


@register("SurfaceJoin", langs.ms_)
def surface_join_ms(tokens_in: i.OutputEntryListIndexed, config: "CatenaConfig") -> i.OutputEntryList:
    """Joins the tokens provided.

    Args:
        tokens_in: Initial list of OutputEntries with indices.
        config: Additional configuration.

    Returns:
        New joined OutputEntryList.
    """
    if tokens_in[0][0].components[0].language in [langs.ms_]:
        tokens_in = capitalize_first_token(tokens_in)
    return surface_join_spaces(tokens_in)
