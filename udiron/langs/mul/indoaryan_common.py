"""Holds functions common to the rendering of certain Indo-Aryan languages."""

from typing import TYPE_CHECKING

import tfsl.interfaces as tfsli

import udiron.base.constants as c

if TYPE_CHECKING:
    from udiron import CatenaZipper

unified_case_set = [c.direct_case, c.oblique, c.postpositional_case]


def change_noun_case(catena: "CatenaZipper", target_case: tfsli.Qid = c.direct_case) -> "CatenaZipper":
    """Adjusts the noun case of a Catena.

    Args:
        catena: Catena for a noun possibly inflected for case.
        target_case: New case to add to the noun.

    Returns:
        Catena reflecting the new noun case.
    """
    return catena.remove_inflections(unified_case_set).add_inflections([target_case])


def attach_postposition(catena: "CatenaZipper", postposition: "CatenaZipper", target_case: tfsli.Qid = c.direct_case) -> "CatenaZipper":
    """Attaches a postposition to the provided object.

    Args:
        catena: Object to be modified.
        postposition: Postposition modifying the object.
        target_case: Case to apply to the object.

    Returns:
        Object with postposition attached.
    """
    catena_out = catena.attach_rightmost(postposition, c.adposition)
    return change_noun_case(catena_out, target_case)


def genitive_complex_postposition(object_in: "CatenaZipper", ke: "CatenaZipper", postposition_base: "CatenaZipper", connector: "CatenaZipper") -> "CatenaZipper":
    """Attaches a complex postposition involving the genitive of a location noun.

    Args:
        object_in: Object of the complex postposition.
        ke: (Already oblique) genitive marker.
        postposition_base: Location noun.
        connector: Postposition connecting the base to the rest of the sentence.

    Returns:
        Combination of all four parts.
    """
    object_in = change_noun_case(object_in, c.oblique)
    object_in = object_in.attach_rightmost(ke, c.adposition)
    postposition_base = change_noun_case(postposition_base, c.oblique)
    postposition_base = postposition_base.attach_left_of_root(object_in, c.possessive)
    postposition_base = postposition_base.attach_rightmost(connector, c.adposition)
    return postposition_base
