"""Functions expected to be useful across languages.

At the moment only really deals with punctuation.
"""
from typing import Optional

import tfsl.interfaces as tfsli
from tfsl import Language, Lexeme, LexemeForm, LexemeSense, langs

import udiron.base.constants as c
import udiron.base.interfaces as i
import udiron.langs.mul.constants as cmul
from udiron.base.catenatracking import CatenaTrackingEntry
from udiron.base.catenazipper import CatenaZipper, zip_lexeme, zip_lid
from udiron.base.functionconfig import C_, FunctionConfig


def punct_lexeme(punct: str) -> Lexeme:
    """Creates a quasi-lexeme representing a punctuation symbol.

    Args:
        punct: String representing the punctuation in question.

    Returns:
        Quasi-lexeme representing that punctuation symbol.
    """
    return Lexeme(
        punct @ langs.mul_,
        langs.mul_,
        c.punctuation,
        forms=[LexemeForm(punct @ langs.mul_)],
        senses=[LexemeSense(punct @ langs.mul_)],
    )


def add_start_mark(catena: CatenaZipper, config_in: FunctionConfig) -> CatenaZipper:
    """Adds a start mark to a Catena.

    Defaults to a full stop in the absence of other parameters.

    Args:
        catena: Catena to prepend to.
        config_in: Additional function configuration.

    Returns:
        Catena with a mark before it.
    """
    endmark: str
    text_formatting = config_in.get(C_.text_formatting)
    punctuation_mark = config_in.get(C_.punctuation_mark)
    origin = config_in.get(C_.origin_of_speech)
    if not isinstance(origin, CatenaTrackingEntry):
        origin = None
    if text_formatting:
        endmark = cmul.endmark_styles[text_formatting]
    elif punctuation_mark:
        endmark = punctuation_mark
    else:
        endmark = "."
    endmark_catena: CatenaZipper = zip_lexeme(punct_lexeme(endmark), config=C_.compounding(c.backwards), origin=origin)
    return catena.attach_leftmost(endmark_catena, c.punctuation)


def add_end_mark(catena: CatenaZipper, config_in: FunctionConfig) -> CatenaZipper:
    """Adds an end mark to a Catena.

    Defaults to a full stop in the absence of other parameters.

    Args:
        catena: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with a mark after it.
    """
    endmark: str
    text_formatting = config_in.get(C_.text_formatting)
    punctuation_mark = config_in.get(C_.punctuation_mark)
    origin = config_in.get(C_.origin_of_speech)
    if not isinstance(origin, CatenaTrackingEntry):
        origin = None
    if text_formatting:
        endmark = cmul.endmark_styles[text_formatting]
    elif punctuation_mark:
        endmark = punctuation_mark
    else:
        endmark = "."
    endmark_catena: CatenaZipper = zip_lexeme(punct_lexeme(endmark), config=C_.compounding(c.backwards), origin=origin)
    return catena.attach_rightmost(endmark_catena, c.punctuation)


def add_brackets(catena: CatenaZipper, config_in: FunctionConfig) -> CatenaZipper:
    """Surrounds a Catena with a particular type of punctuation.

    Defaults to (halfwidth, straight, double) quotation marks in the absence of other parameters.

    Args:
        catena: Catena to surround
        config_in: Additional function configuration.

    Returns:
        Catena with punctuation surrounding it.
    """
    text_formatting = config_in.get(C_.text_formatting)
    opening_bracket = config_in.get(C_.opening_bracket)
    closing_bracket = config_in.get(C_.closing_bracket)
    origin = config_in.get(C_.origin_of_speech)
    if not isinstance(origin, CatenaTrackingEntry):
        origin = None

    left_bracket: str
    right_bracket: str
    if text_formatting:
        left_bracket, right_bracket = cmul.bracket_styles[text_formatting]
    elif opening_bracket and closing_bracket:
        left_bracket, right_bracket = opening_bracket, closing_bracket
    else:
        left_bracket, right_bracket = cmul.bracket_styles["straight_double"]

    bracket_left = zip_lexeme(punct_lexeme(left_bracket), config=C_.compounding(c.forwards), origin=origin)
    bracket_right = zip_lexeme(punct_lexeme(right_bracket), config=C_.compounding(c.backwards), origin=origin)
    new_catena = catena.attach_leftmost(bracket_left, c.punctuation)
    new_catena = new_catena.attach_rightmost(bracket_right, c.punctuation)
    return new_catena


base_fc_full_stop = C_.text_formatting("period")


def full_stop(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to end a Catena with a full stop.

    Args:
        catena_in: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with a full stop after it.
    """
    return add_end_mark(catena_in, base_fc_full_stop | config_in)


base_fc_arabic_full_stop = C_.text_formatting("arabic_full_stop")


def arabic_full_stop(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to end a Catena with an Arabic full stop.

    Args:
        catena_in: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with an Arabic full stop after it.
    """
    return add_end_mark(catena_in, base_fc_arabic_full_stop | config_in)


base_fc_comma = C_.text_formatting("comma")


def preceding_comma(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to start a Catena with a comma.

    Args:
        catena_in: Catena to prepend to.
        config_in: Additional function configuration.

    Returns:
        Catena with a comma before it.
    """
    return add_start_mark(catena_in, base_fc_comma | config_in)


def comma(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to end a Catena with a comma.

    Args:
        catena_in: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with a comma after it.
    """
    return add_end_mark(catena_in, base_fc_comma | config_in)


base_fc_danda = C_.text_formatting("danda")


def danda(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to end a Catena with an Indic full stop.

    Args:
        catena_in: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with an Indic full stop after it.
    """
    return add_end_mark(catena_in, base_fc_danda | config_in)


base_fc_exclamation_mark = C_.text_formatting("exclamation")


def exclamation_mark(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to end a Catena with a (halfwidth) exclamation mark.

    Args:
        catena_in: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with an exclamation mark after it.
    """
    return add_end_mark(catena_in, base_fc_exclamation_mark | config_in)


base_fc_question_mark = C_.text_formatting("question")


def question_mark(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to end a Catena with a (halfwidth) question mark.

    Args:
        catena_in: Catena to append to.
        config_in: Additional function configuration.

    Returns:
        Catena with a question mark after it.
    """
    return add_end_mark(catena_in, base_fc_question_mark | config_in)


base_fc_quotation_marks = C_.text_formatting("straight_double")


def quotation_marks(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to surround a Catena with (halfwidth, straight, double) quotation marks.

    Args:
        catena_in: Catena to surround.
        config_in: Additional function configuration.

    Returns:
        Catena with quotation marks around it.
    """
    return add_brackets(catena_in, base_fc_quotation_marks | config_in)


base_fc_parentheses = C_.text_formatting("period")


def parentheses(catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
    """Convenience function to surround a Catena with (halfwidth) parentheses.

    Args:
        catena_in: Catena to surround.
        config_in: Additional function configuration.

    Returns:
        Catena with parentheses around it.
    """
    return add_brackets(catena_in, base_fc_parentheses | config_in)


def get_grammatical_gender(catena_in: CatenaZipper) -> tfsli.Qid:
    """Gets a grammatical gender from the provided Catena.

    Args:
        catena_in: Catena to be checked.

    Returns:
        First value for the 'grammatical gender' property of the lexeme at the CatenaZipper's cursor.
    """
    return next(stmt.get_itemvalue().get_qid() for stmt in catena_in.get_from_id(c.grammatical_gender))


def get_grammatical_person(catena_in: CatenaZipper) -> tfsli.Qid:
    """Gets a grammatical person from the provided Catena.

    Args:
        catena_in: Catena to be checked.

    Returns:
        First value for the 'grammatical person' property of the lexeme at the CatenaZipper's cursor.
    """
    return next(stmt.get_itemvalue().get_qid() for stmt in catena_in.get_from_id(c.grammatical_person))


def get_grammatical_number(catena_in: CatenaZipper) -> tfsli.Qid:
    """Gets a grammatical number from the provided Catena.

    Args:
        catena_in: Catena to be checked.

    Returns:
        First value for the 'grammatical number' property of the lexeme at the CatenaZipper's cursor.
    """
    return next(stmt.get_itemvalue().get_qid() for stmt in catena_in.get_from_id(c.grammatical_number))


def simple_inflect_for_gender(catena_in: CatenaZipper, gendered_in: CatenaZipper, default: Optional[tfsli.Qid] = None) -> CatenaZipper:
    """Inflects the input Catena for agreement in grammatical gender with another Catena.

    Args:
        catena_in: Catena to be inflected.
        gendered_in: Entity whose gender should be discerned.
        default: Default gender to use if one cannot be found.

    Returns:
        Inflected Catena.

    Raises:
        ValueError: if there is no grammatical gender on gendered_in.
    """
    try:
        catena_in_gender = get_grammatical_gender(gendered_in)
    except StopIteration as exception:
        if default is None:
            raise ValueError from exception
        else:
            catena_in_gender = default
    return catena_in.add_inflections([catena_in_gender])


def zip_language(lid: i.Zippable, language: Language, config: Optional[FunctionConfig] = None) -> "CatenaZipper":
    """Takes the output from zip_lid(lid) and adjusts the language of that CatenaZipper to the provided language.

    Args:
        lid: Entity from which a lexeme ID can be obtained.
        language: Language of the CatenaZipper.
        config: Additional function configuration.

    Returns:
        New CatenaZipper.
    """
    if config is None:
        origin = None
    else:
        origin = config.get(C_.origin_of_speech)
    if not isinstance(origin, CatenaTrackingEntry):
        origin = None
    new_zipper = zip_lid(lid, origin=origin)
    new_zipper = new_zipper.modify(language=language)
    return new_zipper
