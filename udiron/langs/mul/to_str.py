"""Holds functions for processing Catenae with the language set to 'multiple languages'."""

import re
from typing import TYPE_CHECKING, Collection, Deque, Optional, Tuple

import tfsl.interfaces as tfsli
from tfsl import Language, LexemeForm, LexemeFormLike, LexemeLike
from tfsl.languages import langs

import udiron.base.constants as c
import udiron.base.interfaces as i
import udiron.base.utils as u
from udiron.base.catenaconfig import CatenaConfig
from udiron.base.functionconfig import C_
from udiron.base.outputentry import OutputEntry, OutputEntryComponent
from udiron.langs import get_collect_forms_hooks, get_develop_form, register

if TYPE_CHECKING:
    from udiron.base.catena import Catena
    from udiron.base.catenazipper import CatenaZipper


def capitalize_first_token(tokens_in: i.OutputEntryListIndexed) -> i.OutputEntryListIndexed:
    """Capitalizes the first token in the list of tokens.

    Args:
        tokens_in: Current list of tokens and indices.

    Returns:
        The same list but with the first token's surface representation capitalized.
    """
    first_token, first_index = tokens_in[0]
    capitalized_entry = first_token.clone(surface=first_token.surface[0].capitalize() + first_token.surface[1:])
    return [(capitalized_entry, first_index)] + tokens_in[1:]


def tack_on_space(token: OutputEntry) -> OutputEntry:
    """Adds a space to the provided token.

    Args:
        token: OutputEntry to be modified.

    Returns:
        OutputEntry with a space added to the end of the surface representation.
    """
    return token.clone(surface=token.surface + " ")


def surface_join_spaces(tokens_in: i.OutputEntryListIndexed) -> i.OutputEntryList:
    """Adds spaces to the end of each token.

    Args:
        tokens_in: Tokens to be modified.

    Returns:
        List of OutputEntries with spaces added to the end of their surface representations.
    """
    return [tack_on_space(entry) for entry, index in tokens_in]


@register("SurfaceTransform", langs.mul_)
def surface_transform_mul(string_in: i.OutputEntryList, config: CatenaConfig) -> i.OutputEntryList:
    """Transforms the current surface representation of the output string.

    Args:
        string_in: Initial list of SurfaceJoined OutputEntries.
        config: Additional function configuration.

    Returns:
        Appropriately transformed list of OutputEntries.
    """
    return surface_transform_process(string_in)


def surface_transform_process(string_in: i.OutputEntryList) -> i.OutputEntryList:
    """Processes a series of OutputEntries and runs their hooks.

    Args:
        string_in: Initial list of OutputEntries.

    Returns:
        New list of OutputEntries with hooks run.
    """
    input_string = Deque(string_in)
    output_string: Deque[OutputEntry] = Deque()

    first_iter = True
    while len(input_string) > 0:
        try:
            prev_token = output_string.pop()
        except IndexError:
            prev_token = None
        cur_token = input_string.popleft()
        try:
            next_token = input_string.popleft()
        except IndexError:
            next_token = None

        new_prev: Optional[OutputEntry]
        new_cur: OutputEntry
        new_next: Optional[OutputEntry]

        if cur_token.transform_hook is not None:
            new_prev, new_cur, new_next = cur_token.transform_hook(prev_token, cur_token, next_token)
        else:
            new_prev, new_cur, new_next = prev_token, cur_token, next_token
        new_prev_empty = new_prev.empty() if new_prev is not None else True
        new_next_empty = new_next.empty() if new_next is not None else True

        if first_iter and new_next_empty:
            if len(input_string) == 0 and len(output_string) == 0:
                output_string.append(new_cur)
                first_iter = False
            else:
                input_string.appendleft(new_cur)
        elif first_iter and new_next is not None and not new_next_empty:
            output_string.append(new_cur)
            first_iter = False
            input_string.appendleft(new_next)
        elif new_prev_empty and new_next_empty:
            if len(input_string) > 0:
                input_string.appendleft(new_cur)
            else:
                output_string.append(new_cur)
                first_iter = False
        elif new_prev_empty and new_next is not None and not new_next_empty:
            input_string.appendleft(new_next)
            output_string.append(new_cur)
        elif new_prev is not None and not new_prev_empty and new_next_empty:
            output_string.append(new_prev)
            first_iter = False
            if len(input_string) > 0:
                input_string.appendleft(new_cur)
            else:
                output_string.append(new_cur)
        elif (new_prev is not None and new_next is not None) and not (new_prev_empty or new_next_empty):
            output_string.append(new_prev)
            output_string.append(new_cur)
            first_iter = False
            input_string.appendleft(new_next)

    return [token for token in output_string if token is not None]


def obtain_form(lexeme: LexemeLike, language: Language, inflections: i.InflectionSet, config: CatenaConfig, exclusions: Optional[Collection[tfsli.Qid]] = None) -> LexemeFormLike:
    """Obtains a single lexeme form from a lexeme given other information about how to make this choice.

    Args:
        lexeme: Lexeme to draw a form from.
        language: Language being rendered into.
        inflections: Any inflections to take into consideration.
        config: Additional function configuration.
        exclusions: Any inflections to exclude from consideration.

    Returns:
        Single lexeme form.
    """
    obtained_form: LexemeFormLike
    if (chosen_form_text := config.get(C_.word_form)) is not None:
        obtained_form = LexemeForm(chosen_form_text @ language)
    elif (chosen_form_id := config.get(C_.wikibase_form)) is not None:
        chosen_form_id = tfsli.Fid(chosen_form_id)
        obtained_form = lexeme[chosen_form_id]
    else:
        form_list = lexeme.get_forms(inflections, exclusions)
        try:
            obtained_form = next(form for form in form_list)
        except StopIteration:
            try:
                obtained_form = lexeme.get_forms()[0]
            except IndexError:
                obtained_form = LexemeForm("[]" @ language)
    return obtained_form


@register("SelectForms", langs.mul_)
def select_forms_mul(catena_in: "CatenaZipper") -> i.SelectFormsOutput:
    """Default SelectForms step for all languages which do not define their own.

    Args:
        catena_in: Current Catena to select a form for.

    Returns:
        Inflection set and CatenaConfig adjusted towards the goal of one form ultimately being selected later.
    """
    inflections, config = catena_in.get_inflections(), catena_in.get_config()
    if (catena_language := catena_in.get_language()) == langs.mul_:
        return inflections, config
    no_inflections = len(inflections) == 0
    one_suitable_form = len(catena_in.get_forms(inflections)) == 1
    if no_inflections or not one_suitable_form:
        develop_form = get_develop_form(catena_language)
        return develop_form(catena_in)
    else:
        return inflections, config


hebr_finals_to_regulars = {"ך": "כ", "ם": "מ", "ן": "נ", "ף": "פ", "ץ": "צ"}


def definalize_hebr(surface: str) -> str:
    """Changes the final consonant of a Hebrew script string to its non-final form.

    Args:
        surface: Hebrew script string in question.

    Returns:
        Appropriately modified Hebrew script string.
    """
    _last_match = None
    for _last_match in re.finditer(r"[א-ײ]", surface):
        pass

    to_replace = hebr_finals_to_regulars.get(_last_match[0])
    if to_replace is not None:
        surface = surface[:_last_match.start()] + to_replace + surface[_last_match.end():]
    return surface


def join_compound_left(
        left_entry: Optional[OutputEntry],
        center_entry: OutputEntry,
        right_entry: Optional[OutputEntry],
) -> Tuple[OutputEntry, OutputEntry, OutputEntry]:
    """Joins the center entry with the entry to its left.

    Args:
        left_entry: Current left OutputEntry.
        center_entry: Current center OutputEntry.
        right_entry: Current right OutputEntry.

    Returns:
        OutputEntries reflecting any joining of parts.
    """
    if left_entry is None:
        return OutputEntry.new(), center_entry, right_entry or OutputEntry.new()

    left_surface, left_components, _, _ = left_entry
    center_surface, center_components, _, center_config = center_entry
    linking_morpheme = ""
    writing_system = ""
    if center_config is not None:
        linking_morpheme = center_config.get(C_.linking_morpheme) or ""
        writing_system = center_config.get(C_.writing_system) or ""

    if writing_system == c.hebrew_script:
        left_surface_adjusted = left_surface.rstrip(" ").rstrip("-")
        left_surface_adjusted = definalize_hebr(left_surface_adjusted)
        new_left_surface = left_surface_adjusted + linking_morpheme + center_surface.lstrip("-")
    else:
        new_left_surface = left_surface.rstrip(" ").rstrip("-") + linking_morpheme + center_surface.lstrip("-")
    new_left_components = left_components + center_components

    new_center_output_entry = OutputEntry(new_left_surface, new_left_components, left_entry.transform_hook, left_entry.config)
    return OutputEntry.new(), new_center_output_entry, right_entry or OutputEntry.new()


def join_compound_right(
        left_entry: Optional[OutputEntry],
        center_entry: OutputEntry,
        right_entry: Optional[OutputEntry],
) -> Tuple[OutputEntry, OutputEntry, OutputEntry]:
    """Joins the center entry with the entry to its right.

    Args:
        left_entry: Current left OutputEntry.
        center_entry: Current center OutputEntry.
        right_entry: Current right OutputEntry.

    Returns:
        OutputEntries reflecting any joining of parts.
    """
    if right_entry is None:
        return left_entry or OutputEntry.new(), center_entry, OutputEntry.new()

    center_surface, center_components, _, center_config = center_entry
    right_surface, right_components, _, _ = right_entry
    linking_morpheme = ""
    if center_config is not None:
        linking_morpheme = center_config.get(C_.linking_morpheme) or ""

    new_right_surface = center_surface.rstrip(" ").rstrip("-") + linking_morpheme + right_surface.lstrip("-")
    new_right_components = center_components + right_components

    new_center_output_entry = OutputEntry(new_right_surface, new_right_components, right_entry.transform_hook, right_entry.config)
    return left_entry or OutputEntry.new(), new_center_output_entry, OutputEntry.new()


@register("CollectFormsHooks", langs.nb_)
@register("CollectFormsHooks", langs.mul_)
def set_hooks_mul(form_text: str, language: Language, config: CatenaConfig) -> Tuple[Optional[i.SurfaceTransformHook], CatenaConfig]:
    """Sets hooks used when performing surface transformations.

    Args:
        form_text: Surface text of the current OutputEntry.
        language: Language being rendered into.
        config: Additional function configuration.

    Returns:
        Hook to be run during the surface transformation step and modified Catena configuration.
    """
    returned_hook = None
    whether_to_compound = config.get(C_.compounding)
    if whether_to_compound is not None:
        if c.backwards in whether_to_compound:
            returned_hook = join_compound_left
        if c.forwards in whether_to_compound:
            returned_hook = join_compound_right
    return returned_hook, CatenaConfig.new()


@register("CollectForms", langs.mul_)
def collect_forms_mul(catena: "Catena", rel: tfsli.Qid, index: int, parent_index: int) -> i.CollectFormsOutput:
    """Default CollectForms step for all languages which do not define their own.

    Args:
        catena: Catena to get a form from.
        rel: Relationship of the Catena to its parent.
        index: Current index of the current Catena.
        parent_index: Current index of the current Catena.

    Returns:
        OutputEntry-index pair, plus the index for the next Catena.
    """
    lexeme_in, language_in, sense_in, inflections_in, config_in, _, _ = catena

    originator = u.export_originator(config_in)

    obtained_form = obtain_form(lexeme_in, language_in, inflections_in, config_in)

    current_form_text = u.get_wanted_rep(obtained_form, language_in)

    current_form_entries = [
        OutputEntryComponent(
            obtained_form, lexeme_in, language_in, sense_in, inflections_in, originator, index + 1, parent_index, rel,
        ),
    ]

    transform_hook, transform_config = get_collect_forms_hooks(language_in)(current_form_text, language_in, config_in)

    if (linking_morpheme := config_in.get(C_.linking_morpheme)) is not None:
        transform_config = CatenaConfig.new(C_.linking_morpheme(linking_morpheme))

    current_form = OutputEntry(current_form_text, current_form_entries, transform_hook=transform_hook, config=transform_config)

    return [(current_form, index)], index + 1
