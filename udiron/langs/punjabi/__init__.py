"""Holds functions for manipulating Punjabi syntax trees."""

from typing import TYPE_CHECKING

from tfsl import langs

import udiron.base.constants as c
from udiron.base.interfaces import register_language_fallbacks

if TYPE_CHECKING:
    from udiron import CatenaZipper

register_language_fallbacks(langs.pnb_, [langs.pa_])


def attach_postposition(object_in: "CatenaZipper", postposition: "CatenaZipper") -> "CatenaZipper":
    """Attaches a postposition to the provided object.

    Args:
        object_in: Object of the postposition.
        postposition: The postposition itself.

    Returns:
        Provided object with the postposition attached.
    """
    return object_in.attach_rightmost(postposition, c.adposition)
