"""Holds functions for processing Catenae with the language set to Persian."""

from typing import Optional, Tuple

from tfsl import Language, langs

import udiron.base.constants as c
import udiron.base.interfaces as i
from udiron import CatenaConfig, CatenaZipper
from udiron.base.functionconfig import C_
from udiron.langs import register
from udiron.langs.mul.to_str import (capitalize_first_token,
                                     join_compound_left, join_compound_right,
                                     surface_join_spaces)


@register("DevelopForm", langs.new_persian_aran_)
def develop_form_persian(catena_in: CatenaZipper) -> i.SelectFormsOutput:
    """In the absence of exactly one form matching the inflection set provided, modifies the inflection set and Catena configuration to yield an appropriate form.

    Args:
        catena_in: Catena from which to develop a form.

    Returns:
        Possibly modified inflection set and config from that Catena.
    """
    inflections, config = catena_in.get_inflections(), catena_in.get_config()
    if c.short_form in inflections:  # short form
        inflections = inflections - {c.short_form}
    if catena_in.get_category() in {c.noun, c.proper_noun}:
        inflections = inflections | {c.singular}
    return inflections, config


@register("SurfaceJoin", langs.new_persian_aran_)
def surface_join_persian(tokens_in: i.OutputEntryListIndexed, config: CatenaConfig) -> i.OutputEntryList:
    """Joins the tokens provided.

    Args:
        tokens_in: Initial list of OutputEntries with indices.
        config: Additional configuration.

    Returns:
        New joined OutputEntryList.
    """
    if tokens_in[0][0].components[0].language in [langs.new_persian_latn_, langs.new_persian_cyrl_, langs.tg_, langs.tg_latn_]:
        tokens_in = capitalize_first_token(tokens_in)
    return surface_join_spaces(tokens_in)


@register("CollectFormsHooks", langs.new_persian_aran_)
def set_hooks_persian(form_text: str, language: Language, config: CatenaConfig) -> Tuple[Optional[i.SurfaceTransformHook], CatenaConfig]:
    """Sets hooks used when performing surface transformations.

    Args:
        form_text: Surface text of the current OutputEntry.
        language: Language being rendered into.
        config: Additional function configuration.

    Returns:
        Hook to be run during the surface transformation step and modified Catena configuration.
    """
    transform_hook = None
    output_config = CatenaConfig.new()
    if (compound_setting := config.get(C_.compounding)) is not None:
        if language in [langs.jpr_, langs.new_persian_hebr_]:
            output_config = output_config.set(C_.writing_system, c.hebrew_script)
        if compound_setting == c.backwards:
            transform_hook = join_compound_left
        elif compound_setting == c.forwards:
            transform_hook = join_compound_right
    return transform_hook, output_config
