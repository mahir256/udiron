"""Holds functions for manipulating Persian syntax trees."""

from typing import TYPE_CHECKING

import tfsl.interfaces as tfsli
from tfsl import langs

import udiron.base.constants as c
import udiron.base.utils as u
import udiron.langs.mul
import udiron.langs.persian.constants as cfa
import udiron.langs.persian.to_str
from udiron.base.functionconfig import C_
from udiron.base.interfaces import register_language_fallbacks
from udiron.langs.mul import zip_language

if TYPE_CHECKING:
    from udiron import CatenaZipper, FunctionConfig

register_language_fallbacks(langs.new_persian_cyrl_, [langs.new_persian_aran_])
register_language_fallbacks(langs.new_persian_latn_, [langs.new_persian_aran_])
register_language_fallbacks(langs.new_persian_hebr_, [langs.new_persian_aran_])

register_language_fallbacks(langs.fa_, [langs.new_persian_aran_])
register_language_fallbacks(langs.tg_, [langs.new_persian_cyrl_, langs.new_persian_aran_])
register_language_fallbacks(langs.tg_latn_, [langs.new_persian_latn_, langs.new_persian_aran_])
register_language_fallbacks(langs.jpr_, [langs.new_persian_hebr_, langs.new_persian_aran_])


def attach_preposition(object_in: "CatenaZipper", preposition: "CatenaZipper") -> "CatenaZipper":
    """Attaches a preposition to the provided object.

    * ⓘ Q127651975 § 9.1 🗏 132

    Args:
        object_in: Object to be modified.
        preposition: Preposition modifying the object.

    Returns:
        Object with preposition attached.
    """
    return object_in.attach_leftmost(object_in, c.adposition)


def attach_before_verb(predicate: "CatenaZipper", argument: "CatenaZipper", rel: tfsli.Qid, others: "FunctionConfig") -> "CatenaZipper":
    """Attaches an argument to a predicate before the verb.

    Args:
        predicate: Predicate to be modified.
        argument: Argument to attach.
        rel: Relationship between argument and predicate.
        others: Additional function configuration.

    Returns:
        Predicate with argument attached.
    """
    if predicate.has_rel(c.object_complement):
        return predicate.attach_before_rel(argument, rel, c.object_complement)
    return predicate.attach_left_of_root(argument, rel)


def attach_argument_to_predicate(
        predicate: "CatenaZipper",
        argument: "CatenaZipper",
        rel: tfsli.Qid,
        others: "FunctionConfig",
) -> "CatenaZipper":
    """Attaches arguments of a predicate to that predicate.

    Args:
        predicate: Predicate to be modified.
        argument: Argument to attach.
        rel: Relationship between argument and predicate.
        others: Additional function configuration.

    Returns:
        Predicate with argument attached.
    """
    if rel == c.subject or rel == c.modal_adverb:
        return predicate.attach_leftmost(argument, rel)
    else:
        return attach_before_verb(predicate, argument, rel, others)


def form_simple_past(predicate: "CatenaZipper") -> "CatenaZipper":
    """Adds a simple past inflection to the verb.

    * ⓘ Q127651975 § 12.4 🗏 231

    Args:
        predicate: Predicate to inflect.

    Returns:
        Inflected predicate.
    """
    return predicate.add_inflections([c.past_active_participle])


def form_future(predicate: "CatenaZipper") -> "CatenaZipper":
    """Adds a future inflection to the verb.

    * ⓘ Q127651975 § 12.3 🗏 229

    Args:
        predicate: Predicate to inflect.

    Returns:
        Inflected predicate.
    """
    khostan_will = zip_language(cfa.khostan_will, predicate.get_language())
    khostan_will = khostan_will.add_inflections([c.present_stem])
    if predicate.has_rel(c.auxiliary_verb):
        auxiliary = predicate.rel(c.auxiliary_verb)
        auxiliary = auxiliary.add_inflections([c.past_stem])
        _, predicate = auxiliary.pop()
    else:
        predicate = predicate.add_inflections([c.past_stem])
    return predicate.attach_left_of_root(khostan_will, c.auxiliary_verb)


def compound_zwnj_handling(morpheme: "CatenaZipper") -> "CatenaZipper":
    """Ensures that in the Arabic script, a morpheme is attached to something before or after it.

    Args:
        morpheme: Morpheme to modify.

    Returns:
        Appropriately modified morpheme.
    """
    if morpheme.get_language() in {langs.fa_, langs.new_persian_aran_}:
        morpheme = morpheme.add_to_config(C_.linking_morpheme("\u200c"))
    return morpheme


def form_relative_imperfective(predicate: "CatenaZipper", config: "FunctionConfig") -> "CatenaZipper":
    """Adds a future inflection to the verb.

    * ⓘ Q127651975 § 12.2 🗏 224

    Args:
        predicate: Predicate to inflect.
        config: Additional function configuration.

    Returns:
        Inflected predicate.
    """
    if predicate.get_category() == c.verb:
        if predicate.has_rel(c.auxiliary_verb):
            auxiliary = predicate.rel(c.auxiliary_verb)
            if auxiliary.lexeme_id() == tfsli.get_lid_string(cfa.budan_be):
                current_language = auxiliary.get_language()
                ast = zip_language(cfa.ast_copula, current_language, config)
                ast = ast.add_to_config(C_.compounding(c.backwards))
                ast = compound_zwnj_handling(ast)
                auxiliary = auxiliary.substitute_cursor(ast)
                _, predicate = auxiliary.pop()
            return predicate
        elif predicate.lexeme_id() != tfsli.get_lid_string(cfa.doshtan_keep):
            me_imperfective = zip_language(cfa.me_imperfective, predicate.get_language(), config)
            me_imperfective = me_imperfective.add_to_config(C_.compounding(c.forwards))
            me_imperfective = compound_zwnj_handling(me_imperfective)
            predicate = predicate.add_inflections([c.present_stem])
            return predicate.attach_left_of_root(me_imperfective, c.aspect)
        else:
            predicate = predicate.add_inflections([c.present_stem])
            return predicate
    else:
        if predicate.has_rel(c.auxiliary_verb) and predicate.rel(c.auxiliary_verb).lexeme_id() == tfsli.get_lid_string(cfa.budan_be):
            predicate = predicate.detach_rel(c.auxiliary_verb)
        return predicate


def inflect_for_tense_aspect_mood(
        predicate: "CatenaZipper",
        config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds inflections to verbs and verb phrases for tense/aspect/mood.

    Args:
        predicate: Predicate to inflect.
        config: Additional function configuration.

    Returns:
        Inflected predicate.
    """
    utterance_time = u.get_as_timestamp_or_fallback(config, C_.now)
    occurrence_time = u.get_as_timestamp_or_fallback(config, C_.occurrence_time)
    situation_time = u.get_as_timestamp_or_fallback(config, C_.reference_time, utterance_time)

    if occurrence_time.date() < situation_time.date():
        predicate = form_simple_past(predicate)
    elif occurrence_time.date() > situation_time.date():
        predicate = form_future(predicate)
    else:
        predicate = form_relative_imperfective(predicate, config)

    return inflect_for_person_number(predicate, config)


def inflect_for_person_number(predicate: "CatenaZipper", config: "FunctionConfig") -> "CatenaZipper":
    """Inflects a predicate for person and number.

    Args:
        predicate: Predicate to inflect.
        config: Additional function configuration.

    Returns:
        Inflected predicate.
    """
    if not (predicate.has_rel(c.auxiliary_verb) and predicate.rel(c.auxiliary_verb).lexeme_id() == tfsli.get_lid_string(cfa.hast_copula)):
        agreement_basis = predicate.rel(c.subject)
    else:
        agreement_basis = predicate
    person = find_grammatical_person(agreement_basis)
    number = find_grammatical_number(agreement_basis)

    verbal_sense = cfa.verbals[person, number]
    verbal = zip_language(verbal_sense, predicate.get_language(), config)
    verbal = verbal.add_to_config(C_.compounding(c.backwards))

    if predicate.has_rel(c.auxiliary_verb):
        aux = predicate.rel(c.auxiliary_verb)
        if aux.lexeme_id() in [tfsli.get_lid_string(cfa.ast_copula), tfsli.get_lid_string(cfa.hast_copula), tfsli.get_lid_string(cfa.nist_copula)]:
            aux = aux.add_inflections([person, number])
        else:
            aux = aux.attach_right_of_root(verbal, c.inflectional_suffix)
        _, predicate = aux.pop()
    else:
        if predicate.has_inflections([c.past_active_participle]):
            ast = zip_language(cfa.ast_copula, predicate.get_language(), config)
            ast = ast.add_to_config(C_.compounding(c.backwards))
            ast = compound_zwnj_handling(ast)
            ast = ast.add_inflections([person, number, c.short_form])
            predicate = predicate.attach_right_of_root(ast, c.auxiliary_verb)
        else:
            predicate = predicate.attach_right_of_root(verbal, c.inflectional_suffix)
    return predicate


def find_grammatical_person(
        subject: "CatenaZipper",
) -> tfsli.Qid:
    """Checks for a grammatical person in the provided Catena.

    Args:
        subject: Catena whose lexeme possibly contains 'grammatical person' statements.

    Returns:
        Qid for the first 'grammatical person' statement value, or 'third person' if no statement is found.
    """
    try:
        person_inflection = udiron.langs.mul.get_grammatical_person(subject)
    except StopIteration:
        return c.third_person
    else:
        return person_inflection


def find_grammatical_number(
        subject: "CatenaZipper",
) -> tfsli.Qid:
    """Checks for a grammatical number in the provided Catena.

    Args:
        subject: Catena whose lexeme possibly contains 'grammatical number' statements.

    Returns:
        Qid for the first 'grammatical number' statement value, or either 'singular' or 'plural' depending on the presence of suffix '-lar' or whether 'plural' is in the subject's inflection set.
    """
    try:
        person_inflection = udiron.langs.mul.get_grammatical_number(subject)
    except StopIteration:
        if c.plural in subject.get_inflections() or subject.has_rel(c.number_inflection):
            return c.plural
        return c.singular
    else:
        return person_inflection


def build_copular_sentence(
    subject: "CatenaZipper",
    predicate: "CatenaZipper",
    copula: "CatenaZipper",
    config: "FunctionConfig",
) -> "CatenaZipper":
    """Builds a simple copular sentence using 'ast'.

    Args:
        subject: Subject of the copular sentence.
        predicate: Predicate of the copular sentence.
        copula: Copula to use in that sentence.
        config: Additional function configuration.

    Returns:
        Simple copular sentence.
    """
    sentence = predicate.attach_leftmost(subject, c.subject)
    sentence = sentence.attach_rightmost(copula, c.auxiliary_verb)
    inflected_sentence = inflect_for_tense_aspect_mood(sentence, config)
    return inflected_sentence
