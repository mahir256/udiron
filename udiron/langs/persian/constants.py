"""Holds constants for use with Persian."""

from tfsl.interfaces import LSid

import udiron.base.constants as c

dar_in = LSid("L230487-S1")
az_from = LSid("L226818-S1")
bo_with = LSid("L230550-S1")
baroi_reason = LSid("L230551-S2")
baroi_purpose = LSid("L230551-S4")
baroi_against = LSid("L230551-S5")

man_i = LSid("L2377-S1")
tu_thou = LSid("L2378-S1")
u_3s = LSid("L614026-S1")
mo_we = LSid("L614028-S1")
vaj_3s = LSid("L614027-S1")
shumo_you = LSid("L614029-S1")
eshon_3s = LSid("L742783-S1")
eshon_they = LSid("L742783-S2")

me_imperfective = LSid("L749793-S1")

verbal_1s = LSid("L1337830-S1")
verbal_2s = LSid("L1337834-S1")
verbal_3s = LSid("L1337831-S1")
verbal_1p = LSid("L1337832-S1")
verbal_2p = LSid("L1337835-S1")
verbal_3p = LSid("L1337833-S1")

verbals = {
    (c.first_person, c.singular): verbal_1s,
    (c.second_person, c.singular): verbal_2s,
    (c.third_person, c.singular): verbal_3s,
    (c.first_person, c.plural): verbal_1p,
    (c.second_person, c.plural): verbal_2p,
    (c.third_person, c.plural): verbal_3p,
}

budan_be = LSid("L12614-S2")
ast_copula = LSid("L1334434-S1")
hast_copula = LSid("L1334435-S1")
nist_copula = LSid("L1334436-S1")
kardan_do = LSid("L407237-S1")
doshtan_keep = LSid("L749795-S2")
khostan_will = LSid("L749797-S1")

e_ezafe = LSid("L1083584-S1")
