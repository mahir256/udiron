"""Holds functions for manipulating Bengali syntax trees."""

from tfsl import langs

from udiron.base.interfaces import register_language_fallbacks

register_language_fallbacks(langs.rarhi_, [langs.bn_])
register_language_fallbacks(langs.varendri_, [langs.bn_])
register_language_fallbacks(langs.bangali_, [langs.bn_])
register_language_fallbacks(langs.manbhumi_, [langs.bn_])
