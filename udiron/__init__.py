"""Provides a number of base classes.

Note that the contents of udiron.langs are *not* imported with "import udiron".
"""

import logging

from udiron.base import (
    C_, Catena, CatenaConfig, CatenaTracking,
    CatenaTrackingEntry, CatenaZipper, DependentEntry,
    DependentList, FunctionConfig, zip_lexeme, zip_lid,
)

__all__ = [
    "C_", "Catena", "CatenaConfig", "CatenaTracking", "CatenaTrackingEntry", "CatenaZipper",
    "DependentEntry", "DependentList", "FunctionConfig", "zip_lexeme", "zip_lid",
]

logging.getLogger("udiron").addHandler(logging.NullHandler())
