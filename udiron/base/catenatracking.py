"""Holds the CatenaTracking and CatenaTrackingEntry classes."""

from typing import TYPE_CHECKING, List, NamedTuple, Optional

if TYPE_CHECKING:
    import udiron.base.interfaces as i


class CatenaTrackingEntry(NamedTuple):
    """Entry in a CatenaTracking.

    Attributes:
        constructor_type: The type of Constructor involved at a particular stage.
        constructor_id: The ID of the Constructor involved at a particular stage.
    """
    constructor_type: str
    constructor_id: str

    @classmethod
    def new(cls, ctor_type: Optional[str] = None, ctor_id: Optional[str] = None) -> "CatenaTrackingEntry":
        """Essentially a custom constructor for the CatenaTrackingEntry class.

        Args:
            ctor_type: Constructor type.
            ctor_id: Constructor identifier.

        Returns:
            New CatenaTrackingEntry.
        """
        return cls(ctor_type or "", ctor_id or "")

    @classmethod
    def build(cls, arg: "i.CatenaTrackingEntryDict") -> "CatenaTrackingEntry":
        """Builds a CatenaTrackingEntry from a serialization.

        Args:
            arg: Serialization of a CatenaTrackingEntry.

        Returns:
            New CatenaTrackingEntry.
        """
        return cls(arg["constructor_type"], arg["constructor_id"])

    def __jsonout__(self) -> "i.CatenaTrackingEntryDict":
        """Produces a JSON serialization of this CatenaTrackingEntry.

        Returns:
            JSON serialization of this CatenaTrackingEntry.
        """
        return {
            "constructor_type": self.constructor_type,
            "constructor_id": self.constructor_id,
        }

    def __str__(self) -> str:
        """Produces a representation of a catena tracking entry intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a CatenaTrackingEntry.
        """
        return self.constructor_type + "#" + self.constructor_id


class CatenaTracking(NamedTuple):
    """Container holding information about the Constructor origin and Constructor modifiers of a particular Catena.

    The first entry in such a container should represent the origin,
    while later entries should represent later modifiers of the Catena.

    Attributes:
        entries: The actual list of Catena tracking entries.
    """
    entries: List[CatenaTrackingEntry]

    @classmethod
    def new(cls, entries: Optional[List[CatenaTrackingEntry]] = None) -> "CatenaTracking":
        """Essentially a custom constructor for the CatenaTracking class.

        Args:
            entries: List of entries to add to the CatenaTracking.

        Returns:
            New CatenaTracking.
        """
        if entries is None:
            return cls([])
        return cls(entries)

    def push(self, new_entry: CatenaTrackingEntry) -> "CatenaTracking":
        """Adds an entry to the CatenaTracking.

        Args:
            new_entry: The new entry to add to the CatenaTracking.

        Returns:
            New CatenaTracking with the new entry at the end.
        """
        return CatenaTracking(self.entries + [new_entry])

    def top(self) -> CatenaTrackingEntry:
        """Shortcut to the last entry in the CatenaTracking.

        Returns:
            The most recent entry in this CatenaTracking.
        """
        return self.entries[-1]

    def root(self) -> CatenaTrackingEntry:
        """Shortcut to the first entry in the CatenaTracking.

        Returns:
            The least recent entry in this CatenaTracking.
        """
        return self.entries[0]

    def extend(self, other: object) -> "CatenaTracking":
        """Add information from an object to a CatenaTracking.

        Args:
            other: Object to add from.

        Returns:
            Newly extended CatenaTracking:

        Raises:
            ValueError: if the object cannot be used to extend the CatenaTracking.
        """
        if isinstance(other, CatenaTracking):
            return CatenaTracking(self.entries + other.entries)
        raise ValueError(f"Cannot add {type(other)} to CatenaTracking")

    def __jsonout__(self) -> "i.CatenaTrackingDict":
        """Produces a JSON serialization of this CatenaTracking.

        Returns:
            JSON serialization of this CatenaTracking.
        """
        return {"entries": [entry.__jsonout__() for entry in self.entries]}


def build_catenatrackingentry(entry_in: "i.CatenaTrackingEntryDict") -> CatenaTrackingEntry:
    """Builds a CatenaTrackingEntry from a JSON serialization.

    Args:
        entry_in: JSON serialization of a CatenaTrackingEntry.

    Returns:
        New CatenaTrackingEntry.
    """
    return CatenaTrackingEntry(entry_in["constructor_type"], entry_in["constructor_id"])


def build_catenatracking(tracking_in: "i.CatenaTrackingDict") -> CatenaTracking:
    """Builds a CatenaTracking from a JSON serialization.

    Args:
        tracking_in: JSON serialization of a CatenaTracking.

    Returns:
        New CatenaTracking.
    """
    return CatenaTracking([build_catenatrackingentry(entry) for entry in tracking_in["entries"]])
