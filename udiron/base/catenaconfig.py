"""Holds the CatenaConfig class and things related to it."""

from typing import Any, NamedTuple, Optional, Union, overload

import udiron.base.interfaces as i
import udiron.base.utils as u
from udiron.base.catenatracking import (CatenaTracking, CatenaTrackingEntry,
                                        build_catenatracking)
from udiron.base.functionconfig import (Entity, EntityKey, FunctionConfig,
                                        StrKey, build_functionconfig)


class CatenaConfig(NamedTuple):
    """Configuration object for Catenae.

    Examples of scope_args include adverbs within an adjectival phrase that must appear outside their typical position within that phrase,
    to be later extracted and incorporated.

    Attributes:
        config: Holds information about how a syntax tree node should be transformed/manipulated at Catena render time. How this information is processed depends on the implementations of the individual rendering steps.
        scope_args: Holds any syntax subtrees related to this Catena that have not yet been incorporated into the main syntax tree.
        tracking: Holds information about the Constructors which when rendered yielded this Catena.
    """
    config: FunctionConfig
    scope_args: i.ScopeArgumentMapping
    tracking: CatenaTracking

    @classmethod
    def new(
        cls,
        config: Optional[FunctionConfig] = None,
        scope_args: Optional[i.ScopeArgumentMapping] = None,
        tracking: Optional[CatenaTracking] = None,
    ) -> "CatenaConfig":
        """Essentially a custom constructor for the CatenaConfig class.

        Args:
            config: Initial FunctionConfig.
            scope_args: Initial ScopeArgumentMapping.
            tracking: Initial CatenaTracking.

        Returns:
            New CatenaConfig.
        """
        return cls(
            config or FunctionConfig.new(),
            scope_args or {},
            tracking or CatenaTracking.new(),
        )

    def clone(
        self,
        config: Optional[FunctionConfig] = None,
        scope_args: Optional[i.ScopeArgumentMapping] = None,
        tracking: Optional[CatenaTracking] = None,
    ) -> "CatenaConfig":
        """Clones a CatenaConfig while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the CatenaConfig
        is preserved in the output.

        Args:
            config: Initial FunctionConfig.
            scope_args: Initial ScopeArgumentMapping.
            tracking: Initial CatenaTracking.

        Returns:
            New CatenaConfig.
        """
        old_config, old_scope_args, old_tracking = self
        return CatenaConfig(
            old_config if config is None else config,
            old_scope_args if scope_args is None else scope_args,
            old_tracking if tracking is None else tracking,
        )

    def has(self, key: Union[StrKey, EntityKey]) -> bool:
        """Checks for the presence of a key in the CatenaConfig.

        Args:
            key: Key to be checked for.

        Returns:
            Whether the provided key is in the main entry of the CatenaConfig.
        """
        return self.config.has(key)

    @overload
    def get(self, key: EntityKey) -> Optional[Entity]: ...
    @overload
    def get(self, key: StrKey) -> Optional[str]: ...

    def get(self, key: Any) -> Optional[Any]:
        """Retrieves a value in the CatenaConfig for the given key.

        Args:
            key: Key into the CatenaConfig.

        Returns:
            Value mapped to that key.

        Raises:
            KeyError: if the key provided is not valid for a CatenaConfig.
        """
        if isinstance(key, (StrKey, EntityKey)):
            return self.config.get(key)
        raise KeyError(f"{type(key)} is not a valid key for a CatenaConfig")

    @overload
    def set(self, key: StrKey, value: str) -> "CatenaConfig": ...
    @overload
    def set(self, key: EntityKey, value: Entity) -> "CatenaConfig": ...

    def set(self, key: Any, value: Any) -> "CatenaConfig":
        """Sets a key in the main entry of the CatenaConfig to the value provided.

        Args:
            key: Key to set.
            value: Value to set.

        Returns:
            CatenaConfig reflecting the new key-value pair.
        """
        return self.clone(config=self.config.set(key, value))

    def push_tracking(self, new_entry: CatenaTrackingEntry) -> "CatenaConfig":
        """Adds a new entry to this config's CatenaTracking.

        Args:
            new_entry: CatenaTrackingEntry to add.

        Returns:
            CatenaConfig reflecting new tracking entry.
        """
        new_tracking = self.tracking.push(new_entry)
        return self.clone(tracking=new_tracking)

    def update(self, other: "CatenaConfig") -> "CatenaConfig":
        """Updates a CatenaConfig with information from the provided CatenaConfig.

        Args:
            other: CatenaConfig to update with this one.

        Returns:
            CatenaConfig reflecting newly updated entries.
        """
        other_config, other_scope_args, other_tracking = other

        new_config = self.config | other_config
        try:
            new_tracking = self.tracking.extend(other_tracking)
        except ValueError:
            new_tracking = self.tracking

        if self.scope_args is None and other_scope_args is None:
            new_scope_args = None
        else:
            new_scope_args = u.cat_argument_dicts(self.scope_args, other_scope_args)

        return CatenaConfig(new_config, new_scope_args, new_tracking)

    def __jsonout__(self) -> i.CatenaConfigDict:
        """Produces a JSON serialization of this CatenaConfig.

        Returns:
            JSON serialization of this CatenaConfig.
        """
        from udiron.base.catenazipper import jsonout_all_catenazippers
        return {
            "config": self.config.__jsonout__(),
            "scope_args": {k: jsonout_all_catenazippers(v) for k, v in self.scope_args.items()},
            "tracking": self.tracking.__jsonout__(),
        }


def build_catenaconfig(config_in: i.CatenaConfigDict) -> CatenaConfig:
    """Builds a CatenaConfig from a JSON serialization.

    Args:
        config_in: JSON serialization of a CatenaConfig.

    Returns:
        New CatenaConfig.
    """
    from udiron.base.catenazipper import build_all_catenazippers
    return CatenaConfig(
        build_functionconfig(config_in["config"]),
        {k: build_all_catenazippers(v) for k, v in config_in["scope_args"].items()},
        build_catenatracking(config_in["tracking"]),
    )
