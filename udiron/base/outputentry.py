"""This module holds the OutputEntry class and functions used with it.

See `CatenaZipper.render <udiron.base.catenazipper>`_ and the steps it notes
for more context on the uses of the classes in this module.
"""

from typing import TYPE_CHECKING, List, NamedTuple, Optional

import tfsl.interfaces as tfsli

import udiron.base.constants as c
import udiron.base.interfaces as i

if TYPE_CHECKING:
    from tfsl import Language, LexemeFormLike, LexemeLike, LexemeSenseLike

    from udiron.base.catenaconfig import CatenaConfig
    from udiron.base.catenatracking import CatenaTrackingEntry


class OutputEntryComponent(NamedTuple):
    """Component of an OutputEntry.

    The first four components are typically identical to those of the Catena from which
    it originates, while the fifth ideally originates from the tracking portion of the
    Catena's configuration (yet to be used) and the sixth and seventh fields ideally originate
    from someplace yet to be determined.

    Attributes:
        form: This component's lexeme form.
        lexeme: This component's lexeme.
        language: This component's language.
        sense: This component's lexeme sense.
        inflections: This component's inflection set.
        originator: The originator of the Catena that led to this component.
        position: The index of this component within the syntax tree.
        head: The index of the head of this component within the syntax tree.
        rel: The relationship of this component to its head in the syntax tree.
    """
    form: "LexemeFormLike"
    lexeme: "LexemeLike"
    language: "Language"
    sense: "LexemeSenseLike"
    inflections: i.InflectionSet
    originator: "CatenaTrackingEntry"
    position: int = 0
    head: int = 0
    rel: tfsli.Qid = tfsli.Qid(c.syntactic_root)

    def clone(
        self,
        form: Optional["LexemeFormLike"] = None,
        lexeme: Optional["LexemeLike"] = None,
        language: Optional["Language"] = None,
        sense: Optional["LexemeSenseLike"] = None,
        inflections: Optional[i.InflectionSet] = None,
        originator: Optional["CatenaTrackingEntry"] = None,
        position: Optional[int] = None,
        head: Optional[int] = None,
        rel: Optional[tfsli.Qid] = None,
    ) -> "OutputEntryComponent":
        """Clones an OutputEntryComponent while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the OutputEntry
        is preserved in the output.

        Args:
            form: This component's lexeme form.
            lexeme: This component's lexeme.
            language: This component's language.
            sense: This component's lexeme sense.
            inflections: This component's inflection set.
            originator: The originator of the Catena that led to this component.
            position: The index of this component within the syntax tree.
            head: The index of the head of this component within the syntax tree.
            rel: The relationship of this component to its head in the syntax tree.

        Returns:
            New OutputEntryComponent.
        """
        old_form, old_lexeme, old_language, old_sense, old_inflections, old_originator, old_position, old_head, old_rel = self
        return OutputEntryComponent(
            old_form if form is None else form,
            old_lexeme if lexeme is None else lexeme,
            old_language if language is None else language,
            old_sense if sense is None else sense,
            old_inflections if inflections is None else inflections,
            old_originator if originator is None else originator,
            old_position if position is None else position,
            old_head if head is None else head,
            old_rel if rel is None else rel,
        )


class OutputEntry(NamedTuple):
    """An element of the output from rendering a Catena, representing a contiguous unit (most likely a 'word').

    This construct is significant whenever a syntax tree contains individual nodes
    for affixes of a word, and especially when the combination of those affixes
    under phonological rules of a language makes it difficult to recover the individual
    affixes from just the surface form alone.
    It thus allows the components of an individual word,
    as well as information that led up to the production of those components,
    to be retained and presented to an end user in some graphical/interactive form to be determined.

    As an example, the isiZulu word "angiyukwamukela" might be represented in an output with
    one OutputEntry (with that surface form) but with five OutputEntryComponents,
    representing the lexemes for 'a', 'ngi', 'yu', 'ku', and 'amukela'
    (respectively lexemes for negation, subject concord, remote future tense,
    object concord, and the verb 'to accept').

    As another example, the Italian contraction 'alla' might also be represented in an output
    with one OutputEntry but with two OutputEntryComponents (one for 'a' and one for 'la').

    Attributes:
        surface: Surface representation of this element of the output.
        components: List of components which when combined yield the surface representation.
        transform_hook: See the documentation for `SurfaceTransformHook <udiron.base.interfaces>_` for more details.
        config: Any configuration needed by a hook when processing this inside a SurfaceTransform.
    """
    surface: str
    components: List[OutputEntryComponent]
    transform_hook: Optional[i.SurfaceTransformHook] = None
    config: Optional["CatenaConfig"] = None

    @classmethod
    def new(cls) -> "OutputEntry":
        """Essentially a custom constructor for the OutputEntry class.

        Returns:
            New OutputEntry.
        """
        return OutputEntry("", [])

    def empty(self) -> bool:
        """Checks for a nonzero length of the OutputEntry.

        Returns:
            Whether the OutputEntry is empty.
        """
        return self.surface == "" and self.components == []

    def clone(
        self,
        surface: Optional[str] = None,
        components: Optional[List[OutputEntryComponent]] = None,
    ) -> "OutputEntry":
        """Clones an OutputEntry while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the OutputEntry
        is preserved in the output.

        Args:
            surface: Surface representation of this element of the output.
            components: List of components which when combined yield the surface representation.

        Returns:
            New OutputEntry.
        """
        old_surface, old_components, old_hook, old_config = self
        return OutputEntry(
            old_surface if surface is None else surface,
            old_components if components is None else components,
            old_hook,
            old_config,
        )

    def set_heads(self, old_index: int, new_index: int) -> "OutputEntry":
        """Changes the head index of the components in an OutputEntry sharing a particular index.

        Args:
            old_index: Head index shared by components to be modified.
            new_index: New index to set those components' head indices to.

        Returns:
            OutputEntry reflecting those changes.
        """
        old_surface, old_components, old_hook, old_config = self
        new_components = [(component.clone(head=new_index) if component.head == old_index else component) for component in old_components]
        return OutputEntry(
            old_surface,
            new_components,
            old_hook,
            old_config,
        )

    def adjust_indices(self, offset: int) -> "OutputEntry":
        """Adjusts the indices in an OutputEntry's components by a constant amount.

        Args:
            offset: How much to shift component indices by.

        Returns:
            OutputEntry reflecting the shift in component indices.
        """
        old_surface, old_components, old_hook, old_config = self
        new_components = [component.clone(position=component.position + offset, head=0 if component.head == 0 else (component.head + offset)) for component in old_components]
        return OutputEntry(
            old_surface,
            new_components,
            old_hook,
            old_config,
        )


def conllu(entries: i.OutputEntryList, prefer_lang: Optional["Language"] = None) -> str:
    """Produces a textual representation of a list of OutputEntries reminiscent of CoNLL-U.

    Args:
        entries: List of OutputEntries from rendering a CatenaZipper.
        prefer_lang: Language whose script code is preferred for the lemma and form columns.

    Returns:
        CoNLL-U approximation of the provided OutputEntries.
    """
    lines = []
    for entry in entries:
        surface = entry.surface
        if len(entry.components) > 1:
            component_indices = [component.position for component in entry.components]
            lines.append((str(min(component_indices)) + "-" + str(max(component_indices)), surface, "_", "_", "_", "_", "_", "_", "_", "_"))
        for component in entry.components:
            cid = str(component.position)
            if len(entry.components) == 1:
                form = surface.rstrip()
            else:
                if prefer_lang and (prefer_lang in component.form.representations):
                    form = component.form[prefer_lang].text
                else:
                    form = str(component.form.representations)
            form = form.replace(" ", "_")
            if prefer_lang and (prefer_lang in component.lexeme.lemmata):
                lemma = component.lexeme[prefer_lang].text
            else:
                lemma = str(component.lexeme.lemmata).replace(" ", "_")
            lemma = lemma.replace(" ", "_")
            upos = component.lexeme.category
            xpos = "_"
            feats = "|".join(component.form.features) or "_"
            head = str(component.head)
            deprel = component.rel
            deps = "_"

            misc_parts = []
            if surface.rstrip() != form:
                misc_parts.append("surfacerep=" + surface.rstrip().replace(" ", "_"))
            misc_parts.append("lexeme=" + (component.lexeme.id or "_"))
            misc_parts.append("form=" + (component.form.id or "_"))
            misc_parts.append("sense=" + (component.sense.id or "_"))
            originator_str = str(component.originator)
            if originator_str != "#":
                misc_parts.append("originator=" + originator_str)
            misc = "|".join(misc_parts)

            lines.append((cid, form, lemma, upos, xpos, feats, head, deprel, deps, misc))
    return "\n".join(["\t".join(line) for line in lines])
