"""Holds the DependentEntry class and things related to it."""

from typing import TYPE_CHECKING, NamedTuple

if TYPE_CHECKING:
    import tfsl.interfaces as tfsli

    import udiron.base.catena
    import udiron.base.interfaces as i


class DependentEntry(NamedTuple):
    """Entry in a Catena's list of dependents.

    Attributes:
        catena: Dependent Catena.
        rel: Relation of the Catena to its parent.
    """
    catena: "udiron.base.catena.Catena"
    rel: "tfsli.Qid"

    def __repr__(self) -> str:
        """Produces string representation of the DependentEntry.

        Returns:
            String representation of the DependentEntry.
        """
        catena, rel = self
        base_string = f"<──({rel})─" + catena.abbr()
        left_dependent_string = catena.left_dependent_string(rel)
        right_dependent_string = catena.right_dependent_string(rel)
        return "\n".join([left_dependent_string, base_string, right_dependent_string]).strip("\n")

    # Calls to members

    def __jsonout__(self) -> "i.DependentEntryDict":
        """Produces a JSON serialization of this DependentEntry.

        Returns:
            JSON serialization of this DependentEntry.
        """
        return {"catena": self.catena.__jsonout__(), "rel": self.rel}


def build_dependententry(entry_in: "i.DependentEntryDict") -> DependentEntry:
    """Builds a DependentEntry from a JSON serialization.

    Args:
        entry_in: JSON serialization of a DependentEntry.

    Returns:
        New DependentEntry.
    """
    from udiron.base.catena import build_catena
    return DependentEntry(build_catena(entry_in["catena"]), entry_in["rel"])
