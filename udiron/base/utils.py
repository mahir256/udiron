"""Holds a number of general utility functions."""

from datetime import datetime
from typing import TYPE_CHECKING, Optional, Sequence, Union

import tfsl.interfaces as tfsli
from tfsl import Language, LexemeFormLike, LexemeLike, LexemeSenseLike

import udiron.base.constants as c
import udiron.base.interfaces as i
from udiron.base.catenatracking import CatenaTrackingEntry

if TYPE_CHECKING:
    from udiron.base.catenaconfig import CatenaConfig
    from udiron.base.catenazipper import CatenaZipper
    from udiron.base.functionconfig import EntityKey, FunctionConfig, StrKey


def get_wanted_rep(
        wanted_entity: Union[LexemeLike, LexemeSenseLike, LexemeFormLike],
        wanted_language: Union[Language, Sequence[Language]],
) -> str:
    """Obtains a string from the top-level of the provided entity, be it a lemma, form representation, or sense gloss.

    If a single wanted_language is provided, and a string does not exist in that entity in that language,
    then the languages in its fallback chain are tried afterwards.
    The wanted_language provided may instead be a sequence of Languages to be tried in order.

    Args:
        wanted_entity: Lexeme, lexeme form, or lexeme sense.
        wanted_language: Language (or a set of languages) to find a string from.

    Returns:
        String portion of a lemma, form representation, or sense gloss (which is a MonolingualText).
    """
    if isinstance(wanted_language, Language):
        wanted_language_list = i.get_language_fallbacks(wanted_language)
    else:
        wanted_language_list = wanted_language

    for language in wanted_language_list:
        try:
            wanted_mt = wanted_entity[language]
            break
        except StopIteration:
            continue
    return wanted_mt.text


def catena_lexeme_of_grammatical_gender(catena: "CatenaZipper", gender: tfsli.Qid) -> bool:
    """Checks whether catena has a 'grammatical gender' statement with gender as a value.

    Args:
        catena: Lexeme of the cursor of this CatenaZipper.
        gender: Qid representing a grammatical gender.

    Returns:
        Ultimate decision.
    """
    return catena.haswbstatement(c.grammatical_gender, gender)


def cat_argument_dicts(*argument_dicts: Optional[i.ScopeArgumentMapping]) -> i.ScopeArgumentMapping:
    """Combines multiple ScopeArgumentMappings.

    Args:
        argument_dicts: List of ScopeArgumentMappings, any of which may be None.

    Returns:
        Concatenation of argument_dicts, having skipped entries in that list that are None.
    """
    new_argument_dict: i.ScopeArgumentMapping = {}
    for argument_dict in argument_dicts:
        if argument_dict is None:
            continue
        for arg in argument_dict:
            if arg in new_argument_dict:
                new_argument_dict[arg] += argument_dict[arg]
            else:
                new_argument_dict[arg] = argument_dict[arg]
    return new_argument_dict


def export_originator(config: "CatenaConfig") -> CatenaTrackingEntry:
    """Produces a CatenaTrackingEntry for the origin of a Catena.

    Args:
        config: CatenaConfig with a potentially non-empty CatenaTracking.

    Returns:
        First CatenaTrackingEntry in the CatenaConfig, or an empty one if the CatenaTracking is empty.
    """
    try:
        return config.tracking.root()
    except IndexError:
        return CatenaTrackingEntry.new()


def get_as_qid_or_fallback(config: "FunctionConfig", key: "StrKey", fallback: Optional[tfsli.Qid] = None) -> tfsli.Qid:
    """Attempts to retrieve the Qid value for the given key from the given config, returning the given fallback if there is no value for that key.

    Args:
        config: Configuration from which to retrieve a Qid.
        key: Key in configuration whose value should be a Qid.
        fallback: Qid to return if provided key doesn't exist in configuration.

    Returns:
        Qid corresponding to provided key in configuration, or fallback if key not present.

    Raises:
        ValueError: if the provided key does not exist, or if it exists but the value is not a Qid.
    """
    if (value := config.get(key)) is not None:
        if tfsli.is_qid(value):
            return value
        raise ValueError("Key exists but value is not a Qid")
    elif fallback is not None:
        return fallback
    raise ValueError("No value for key in config")


def get_as_timestamp_or_fallback(config: "FunctionConfig", key: "EntityKey", fallback: Optional[datetime] = None) -> datetime:
    """Attempts to retrieve the datetime value for the given key from the given config, returning the given fallback if there is no value for that key.

    Args:
        config: Configuration from which to retrieve a timestamp.
        key: Key in configuration whose value should be a timestamp.
        fallback: Timestamp to return if provided key doesn't exist in configuration.

    Returns:
        Timestamp corresponding to provided key in configuration, or fallback if key not present.

    Raises:
        ValueError: if the provided key does not exist, or if it exists but the value is not a datetime.
    """
    if (value := config.get(key)) is not None:
        if isinstance(value, datetime):
            return value
        raise ValueError("Key exists but value is not a datetime")
    elif fallback is not None:
        return fallback
    raise ValueError("No value for key in config")
