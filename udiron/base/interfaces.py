"""Holds some type aliases used elsewhere."""

import re
from typing import (TYPE_CHECKING, Any, Dict, FrozenSet, List, MutableMapping,
                    NewType, Optional, Protocol, Sequence, Tuple, TypedDict,
                    Union)

import tfsl.interfaces as tfsli
from tfsl import ItemValue, Language, langs

if TYPE_CHECKING:
    from typing_extensions import TypeAlias, TypeGuard

    import udiron.base.catena
    import udiron.base.functionconfig
    from udiron.base.catenaconfig import CatenaConfig
    from udiron.base.catenazipper import CatenaZipper
    from udiron.base.dependententry import DependentEntry
    from udiron.base.dependentlist import DependentList
    from udiron.base.outputentry import OutputEntry

# The interfaces below do not rely on any Udiron types.

LFSid_regex = re.compile(r"^(L\d+)-(F\d+)-(S\d+)$")
LFSid = NewType("LFSid", str)


def is_lfsid(arg: str) -> "TypeGuard[LFSid]":
    """Checks that a string is a LFSid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an LFSid.
    """
    return LFSid_regex.match(arg) is not None


LSFid_regex = re.compile(r"^(L\d+)-(S\d+)-(F\d+)$")
LSFid = NewType("LSFid", str)


def is_lsfid(arg: str) -> "TypeGuard[LSFid]":
    """Checks that a string is a LSFid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an LSFid.
    """
    return LSFid_regex.match(arg) is not None


Zippable = Union[int, tfsli.Lid, tfsli.LFid, tfsli.LSid, LFSid, LSFid, ItemValue]


def split_lfsid(arg: LFSid) -> Optional[Tuple[tfsli.LSid, tfsli.LFid]]:
    """Splits an LFSid into an LSid and an LFid.

    Args:
        arg: Indicator of a lexeme-form-sense combination where those parts appear in that order.

    Returns:
        Either a sense ID and form ID, or neither.
    """
    if matched_parts := LFSid_regex.match(arg):
        lid_part: Optional[str] = matched_parts.group(1)
        fid_part: Optional[str] = matched_parts.group(2)
        sid_part: Optional[str] = matched_parts.group(3)
        if lid_part is not None and fid_part is not None and sid_part is not None:
            lsid_part = lid_part + "-" + sid_part
            lfid_part = lid_part + "-" + fid_part
            if tfsli.is_lsid(lsid_part) and tfsli.is_lfid(lfid_part):
                return lsid_part, lfid_part
    return None


def split_lsfid(arg: LSFid) -> Optional[Tuple[tfsli.LSid, tfsli.LFid]]:
    """Splits an LSFid into an LSid and an LFid.

    Args:
        arg: Indicator of a lexeme-sense-form combination where those parts appear in that order.

    Returns:
        Either a sense ID and form ID, or neither.
    """
    if matched_parts := LSFid_regex.match(arg):
        lid_part: Optional[str] = matched_parts.group(1)
        sid_part: Optional[str] = matched_parts.group(2)
        fid_part: Optional[str] = matched_parts.group(3)
        if lid_part is not None and fid_part is not None and sid_part is not None:
            lsid_part = lid_part + "-" + sid_part
            lfid_part = lid_part + "-" + fid_part
            if tfsli.is_lsid(lsid_part) and tfsli.is_lfid(lfid_part):
                return lsid_part, lfid_part
    return None


def split_into_form_and_sense(arg: Union[LFSid, LSFid]) -> Optional[Tuple[tfsli.LSid, tfsli.LFid]]:
    """Splits an LFSid or LSFid into the LFid part and the LSid part.

    Args:
        arg: Indicator of a lexeme-form-sense combination.

    Returns:
        Either a sense ID and form ID, or neither.
    """
    if is_lsfid(arg):
        return split_lsfid(arg)
    elif is_lfsid(arg):
        return split_lfsid(arg)
    return None


class UdironError(Exception):
    """Base exception from which all Udiron exceptions must inherit."""

    def __init__(self, message: str) -> None:
        """Builds an UdironError.

        Args:
            message: Short message indicating the underlying exception.
        """
        super().__init__()
        self.message = message


OutputEntryListIndexed: "TypeAlias" = List[Tuple["OutputEntry", float]]
"""A list of OutputEntries with position indices provided alongside each."""
OutputEntryList: "TypeAlias" = List["OutputEntry"]
"""A list of OutputEntries."""

InflectionSet: "TypeAlias" = FrozenSet[tfsli.Qid]

# The interfaces below rely on at least one Udiron type
# and thus *must* use forward references for those types.

CatenaZipperPopOutput: "TypeAlias" = Tuple["DependentEntry", "CatenaZipper"]
"""The output from CatenaZipper.pop."""
DependentListDetachOutput: "TypeAlias" = Tuple["DependentEntry", "DependentList"]

CoreArgumentMapping: "TypeAlias" = Dict[str, "CatenaZipper"]
"""A mapping from strings to related CatenaZippers."""
ScopeArgumentMapping: "TypeAlias" = Dict[str, List["CatenaZipper"]]
"""A mapping from strings to a list of related CatenaZippers."""

SelectFormsOutput: "TypeAlias" = Tuple[InflectionSet, "CatenaConfig"]
"""The output from a SelectForms function."""


class SelectForms(Protocol):
    """Type of function used in the first step of the Catena rendering process.

    Takes in a CatenaZipper (whose cursor is the Catena to be set up)
    and sets up necessary information for generating a lexeme form.

    The general setup for this function consists of checking whether
    the Catena's inflection set is empty *and* there is exactly one form in
    the Catena's lexeme that has the inflections in the inflection set.
    If both are the case, then this function does nothing; otherwise, it
    tries to *develop* a form based on the information in the Catena.
    The result from this form development must reside entirely in an inflection set
    and Catena configuration, which are returned by this function.

    This form development can take on different forms depending on the circumstances
    of the Catena--an entirely non-exhaustive list of possibilities is below:

    * In German, if a noun has only a case inflection but no number inflection,
        and if the noun is an 'instance of' 'plurale tantum', then the 'plural'
        inflection is added to the Catena's inflection set.
    * Also in German, rather than selecting an individual form on an adjective,
        an appropriate adjective ending is constructed based on whether the adjective is declinable,
        the inflections provided, and the type of inflection (strong, weak, mixed) that is needed
        based on context. This ending is then saved to the Catena's configuration, to be recalled
        in the German CollectForms function.
    * In Italian, the specific form of the definite article is chosen based on context,
        and this choice is saved to the Catena's configuration, to be recalled in the Italian
        CollectForms function.
    * In Breton, a similar process to the Italian example above is performed with its articles.
    * In Swedish, any inflections that conflict with the presence of 'definite' or 'superlative'
        in the inflection set are adjusted away.
    * In isiZulu, certain proclitics reflecting a particular noun class have multiple forms depending
        on the number of syllables of whatever they attach to, and the specific form needed of that
        proclitic is chosen here, to be recalled in the isiZulu CollectForms function.
    """

    def __call__(self, catena: "CatenaZipper") -> SelectFormsOutput:
        """Runs a SelectForms.

        Args:
            catena: Catena to select a form for.

        Returns:
            Modified inflection set and configuration.
        """


SelectFormsMapping: "TypeAlias" = MutableMapping[Language, SelectForms]


class DevelopForm(Protocol):
    """If during the SelectForms step a single form matching the current inflection set cannot be found, then this function may be called (if it is defined for a particular language) to adjust the current CatenaConfig and current inflection set so that an appropriate form is ultimately chosen during the CollectForms step."""

    def __call__(self, catena: "CatenaZipper") -> SelectFormsOutput:
        """Runs a DevelopForm.

        Args:
            catena: Catena to develop a form for.

        Returns:
            Modified inflection set and configuration.
        """


DevelopFormMapping: "TypeAlias" = MutableMapping[Language, DevelopForm]

CollectFormsOutput: "TypeAlias" = Tuple[OutputEntryListIndexed, int]
"""The output from a CollectForms function."""


class CollectForms(Protocol):
    """Type of function used in the second step of the Catena rendering process.

    Takes in a Catena (not a CatenaZipper!) and a starting index
    and returns a list of pairs (OutputEntry, number) where the numbers start from
    the provided starting index.

    Whereas it was possible in the first step to use context by visiting other nodes
    in the syntax tree to make decisions, given that the input was a CatenaZipper,
    this is not possible with this step. Here the objective is to produce a form representation
    by means of the information in just a Catena--no dependents and no parents.
    The decisions to be made in this step are thus likely to be less complex than those
    in the previous step, and if there is any information about the Catena that is not
    reflected in the form representation chosen and absolutely must be carried over to later steps,
    then it must be supplied in the OutputEntr(y|ies) it produces;
    see `OutputEntry <udiron.base.outputentry>`_ for what can be supplied.

    Just as there is variation in how forms are selected and developed in the first step,
    so too can form collection take on different forms depending on the circumstances
    of the Catena--an entirely non-exhaustive list of possibilities is below:

    * Across different languages, certain Catenae with certain properties trigger the addition,
        in this step, of hooks to be run during the SurfaceTransform step (see the 'transform_hook'
        parameter of OutputEntry for how they are used). These include the Bengali
        definiteness marker, the preposition 'in' and separable verb prefixes in German,
        the auxiliary verb and preposition 'na' in Igbo, and punctuation marks.
    * The forms selected in the previous step in the Breton, Italian, and isiZulu examples
        given for that step are here recalled and returned.
    * In German, an adjective ending obtained in the previous step is combined with
        the 'word stem' of the Catena's lexeme (based on the property of the same name).
    * In Swedish, the list of forms selectable based on information in the Catena is filtered
        to remove archaisms.
    """

    def __call__(self, catena: "udiron.base.catena.Catena", rel: tfsli.Qid, index: int, parent_index: int) -> CollectFormsOutput:
        """Runs a CollectForms.

        Args:
            catena: Catena to get a form from.
            rel: Relationship of the Catena to its parent.
            index: Current index of the current Catena.
            parent_index: Current index of the current Catena.

        Returns:
            OutputEntry-index pair, plus the index for the next Catena.
        """


CollectFormsMapping: "TypeAlias" = MutableMapping[Language, CollectForms]


class SurfaceJoin(Protocol):
    """Type of function used in the third step of the Catena rendering process.

    Takes in a list of (OutputEntry, number) pairs as well as the configuration of the
    root Catena and returns a list of OutputEntries.

    "Joins" the forms obtained by a CollectForms. This term is largely a holdover from
    when the entire Catena rendering process returned a single string, but the function
    of adding spaces to the output (in all languages for which some support existed,
    given that they all use spaces) at this stage has been retained.

    Depending on the language in question, this may or may not involve appending spaces
    to the surface representations of each OutputEntry:

    * In Sinitic scripts, Japanese, Thai, and Lao, spaces are not involved in joining
        words into a sentence.
    * In Tibetic scripts, the same mark used to separate syllables is used to separate
        words as well.
    * In Latin, an interpunct may be used as a word separator.
    * In the Ge'ez script, there is a dedicated word separator similar to a colon.

    In addition to "joining" forms, in those languages with capitalization this is applied
    at this stage.
    """

    def __call__(self, formlist: OutputEntryListIndexed, config: "CatenaConfig") -> OutputEntryList:
        """Runs a SurfaceJoin.

        Args:
            formlist: List of OutputEntry-index pairs.
            config: Configuration of the root of the Catena.

        Returns:
            List of "joined" OutputEntries.
        """


SurfaceJoinMapping: "TypeAlias" = MutableMapping[Language, SurfaceJoin]


class SurfaceTransform(Protocol):
    """Type of function used in the fourth step of the Catena rendering process.

    Performs transformations on the list of OutputEntries output by a SurfaceJoin.

    * ☞ Information: At this time there is exactly one defined SurfaceTransform
        applied to all languages. (See `OutputEntry.transform_hook <udiron.base.outputentry>`_ for
        information on what happens in it.)
        Your language need not also use this, however, especially if
        there is some handling at a surface level that cannot be encoded in hooks.
    * ⌨ Implementation detail: In earlier revisions of Udiron, this function would do
        the sorts of modifications that are now currently done inside hooks.
        These were achieved through the addition of control characters
        (from the end of Unicode Plane 16) at the boundaries of words in what is now CollectForms,
        and then running various regular expression replacements to transform them.
        It is this shift of functionality into hooks that has led to exactly one SurfaceTransform
        being defined at this time.
    """

    def __call__(self, joined: OutputEntryList, config: "CatenaConfig") -> OutputEntryList:
        """Runs a SurfaceTransform.

        Args:
            joined: List of OutputEntries "joined".
            config: Configuration of the root of the Catena.

        Returns:
            List of OutputEntries reflecting any actual joining of components.
        """


SurfaceTransformMapping: "TypeAlias" = MutableMapping[Language, SurfaceTransform]


class SurfaceTransformHook(Protocol):
    """Hook run during the SurfaceTransform step (or at least all current implementations of it).

    Every OutputEntry in the entry list is checked from left to right. If an entry defines a hook,
    then that entry and the ones adjacent to it are provided to that hook--the
    preceding one as the first argument, the current one as the second, and the following one as the third.
    The hook can then perform some transformation of the OutputEntries and return them,
    deleting any of the returned ones from the eventual output if they are empty,
    and continuing checks of the rest of the entry list.

    Some examples of hooks are those that remove spaces between components of a compound
    (or between a word and following punctuation, or at the end of a sentence),
    that contract certain combinations of preposition and definite article in some languages,
    and that perform phonological transformations at the boundaries of proclitics and other function words.
    """

    def __call__(
        self,
        left_entry: Optional["OutputEntry"],
        center_entry: "OutputEntry",
        right_entry: Optional["OutputEntry"],
    ) -> Tuple["OutputEntry", "OutputEntry", "OutputEntry"]:
        """Runs a SurfaceTransformHook.

        Args:
            left_entry: Current left OutputEntry.
            center_entry: Current center OutputEntry.
            right_entry: Current right OutputEntry.

        Returns:
            OutputEntries reflecting any joining of parts.
        """


class CollectFormsHooks(Protocol):
    """Type of function that selects a SurfaceTransformHook based on the form text and CatenaConfig provided and returns it."""

    def __call__(self, form_text: str, language: Language, config: "CatenaConfig") -> Tuple[Optional[SurfaceTransformHook], "CatenaConfig"]:
        """Runs a CollectFormsHooks.

        Args:
            form_text: Current surface representation of a particular Catena.
            language: Language being rendered into.
            config: Configuration of a particular Catena.

        Returns:
            Surface transform hook to be applied, if any, plus a potentially modified CatenaConfig.
        """


CollectFormsHooksMapping: "TypeAlias" = MutableMapping[Language, CollectFormsHooks]

__language_fallbacks__: Dict[Language, Sequence[Language]] = {}


def register_language_fallbacks(lang: Language, fallbacks: Sequence[Language]) -> None:
    """Sets the fallbacks for a particular Language to the sequence of Languages provided.

    Args:
        lang: Language to set fallbacks for.
        fallbacks: The fallbacks in question.
    """
    if fallbacks[-1] != langs.mul_:
        __language_fallbacks__[lang] = [lang, *fallbacks, langs.mul_]
    else:
        __language_fallbacks__[lang] = fallbacks


def get_language_fallbacks(lang: Language) -> Sequence[Language]:
    """Retrieves the list of language fallbacks for a particular Language.

    Args:
        lang: Language to find fallbacks for.

    Returns:
        List of fallback languages for that language.
    """
    return __language_fallbacks__.get(lang, [lang, langs.mul_])


class CatenaDict(TypedDict):
    """Represents the serialization of a Catena.

    Attributes:
        lexeme: Serialization of a lexeme, or merely its ID if it comes directly from Wikidata.
        language: Serialization of a language.
        sense: Serialization of a lexeme sense, or merely its ID if it comes directly from Wikidata.
        inflections: List of item IDs for inflections.
        config: Serialization of a contained CatenaConfig.
        dependents: Serialization of this Catena's dependents.
    """
    lexeme: Union[tfsli.Lid, tfsli.LexemeDict]
    language: tfsli.LanguageDict
    sense: Union[tfsli.LSid, tfsli.LexemeSenseDict]
    inflections: List[tfsli.Qid]
    config: "CatenaConfigDict"
    dependents: "DependentListDict"


ScopeArgumentMappingDict: "TypeAlias" = Dict[str, List[CatenaDict]]


class FunctionConfigDict(TypedDict):
    """Represents the serialization of a FunctionConfig.

    Attributes:
        strs: Mapping from strings to strings.
        entities: Mapping from strings to serializations of entities.
    """
    strs: List[Tuple[str, str]]
    entities: List[Tuple[str, "udiron.base.functionconfig.EntitySerialized"]]


class CatenaTrackingEntryDict(TypedDict):
    """Represents the serialization of a CatenaTrackingEntry.

    Attributes:
        constructor_type: Type of a Constructor.
        constructor_id: Identifier of that Constructor.
    """
    constructor_type: str
    constructor_id: str


def is_catenatrackingentrydict(arg: Dict[str, Any]) -> "TypeGuard[CatenaTrackingEntryDict]":
    """Checks that the keys expected for a catena tracking entry exist.

    Args:
        arg: Some dictionary.

    Returns:
        Whether this has the form of a CatenaTrackingEntry serialization.
    """
    return all(x in arg for x in ["constructor_type", "constructor_id"])


class CatenaTrackingDict(TypedDict):
    """Represents the serialization of a CatenaTracking.

    Attributes:
        entries: List of serializations of CatenaTrackingEntries.
    """
    entries: List[CatenaTrackingEntryDict]


class CatenaConfigDict(TypedDict):
    """Represents the serialization of a CatenaConfig.

    Attributes:
        config: Serialization of the contained FunctionConfig.
        scope_args: Serialization of the contained ScopeArgumentsMapping.
        tracking: Serialization of the contained CatenaTracking.
    """
    config: "FunctionConfigDict"
    scope_args: Dict[str, List[CatenaDict]]
    tracking: CatenaTrackingDict


class DependentEntryDict(TypedDict):
    """Represents the serialization of a DependentEntry.

    Attributes:
        catena: Dependent Catena.
        rel: Relationship of the Catena to its head.
    """
    catena: CatenaDict
    rel: tfsli.Qid


class DependentListDict(TypedDict):
    """Represents the serialization of a DependentList.

    Attributes:
        left_dependents: Logically-left dependents of a Catena.
        right_dependents: Logically-right dependents of a Catena.
    """
    left_dependents: List[DependentEntryDict]
    right_dependents: List[DependentEntryDict]
