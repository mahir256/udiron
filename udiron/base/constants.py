"""Holds constants for use with Udiron and tools that rely on it.

Constants that map to individual entity IDs (for items or for properties)
will not be individually documented here, as they are mostly defined for
convenience when reading the source code of this Python implementation.
(Sets or mappings of these constants may be so documented, though.)

* ⌨ Implementation detail: It is hoped that Wikifunctions will allow such
    constants to be displayed as labels within the source code editor
    for ease of reading, and then translate them back into entity IDs at
    save time, such that separate ZObjects for these constants don"t have to
    be defined.
"""

from tfsl.interfaces import Pid, Qid

TAB_WIDTH = 4
TAB_HYPHENS = "─" * (TAB_WIDTH - 1)
TAB_SPACES = " " * (TAB_WIDTH - 1)
ZWNJ = "\u200C"

archaism = Qid("Q181970")

part_of_speech = Qid("Q82042")
adjective = Qid("Q34698")
noun = Qid("Q1084")
proper_noun = Qid("Q147276")
verb = Qid("Q24905")
classifier = Qid("Q63153")
numeral = Qid("Q63116")
determiner = Qid("Q576271")
adposition = Qid("Q134316")
preposition = Qid("Q4833830")
postposition = Qid("Q161873")
pronoun = Qid("Q36224")
personal_pronoun = Qid("Q468801")
indefinite_article = Qid("Q3813849")
definite_article = Qid("Q2865743")
verbal_locution = Qid("Q10976085")
proclitic = Qid("Q108819306")
noun_prefix = Qid("Q104057461")
inflectional_suffix = Qid("Q109528443")
case_marker = Qid("Q119406032")
interjectional_locution = Qid("Q10319520")
thematic_relation = Qid("Q613930")
word = Qid("Q8171")
suffix = Qid("Q102047")
possessive_affix = Qid("Q804020")
possessive_suffix = Qid("Q1667071")
complement = Qid("Q1051175")
linking_morpheme = Qid("Q1472909")

separable_verb = Qid("Q3254028")
indeclinable_adjective = Qid("Q58961134")
indeclinable_noun = Qid("Q66364639")
verbo_nominal_syntagma = Qid("Q117606981")
perfective_verbal_expression = Qid("Q117605926")
endocentric_compound = Qid("Q61366396")

masculine = Qid("Q499327")
feminine = Qid("Q1775415")
neuter = Qid("Q1775461")
common_gender = Qid("Q1305037")
grammatical_gender_item = Qid("Q162378")

person = Qid("Q690940")
first_person = Qid("Q21714344")
second_person = Qid("Q51929049")
informal_second_person = Qid("Q56650485")
general_second_person = Qid("Q56650487")
third_person = Qid("Q51929074")
bengali_polite_form = Qid("Q56650512")

indefinite = Qid("Q53997857")
definite = Qid("Q53997851")

nominative = Qid("Q131105")
genitive = Qid("Q146233")
accusative = Qid("Q146078")
dative = Qid("Q145599")
locative = Qid("Q202142")
oblique = Qid("Q1233197")
partitive = Qid("Q857325")
inessive = Qid("Q282031")
elative = Qid("Q394253")
inelative = Qid("Q11023607")
illative = Qid("Q474668")
adessive = Qid("Q281954")
ablative = Qid("Q156986")
allative = Qid("Q655020")
essive = Qid("Q148465")
translative = Qid("Q950170")
abessive = Qid("Q319822")
instructive = Qid("Q1665275")
comitative = Qid("Q838581")
causal_final = Qid("Q11870653")
terminative = Qid("Q747019")
essive_formal = Qid("Q3827688")
essive_modal = Qid("Q3827703")
superessive = Qid("Q222355")
sublative = Qid("Q2120615")
delative = Qid("Q1183901")
construct_state = Qid("Q1641446")
direct_case = Qid("Q1751855")
postpositional_case = Qid("Q117226389")

attributive = Qid("Q4818723")

negation = Qid("Q1478451")

positive = Qid("Q3482678")
comparative = Qid("Q14169499")
superlative = Qid("Q1817208")

active_voice = Qid("Q1317831")
passive_voice = Qid("Q1194697")

infinitive = Qid("Q179230")
perfect_aspect = Qid("Q625420")
simple_present = Qid("Q3910936")
continuous_present = Qid("Q7240943")
perfect_present = Qid("Q1240211")
simple_past = Qid("Q1392475")
continuous_past = Qid("Q56650537")
pluperfect = Qid("Q623742")
habitual_past = Qid("Q75243920")
simple_future = Qid("Q96323395")
present_imperative = Qid("Q52434162")
future_imperative = Qid("Q75244800")
verbal_noun = Qid("Q1350145")
preterite = Qid("Q442485")
past_participle = Qid("Q12717679")
past_active_participle = Qid("Q16086106")
participle = Qid("Q814722")
word_stem = Qid("Q210523")
aorist = Qid("Q216497")
present_stem = Qid("Q5664392")
past_stem = Qid("Q5674715")

third_person_singular = Qid("Q51929447")
present_indicative = Qid("Q56682909")

sadhu_bhasa = Qid("Q20613396")
chalit_bhasa = Qid("Q75242466")

syntactic_root = Qid("Q128048896")
nom_attribute = Qid("Q12721176")
adj_attribute = Qid("Q10401368")
adv_attribute = Qid("Q10401562")
gen_attribute = Qid("Q10503864")
subject = Qid("Q164573")
copula = Qid("Q28129955")
numeral_adjective = Qid("Q55951821")
subordinate_conjunction = Qid("Q11655558")
punctuation = Qid("Q82622")
expletive = Qid("Q188274")
auxiliary_verb = Qid("Q465800")
infinitive_marker = Qid("Q85103750")
direct_object = Qid("Q2990574")
indirect_object = Qid("Q1094061")
oblique_object = Qid("Q117795533")
light_verb_construction = Qid("Q1474669")
verbal_particle = Qid("Q10714103")
adverbial = Qid("Q380012")
adverbial_clause = Qid("Q132120")
conditional_clause = Qid("Q12738640")
sentence = Qid("Q41796")
modal_adverb = Qid("Q1941737")

mode_complement = Qid("Q12724475")
location_complement = Qid("Q12724480")
time_complement = Qid("Q12724479")
stative_location_complement = Qid("Q3685162")
source_location_complement = Qid("Q3685157")
intermediate_location_complement = Qid("Q3685158")
destination_location_complement = Qid("Q3685156")
object_complement = Qid("Q10605893")

advanced_tongue_root = Qid("Q110629567")
retracted_tongue_root = Qid("Q110629568")
bound_morpheme = Qid("Q985836")
free_morpheme = Qid("Q4115359")

soft_mutation = Qid("Q56648699")
hard_mutation = Qid("Q97130345")
aspirate_mutation = Qid("Q56648701")
no_mutation = Qid("Q101252532")

emphasis = Qid("Q65044042")

tense = Qid("Q177691")
present = Qid("Q192613")
past = Qid("Q1994301")
near_past = Qid("Q113326922")
remote_past = Qid("Q113326813")
nonpast = Qid("Q16916993")
near_future = Qid("Q104872742")
remote_future = Qid("Q113565070")
future = Qid("Q501405")
hesternal = Qid("Q11513930")
hodiernal = Qid("Q5876223")
crastinal = Qid("Q5182530")
nonremote_tense = Qid("Q113326559")

number = Qid("Q104083")
singular = Qid("Q110786")
paucal = Qid("Q489410")
plural = Qid("Q146786")
plurale_tantum = Qid("Q138246")

aspect = Qid("Q208084")
habitual = Qid("Q5636904")
inchoative = Qid("Q1423398")
cessative = Qid("Q17027342")
perfective = Qid("Q1424306")
continuous = Qid("Q12721117")
progressive = Qid("Q1423674")

mood = Qid("Q184932")
indicative = Qid("Q682111")
subjunctive = Qid("Q473746")
desiderative = Qid("Q1200631")
jussive = Qid("Q462367")
optative = Qid("Q527205")
imperative = Qid("Q22716")
hortative = Qid("Q5906629")
dubitative = Qid("Q1263049")
permissive = Qid("Q4351483")
renarrative = Qid("Q3332616")

honorific = Qid("Q5897044")
informal = Qid("Q77768943")
familiar = Qid("Q110891860")
formal = Qid("Q77768790")

misspelling = Qid("Q1984758")

wikidatan = Qid("Q55872899")
not_yet_determined = Qid("Q59496158")

male = Qid("Q6581097")
cis_male = Qid("Q15145778")
trans_male = Qid("Q2449503")
female = Qid("Q6581072")
cis_female = Qid("Q15145779")
trans_female = Qid("Q1052281")

sex_or_gender = Pid("P21")
country_of_citizenship = Pid("P27")
instance_of = Pid("P31")
occupation = Pid("P106")
stated_in = Pid("P248")
has_part = Pid("P527")
date_of_birth = Pid("P569")
date_of_death = Pid("P570")
given_name = Pid("P735")
ipa_pronunciation = Pid("P898")
series_ordinal = Pid("P1545")
does_not_have_part = Pid("P3113")
object_has_role = Pid("P3831")
nature_of_statement = Pid("P5102")
item_for_this_sense = Pid("P5137")
grammatical_gender = Pid("P5185")
word_stem_property = Pid("P5187")
derived_from_lexeme = Pid("P5191")
combines_lexeme = Pid("P5238")
object_form = Pid("P5548")
requires_grammatical_feature = Pid("P5713")
translation = Pid("P5972")
synonym = Pid("P5973")
antonym = Pid("P5974")
troponym_of = Pid("P5975")
object_sense = Pid("P5980")
location_of_sense_usage = Pid("P6084")
language_style = Pid("P6191")
demonym_of = Pid("P6271")
hyperonym = Pid("P6593")
pronunciation = Pid("P7243")
pertainym = Pid("P8471")
field_of_usage = Pid("P9488")
head_relationship = Pid("P9763")
head_position = Pid("P9764")
predicate_for = Pid("P9970")
has_thematic_relation = Pid("P9971")
grammatical_person = Pid("P11053")
grammatical_number = Pid("P11054")
before_feature = Pid("P11950")
after_feature = Pid("P11951")
before_form = Pid("P11952")
after_form = Pid("P11953")
derived_lexeme_label_item = Pid("Q116943359")

agent = Qid("Q392648")
patient = Qid("Q170212")
experiencer = Qid("Q1242505")
focus = Qid("Q1435289")
addressee = Qid("Q19720921")
recipient = Qid("Q20820253")
topic = Qid("Q22338337")
source = Qid("Q31464082")
location = Qid("Q109377685")
goal = Qid("Q109405570")
instrument = Qid("Q109564569")
stimulus = Qid("Q109566760")
cause = Qid("Q2574811")
possessor = Qid("Q109574863")
possessed = Qid("Q118835696")
class_item = Qid("Q16889133")

final_dag = Qid("Q30013662")

linguistic_style = Qid("Q2313235")
geolect = Qid("Q3123468")
colloquial_language = Qid("Q901711")
slang = Qid("Q8102")
pejorative = Qid("Q545779")
human = Qid("Q5")

text_formatting = Qid("Q2407816")
punctuation_mark = Qid("Q1668151")
opening_bracket = Qid("Q111971734")
closing_bracket = Qid("Q111971808")
inquiry_point = Qid("Q0")
word_form = Qid("Q1224852")
wikibase_form = Qid("Q54285143")
wikibase_sense = Qid("Q54285715")
adjectival_suffix = Qid("Q104051989")
contraction = Qid("Q126473")
compounding = Qid("Q29445010")
omission = Qid("Q110240047")
index_number = Qid("Q1738991")
v2_word_order = Qid("Q1516105")
occurrence_time = Qid("Q7805404")
reference_time = Qid("Q2875947")
now = Qid("Q18488805")
exclusion = Qid("Q110240047")
writing_system = Qid("Q8192")
origin_of_speech = Qid("Q7102483")

declaration = Qid("Q1183659")
timestamp_computing = Qid("Q7806609")
spatial_relation = Qid("Q2178623")
identifier = Qid("Q1773882")
signal = Qid("Q2150504")
concept = Qid("Q151885")
predicate = Qid("Q179080")
speaker = Qid("Q2324479")
listener = Qid("Q108606601")
subordination = Qid("Q2031017")
context = Qid("Q196626")
command = Qid("Q1079196")
backreference = Qid("Q126932816")
simultaneity = Qid("Q1530412")
configuration_option = Qid("Q105029446")
name_binding = Qid("Q16928028")
system_configuration = Qid("Q7663702")
shortest_path_tree = Qid("Q4919350")
combined_in_lexeme = Qid("Q116943343")

postconsonantal_form = Qid("Q115551160")
postvocalic_form = Qid("Q115551161")
syllable = Qid("Q8188")
open_syllable = Qid("Q5997175")
closed_syllable = Qid("Q25495380")
monosyllabic_word = Qid("Q19050655")
polysyllable = Qid("Q91741368")
short_form = Qid("Q96406487")

front_vowel = Qid("Q5505949")
back_vowel = Qid("Q853589")
front_unrounded_vowel = Qid("Q121455329")
front_rounded_vowel = Qid("Q5505896")
back_rounded_vowel = Qid("Q121455331")
back_unrounded_vowel = Qid("Q121455330")
voiceless_final_consonant = Qid("Q123170485")
voiced_final_consonant = Qid("Q123198442")
initial_vowel = Qid("Q121301470")
initial_consonant = Qid("Q121301466")
surface_representation = Qid("Q124100534")
lexical_representation = Qid("Q124100536")
predicative = Qid("Q1931259")
underspecification = Qid("Q7883767")

noun_class = Qid("Q1598075")
number_inflection = Qid("Q113641362")

novalue = Qid("Q108474139")
somevalue = Qid("Q53569537")
nonexistent_entity = Qid("Q64728693")
undetermined_language = Qid("Q22282914")
uncoded_language = Qid("Q22283016")
no_linguistic_content = Qid("Q22282939")

vowel_harmony = Qid("Q147137")
backwards = Qid("Q16938807")
forwards = Qid("Q16938806")

hebrew_script = Qid("Q33513")

# of note for NCB languages
nc_1 = Qid("Q113331807")
nc_1a = Qid("Q113195674")
nc_2 = Qid("Q113380991")
nc_2a = Qid("Q113195677")
nc_3 = Qid("Q113194639")
nc_3a = Qid("Q113195763")
nc_4 = Qid("Q113194715")
nc_5 = Qid("Q113383619")
nc_6 = Qid("Q113383630")
nc_7 = Qid("Q113383641")
nc_8 = Qid("Q113383650")
nc_9 = Qid("Q113383660")
nc_9a = Qid("Q113383667")
nc_10 = Qid("Q113383679")
nc_11 = Qid("Q113383687")
nc_12 = Qid("Q113383692")
nc_13 = Qid("Q113383694")
nc_14 = Qid("Q113383695")
nc_15 = Qid("Q113383696")
nc_16 = Qid("Q113383699")
nc_17 = Qid("Q113383703")
true_adjective = Qid("Q65453883")
relative_adjective = Qid("Q115388551")

proximal = Qid("Q79377411")
medial = Qid("Q106548921")
distal = Qid("Q79377486")

# Some sets of the above constants defined for convenience.

three_person_set = {first_person, second_person, third_person}
person_set = {first_person, second_person, third_person, informal_second_person, general_second_person, bengali_polite_form}
"""Set of grammatical persons."""
past_tense_set = {past, hesternal, near_past, remote_past}
"""Set of tenses that refer to a time before the current time."""
present_tense_set = {present, hodiernal}
"""Set of tenses that refer to a time at or around the current time."""
future_tense_set = {future, crastinal, near_future, remote_future}
"""Set of tenses that refer to a time after the current time."""

non_formal_set = {informal, familiar}
"""Set of formality levels that are not considered to be "formal"."""

two_number_set = {singular, plural}
non_singular_grammatical_numbers = {plural, paucal}
"""Set of formality levels that are not considered to be "singular"."""

ncb_noun_classes = {nc_1, nc_1a, nc_2, nc_2a, nc_3, nc_3a, nc_4, nc_5, nc_6, nc_7, nc_8, nc_9, nc_9a, nc_10, nc_11, nc_12, nc_13, nc_14, nc_15, nc_16, nc_17}
"""Set of Niger-Congo B noun classes."""

ncb_singulars_to_plurals = {
    nc_1: nc_2,
    nc_1a: nc_2a,
    nc_3a: nc_2a,
    nc_3: nc_4,
    nc_5: nc_6,
    nc_7: nc_8,
    nc_9a: nc_6,
    nc_9: nc_10,
    nc_11: nc_10,
}
"""Mapping of the most common Niger-Congo B noun class of a singular noun
    to the noun class of its corresponding plural.
        """

duonominal_construction = Qid("Q124316813")
classificational_construction = Qid("Q124316841")
equational_construction = Qid("Q124316867")
characterizational_construction = Qid("Q124316943")
specificational_construction = Qid("Q124317005")
attributional_construction = Qid("Q124317331")
locative_construction = Qid("Q113623710")
predlocative_construction = Qid("Q124317387")
existential_construction = Qid("Q124317425")
possessive = Qid("Q2105891")
possession = Qid("Q3543662")
predpossessive_construction = Qid("Q124317525")
appertentive_construction = Qid("Q124317532")
hyparctic_construction = Qid("Q124317561")
