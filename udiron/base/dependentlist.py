"""Holds the DependentList class and functions used with it."""

from typing import TYPE_CHECKING, List, NamedTuple, Optional

from udiron.base.dependententry import DependentEntry, build_dependententry

if TYPE_CHECKING:
    import tfsl.interfaces as tfsli

    import udiron.base.interfaces as i


class DependentList(NamedTuple):
    """List of dependents of a Catena.

    The split into left and right originates in a Catena generally consistently appearing
    after all of its left dependents and before all of its right dependents.

    Attributes:
        left_dependents: Dependents of the Catena logically to the left of it.
        right_dependents: Dependents of the Catena logically to the right of it.
    """
    left_dependents: List[DependentEntry]
    right_dependents: List[DependentEntry]

    def clone(
        self,
        left_dependents: Optional[List[DependentEntry]] = None,
        right_dependents: Optional[List[DependentEntry]] = None,
    ) -> "DependentList":
        """Clones a DependentList while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the DependentList
        is preserved in the output.

        Args:
            left_dependents: Left dependents to replace the current ones.
            right_dependents: Right dependents to replace the current ones.

        Returns:
            DependentList like the original but with fields modified as per the provided arguments.
        """
        old_left, old_right = self
        return DependentList(
            old_left if left_dependents is None else left_dependents,
            old_right if right_dependents is None else right_dependents,
        )

    def __repr__(self) -> str:
        """Produces string representation of the DependentList.

        Returns:
            String representation of the DependentList.
        """
        return "(" + ", ".join([repr(dependent) for dependent in self.left_dependents]) + " | " + ", ".join([repr(dependent) for dependent in self.right_dependents]) + ")"

    # Actual implementations to test

    def get_dependent_index_left(self, target_dependent: DependentEntry) -> int:
        """Gets the index of a particular dependent among left dependents.

        Args:
            target_dependent: Dependent to search for among left dependents.

        Returns:
            Index of the provided target dependent among left dependents.
        """
        return self.left_dependents.index(target_dependent)

    def get_dependent_index_right(self, target_dependent: DependentEntry) -> int:
        """Gets the index of a particular dependent among right dependents.

        Args:
            target_dependent: Dependent to search for among right dependents.

        Returns:
            Index of the provided target dependent among right dependents.
        """
        return self.right_dependents.index(target_dependent)

    def get_rel_index_left(self, target_rel: "tfsli.Qid") -> int:
        """Gets the index of a dependent with a particular relation among left dependents.

        Args:
            target_rel: Relationship to search for among left dependents.

        Returns:
            Index of the leftmost dependent with the provided relationship among left dependents.
        """
        return next(filter(lambda pair: pair[1].rel == target_rel, enumerate(self.left_dependents)))[0]

    def get_rel_index_right(self, target_rel: "tfsli.Qid") -> int:
        """Gets the index of a dependent with a particular relation among right dependents.

        Args:
            target_rel: Relationship to search for among right dependents.

        Returns:
            Index of the leftmost dependent with the provided relationship among right dependents.
        """
        return next(filter(lambda pair: pair[1].rel == target_rel, enumerate(self.right_dependents)))[0]

    def has_rel(self, rel: "tfsli.Qid") -> bool:
        """Checks a DependentList for a relationship.

        Args:
            rel: The relation to check for.

        Returns:
            Whether a dependent with the provided relation is present among dependents.
        """
        has_on_left = any(entry.rel == rel for entry in self.left_dependents)
        has_on_right = any(entry.rel == rel for entry in self.right_dependents)
        return has_on_left or has_on_right

    def attach_leftmost(self, new_dependent: DependentEntry) -> "DependentList":
        """Attaches the provided DependentEntry as the leftmost dependent.

        Args:
            new_dependent: New DependentEntry to add.

        Returns:
            Adjusted DependentList.
        """
        return self.clone(left_dependents=[new_dependent] + self.left_dependents)

    def attach_rightmost(self, new_dependent: DependentEntry) -> "DependentList":
        """Attaches the provided DependentEntry as the rightmost dependent.

        Args:
            new_dependent: New DependentEntry to add.

        Returns:
            Adjusted DependentList.
        """
        return self.clone(right_dependents=self.right_dependents + [new_dependent])

    def attach_left_of_root(self, new_dependent: DependentEntry) -> "DependentList":
        """Attaches the provided DependentEntry as the rightmost among left dependents.

        Args:
            new_dependent: New DependentEntry to add.

        Returns:
            Adjusted DependentList.
        """
        return self.clone(left_dependents=self.left_dependents + [new_dependent])

    def attach_right_of_root(self, new_dependent: DependentEntry) -> "DependentList":
        """Attaches the provided DependentEntry as the leftmost among right dependents.

        Args:
            new_dependent: New DependentEntry to add.

        Returns:
            Adjusted DependentList.
        """
        return self.clone(right_dependents=[new_dependent] + self.right_dependents)

    def attach_after_index_left(self, new_dependent: DependentEntry, target: int) -> "DependentList":
        """Attaches the provided DependentEntry after the left dependent with the provided target index.

        Args:
            new_dependent: New DependentEntry to add.
            target: Index of left DependentEntry after which the new one should be added.

        Returns:
            Adjusted DependentList.

        Raises:
            ValueError: if the target is out of the range of left dependent indices.
        """
        if target < 0 or target > len(self.left_dependents):
            raise ValueError("Target index out of range for left dependents")
        return self.clone(left_dependents=self.left_dependents[:target + 1] + [new_dependent] + self.left_dependents[target + 1:])

    def attach_after_index_right(self, new_dependent: DependentEntry, target: int) -> "DependentList":
        """Attaches the provided DependentEntry after the right dependent with the provided target index.

        Args:
            new_dependent: New DependentEntry to add.
            target: Index of right DependentEntry after which the new one should be added.

        Returns:
            Adjusted DependentList.

        Raises:
            ValueError: if the target is out of the range of left dependent indices.
        """
        if target < 0 or target > len(self.right_dependents):
            raise ValueError("Target index out of range for right dependents")
        return self.clone(right_dependents=self.right_dependents[:target + 1] + [new_dependent] + self.right_dependents[target + 1:])

    def attach_before_index_left(self, new_dependent: DependentEntry, target: int) -> "DependentList":
        """Attaches the provided DependentEntry before the left dependent with the provided target index.

        Args:
            new_dependent: New DependentEntry to add.
            target: Index of left DependentEntry before which the new one should be added.

        Returns:
            Adjusted DependentList.

        Raises:
            ValueError: if the target is out of the range of left dependent indices.
        """
        if target < 0 or target > len(self.left_dependents):
            raise ValueError("Target index out of range for left dependents")
        elif target == 0:
            return self.attach_leftmost(new_dependent)
        return self.clone(left_dependents=self.left_dependents[:target] + [new_dependent] + self.left_dependents[target:])

    def attach_before_index_right(self, new_dependent: DependentEntry, target: int) -> "DependentList":
        """Attaches the provided DependentEntry before the left dependent with the provided target index.

        Args:
            new_dependent: New DependentEntry to add.
            target: Index of right DependentEntry before which the new one should be added.

        Returns:
            Adjusted DependentList.

        Raises:
            ValueError: if the target is out of the range of left dependent indices.
        """
        if target < 0 or target > len(self.right_dependents):
            raise ValueError("Target index out of range for right dependents")
        elif target == 0:
            return self.attach_right_of_root(new_dependent)
        return self.clone(right_dependents=self.right_dependents[:target] + [new_dependent] + self.right_dependents[target:])

    def detach_leftmost(self) -> "i.DependentListDetachOutput":
        """Detaches from a DependentList.

        Returns:
            Leftmost dependent and a DependentList without it.

        Raises:
            ValueError: if there are no left dependents.
        """
        if len(self.left_dependents) == 0:
            raise ValueError("No left dependents in dependent list")
        return self.left_dependents[0], self.clone(left_dependents=self.left_dependents[1:])

    def detach_rightmost(self) -> "i.DependentListDetachOutput":
        """Detaches from a DependentList.

        Returns:
            Rightmost dependent and a DependentList without it.

        Raises:
            ValueError: if there are no right dependents.
        """
        if len(self.right_dependents) == 0:
            raise ValueError("No right dependents in dependent list")
        return self.right_dependents[-1], self.clone(right_dependents=self.right_dependents[:-1])

    def detach_left_of_root(self) -> "i.DependentListDetachOutput":
        """Detaches from a DependentList.

        Returns:
            Rightmost among left dependents and a DependentList without it.

        Raises:
            ValueError: if there are no left dependents.
        """
        if len(self.left_dependents) == 0:
            raise ValueError("No left dependents in dependent list")
        return self.left_dependents[-1], self.clone(left_dependents=self.left_dependents[:-1])

    def detach_right_of_root(self) -> "i.DependentListDetachOutput":
        """Detaches from a DependentList.

        Returns:
            Leftmost among right dependents and a DependentList without it.

        Raises:
            ValueError: if there are no right dependents.
        """
        if len(self.right_dependents) == 0:
            raise ValueError("No right dependents in dependent list")
        return self.right_dependents[0], self.clone(right_dependents=self.right_dependents[1:])

    def detach_at_index_left(self, target: int) -> "i.DependentListDetachOutput":
        """Detaches from a DependentList.

        Args:
            target: Index of the left dependent to detach.

        Returns:
            Left DependentEntry at the provided target index and a DependentList without it.

        Raises:
            IndexError: if the target is out of the range of left dependent indices.
        """
        if target < 0 or target >= len(self.left_dependents):
            raise IndexError("Target index out of range for left dependents")
        return self.left_dependents[target], DependentList(self.left_dependents[:target] + self.left_dependents[target + 1:], self.right_dependents)

    def detach_at_index_right(self, target: int) -> "i.DependentListDetachOutput":
        """Detaches from a DependentList.

        Args:
            target: Index of the right dependent to detach.

        Returns:
            Right DependentEntry at the provided target index and a DependentList without it.

        Raises:
            IndexError: if the target is out of the range of right dependent indices.
        """
        if target < 0 or target >= len(self.right_dependents):
            raise IndexError("Target index out of range for right dependents")
        return self.right_dependents[target], DependentList(self.left_dependents, self.right_dependents[:target] + self.right_dependents[target + 1:])

    def __jsonout__(self) -> "i.DependentListDict":
        """Produces a JSON serialization of this DependentList.

        Returns:
            JSON serialization of this DependentList.
        """
        return {
            "left_dependents": [x.__jsonout__() for x in self.left_dependents],
            "right_dependents": [x.__jsonout__() for x in self.right_dependents],
        }


def build_dependentlist(list_in: "i.DependentListDict") -> DependentList:
    """Builds a DependentList from a JSON serialization.

    Args:
        list_in: JSON serialization of a DependentList.

    Returns:
        New DependentList.
    """
    return DependentList(
        [build_dependententry(entry) for entry in list_in["left_dependents"]],
        [build_dependententry(entry) for entry in list_in["right_dependents"]],
    )
