"""Holds the FunctionConfig class."""

import contextlib
import json
from datetime import datetime
from typing import (Any, Dict, FrozenSet, List, Literal, NamedTuple, Optional,
                    Union, overload)

import tfsl.interfaces as tfsli
import tfsl.item
import tfsl.lexeme
import tfsl.lexemeform
import tfsl.lexemesense
from tfsl import Item, Lexeme, LexemeForm, LexemeSense

import udiron.base.constants as c
import udiron.base.interfaces as i
from udiron.base.catenatracking import CatenaTrackingEntry


class StrKey(NamedTuple):
    """Represents a key in a FunctionConfig mapped to a string value--that is, any type which is underlyingly a string, to include things like Qids, LSids, and LFSids.

    Integer values may be stored by converting them with str() and int().
    Float values may be stored by converting them with hex() and float.from_hex().

    Attributes:
        key: The key in question.
        val: Type of the value in question.
    """
    key: str
    val: Literal["str"] = "str"

    def __call__(self, arg: str) -> "FunctionConfig":
        """Generates a single-entry FunctionConfig.

        Args:
            arg: Entity to set in the FunctionConfig.

        Returns:
            FunctionConfig with one entry with the key set to the provided argument.
        """
        return FunctionConfig({self.key: arg}, {})


class StringSet(NamedTuple):
    """Set of strings. (Who knows how Wikifunctions will check whether an object is an instance of a set of strings?)

    Attributes:
        strs: The strings themselves.
    """
    strs: FrozenSet[str]

    def append(self, thing: str) -> "StringSet":
        """Adds to the set of strings.

        Args:
            thing: String to add.

        Returns:
            StringSet reflecting the newly added string.
        """
        return StringSet(self.strs | frozenset([thing]))

    def has(self, thing: object) -> bool:
        """Checks whether a thing is in the set of strings.

        Args:
            thing: Thing to check for.

        Returns:
            Whether the thing is in the set of strings.
        """
        if isinstance(thing, str):
            return thing in self.strs
        return False


class ItemList(NamedTuple):
    """List of Items. (Who knows how Wikifunctions will check whether an object is an instance of a list of Items?)

    Attributes:
        items: The items themselves.
    """
    items: List[Item]

    def append(self, thing: Item) -> "ItemList":
        """Adds to the list of Items.

        Args:
            thing: Item to add.

        Returns:
            ItemList reflecting the newly added Item.
        """
        return ItemList(self.items + [thing])

    def has(self, thing: object) -> bool:
        """Checks for a thing is in the list of Items.

        Args:
            thing: Thing to check for.

        Returns:
            Whether the thing is in the list of Items.
        """
        if isinstance(thing, Item):
            return thing in self.items
        return False

    def length(self) -> int:
        """Checks the length of the list of Items.

        Returns:
            How long the list is.
        """
        return len(self.items)


Entity = Union[datetime, Item, Lexeme, LexemeForm, LexemeSense, ItemList, StringSet, CatenaTrackingEntry]
EntitySerialized = Union[str, tfsli.ItemDict, tfsli.LexemeDict, tfsli.LexemeFormDict, tfsli.LexemeSenseDict, i.CatenaTrackingEntryDict]


class EntityKey(NamedTuple):
    """Represents a key in a FunctionConfig mapped to an Entity value.

    The types represented by an EntityKey must be uniquely resolvable
    with a Python isinstance() check--no one of those types can be
    a subclass of any of the others.

    Attributes:
        key: The key in question.
        val: Type of the value in question.
    """
    key: str
    val: Literal["entity"] = "entity"

    def __call__(self, arg: Entity) -> "FunctionConfig":
        """Generates a single-entry FunctionConfig.

        Args:
            arg: Entity to set in the FunctionConfig.

        Returns:
            FunctionConfig with one entry with the key set to the provided argument.
        """
        return FunctionConfig({}, {self.key: arg})


FCKey = Union[StrKey, EntityKey]


class FunctionConfig(NamedTuple):
    """General container for configurations to Udiron functions.

    Attributes:
        strs: A mapping from strings to other strings.
        entities: A mapping from strings to entities that may hold Statements.
    """
    strs: Dict[str, str]
    entities: Dict[str, Entity]

    @classmethod
    def new(cls) -> "FunctionConfig":
        """Essentially a custom constructor for the FunctionConfig class.

        Returns:
            New FunctionConfig.
        """
        return cls({}, {})

    @overload
    def get(self, key: EntityKey) -> Optional[Entity]: ...
    @overload
    def get(self, key: StrKey) -> Optional[str]: ...

    def get(self, key: Any) -> Optional[Any]:
        """Gets the value associated with a key.

        Args:
            key: Key to be looked for.

        Returns:
            Value associated with the key in this FunctionConfig, if that key is present.

        Raises:
            KeyError: if the provided key is not valid for a FunctionConfig.
        """
        if isinstance(key, StrKey):
            return self.strs.get(key.key)
        elif isinstance(key, EntityKey):
            return self.entities.get(key.key)
        raise KeyError(f"{type(key)} is not a valid key for a FunctionConfig")

    def __len__(self) -> int:
        """Calculates the number of entries in this FunctionConfig.

        Returns:
            Size of this FunctionConfig.
        """
        return len(self.strs) + len(self.entities)

    def merge(self, other: "FunctionConfig") -> "FunctionConfig":
        """Merges information into this FunctionConfig from the other FunctionConfig.

        Args:
            other: FunctionConfig to merge into this one.

        Returns:
            FunctionConfig reflecting the newly merged information.
        """
        return FunctionConfig({**self.strs, **other.strs}, {**self.entities, **other.entities})

    def has(self, key: Union[EntityKey, StrKey]) -> bool:
        """Checks for the presence of a key.

        Args:
            key: Key to be checked for.

        Returns:
            Whether the provided key is in this FunctionConfig.

        Raises:
            KeyError: if the key in question cannot be checked for.
        """
        if isinstance(key, StrKey):
            return key.key in self.strs
        elif isinstance(key, EntityKey):
            return key.key in self.entities
        raise KeyError(f"Can't check for {type(key)} in FunctionConfig")

    def __or_handling(self, other: Any) -> "FunctionConfig":
        """Dispatches the 'or' operator.

        Args:
            other: Right-hand side of the operator.

        Returns:
            FunctionConfig reflecting newly merged information.

        Raises:
            NotImplementedError: if the other argument cannot be merged with this FunctionConfig.
        """
        if other is None:
            return self
        raise NotImplementedError(f"Can't merge {type(other)} with FunctionConfig")

    def __or__(self, other: Any) -> "FunctionConfig":
        """Merges a FunctionConfig with the operand to its right.

        Args:
            other: Some other operand.

        Returns:
            Merged FunctionConfig.
        """
        if isinstance(other, FunctionConfig):
            return self.merge(other)
        return self.__or_handling(other)

    def __ror__(self, other: Any) -> "FunctionConfig":
        """Merges a FunctionConfig with the operand to its left.

        Args:
            other: Some other operand.

        Returns:
            Merged FunctionConfig.
        """
        if isinstance(other, FunctionConfig):
            return other.merge(self)
        return self.__or_handling(other)

    @overload
    def set(self, key: StrKey, value: str) -> "FunctionConfig": ...
    @overload
    def set(self, key: EntityKey, value: Entity) -> "FunctionConfig": ...

    def set(self, key: Any, value: Any) -> "FunctionConfig":
        """Sets a key-value pair in the FunctionConfig.

        Args:
            key: Key into the FunctionConfig.
            value: Value to be set for that key.

        Returns:
            FunctionConfig reflecting the set key.

        Raises:
            TypeError: if the type of the value does not match the type reflected by the key.
        """
        new_strs, new_entities = self
        if isinstance(key, EntityKey):
            if not isinstance(value, (datetime, Item, Lexeme, LexemeForm, LexemeSense, StringSet, CatenaTrackingEntry)):
                raise TypeError("Assigning non-Entity to Entity key in FunctionConfig")
            new_entities = self.entities.copy()
            new_entities.update({key.key: value})
        elif isinstance(key, StrKey):
            if not isinstance(value, str):
                raise TypeError("Assigning non-string to string key in FunctionConfig")
            new_strs = self.strs.copy()
            new_strs.update({key.key: value})
        return FunctionConfig(new_strs, new_entities)

    def unset(self, key: Union[StrKey, EntityKey]) -> "FunctionConfig":
        """Removes a key-value pair from the FunctionConfig.

        Args:
            key: Key to remove.

        Returns:
            FunctionConfig reflecting the removed key.
        """
        new_strs, new_entities = self
        if isinstance(key, StrKey):
            new_strs = self.strs.copy()
            with contextlib.suppress(KeyError):
                del new_strs[key.key]
        elif isinstance(key, EntityKey):
            new_entities = self.entities.copy()
            with contextlib.suppress(KeyError):
                del new_entities[key.key]
        return FunctionConfig(new_strs, new_entities)

    def __jsonout__(self) -> i.FunctionConfigDict:
        """Produces a JSON serialization of this FunctionConfig.

        Returns:
            JSON serialization of this FunctionConfig.
        """
        return {
            "strs": [(k, v) for k, v in self.strs.items()],
            "entities": [(k, jsonout_entity(v)) for k, v in self.entities.items()],
        }

    def __hash__(self) -> int:
        """Produces a hash for this FunctionConfig.

        Returns:
            Hash for this FunctionConfig.
        """
        return hash(
            frozenset(
                [(k, v) for k, v in self.strs.items()] + [(k, jsonout_entity(v)) for k, v in self.entities.items()],
            ),
        )


def jsonout_entity(v: Entity) -> EntitySerialized:
    """Serializes an entity.

    Args:
        v: Entity to serialize.

    Returns:
        JSON serialization of the entity.
    """
    if isinstance(v, datetime):
        return v.isoformat()
    elif isinstance(v, StringSet):
        return json.dumps(list(v.strs))
    elif isinstance(v, ItemList):
        return json.dumps([item.__jsonout__() for item in v.items])
    return v.__jsonout__()


def build_entity(v: EntitySerialized) -> Entity:
    """Builds an Entity from a JSON serialization.

    Args:
        v: JSON serialization of an Entity.

    Returns:
        New Entity.

    Raises:
        ValueError: if for some reason the JSON doesn't deserialize into an Entity.
    """
    if isinstance(v, str):
        try:
            deserialized = json.loads(v)
            if isinstance(deserialized, list):
                if isinstance(deserialized[0], str):
                    return StringSet(deserialized)
                return ItemList([build_entity(thing) for thing in deserialized])
            raise ValueError("JSON-deserializable entity does not have recognized type")
        except json.decoder.JSONDecodeError:
            return datetime.fromisoformat(v)
    elif i.is_catenatrackingentrydict(v):
        return CatenaTrackingEntry.build(v)
    elif tfsli.is_entitypublishedsettings(v):
        if tfsli.is_itemdict(v):
            return tfsl.item.build_item(v)
        elif tfsli.is_lexemedict(v):
            return tfsl.lexeme.build_lexeme(v)
        elif tfsli.is_lexemeformdict(v):
            return tfsl.lexemeform.build_form(v)
        elif tfsli.is_lexemesensedict(v):
            return tfsl.lexemesense.build_sense(v)
    raise ValueError(f"Unexpected entity with keys {list(v.keys())} found!")


def build_functionconfig(config_in: i.FunctionConfigDict) -> FunctionConfig:
    """Builds a FunctionConfig from a JSON serialization.

    Args:
        config_in: JSON serialization of a FunctionConfig.

    Returns:
        New FunctionConfig.
    """
    return FunctionConfig(
        {**config_in["strs"]},
        {k: build_entity(v) for (k, v) in config_in["entities"]},
    )


class FCKeys:
    """Provides some keys that can be used in a FunctionConfig."""

    def __init__(self) -> None:
        """Defines some FunctionConfig keys used in Udiron and elsewhere."""
        self.definite = StrKey(c.definite)
        self.emphasis = StrKey(c.emphasis)
        self.closing_bracket = StrKey(c.closing_bracket)
        self.opening_bracket = StrKey(c.opening_bracket)
        self.punctuation_mark = StrKey(c.punctuation_mark)
        self.text_formatting = StrKey(c.text_formatting)
        self.inquiry_point = StrKey(c.inquiry_point)
        self.aspect = StrKey(c.aspect)
        self.honorific = StrKey(c.honorific)
        self.mood = StrKey(c.mood)
        self.number = StrKey(c.number)
        self.person = StrKey(c.person)
        self.tense = StrKey(c.tense)
        self.grammatical_gender = StrKey(c.grammatical_gender_item)
        self.location_of_sense_usage = StrKey(c.geolect)
        self.language_style = StrKey(c.linguistic_style)
        self.part_of_speech = StrKey(c.part_of_speech)
        self.compounding = StrKey(c.compounding)
        self.contraction = StrKey(c.contraction)
        self.word_form = StrKey(c.word_form)
        self.wikibase_form = StrKey(c.wikibase_form)
        self.omission = StrKey(c.omission)
        self.auxiliary_verb = StrKey(c.auxiliary_verb)
        self.preposition = StrKey(c.preposition)
        self.index_number = StrKey(c.index_number)
        self.adjectival_suffix = StrKey(c.adjectival_suffix)
        self.v2_word_order = StrKey(c.v2_word_order)
        self.thematic_relation = StrKey(c.thematic_relation)
        self.focus = StrKey(c.focus)
        self.linking_morpheme = StrKey(c.linking_morpheme)
        self.writing_system = StrKey(c.writing_system)
        self.shortest_path_tree = StrKey(c.shortest_path_tree)
        self.combined_in_lexeme = StrKey(c.combined_in_lexeme)
        self.underspecification = StrKey(c.underspecification)

        self.now = EntityKey(c.now)
        self.reference_time = EntityKey(c.reference_time)
        self.occurrence_time = EntityKey(c.occurrence_time)
        self.exclusion = EntityKey(c.exclusion)
        self.origin_of_speech = EntityKey(c.origin_of_speech)


C_ = FCKeys()
