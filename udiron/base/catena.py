"""This module holds the Catena class and functions used with it.

* ☞ Information: Please see `CatenaZipper <udiron.base.catenazipper>`_ for more
    information about larger structures built of Catenae. It is hoped that this
    clarifies the role of the **dependents** field and those methods that use it.
* ⌨ Implementation detail: In early revisions of Udiron, the primary
    syntactic unit was the ``Clause``, which also represented a syntax tree node
    and its descendants, but also kept track of its position relative to other
    syntax tree nodes. Because this position tracking introduced lots of
    overhead--any change to a single node affected all other connected nodes--it was
    abandoned in favor of the current ``Catena``.
    Also, because functions manipulating ``Clauses`` were incompatible with
    the notion of immutable types, they were reworked to operate on CatenaZippers.
"""

import contextlib
import logging
import re
from collections import defaultdict
from functools import reduce
from itertools import count
from textwrap import indent
from typing import (TYPE_CHECKING, Collection, DefaultDict, Iterable, List,
                    Mapping, NamedTuple, Optional, Tuple)

import tfsl.interfaces as tfsli
import tfsl.languages
import tfsl.lexeme
import tfsl.lexemesense
from tfsl import L, Language, LexemeLike, LexemeSenseLike, MonolingualText

import udiron.base.constants as c
import udiron.base.dependententry
import udiron.base.interfaces as i
from udiron.base.catenaconfig import CatenaConfig, build_catenaconfig
from udiron.base.dependentlist import DependentList, build_dependentlist
from udiron.base.functionconfig import C_

if TYPE_CHECKING:
    import tfsl.lexemeform

    from udiron.base.catenatracking import CatenaTracking

logger = logging.getLogger(__name__)

catena_id_source = count()


class Catena(NamedTuple):
    """Base object of consideration in a dependency syntax tree.

    It is named for the `construct of the same name <https://en.wikipedia.org/wiki/Catena_(linguistics)>`_
    due to such constructs being used in the analysis of dependency grammars.
    It does, however, contain *all* dependents of the root (i.e. it is complete),
    so it may be suggested to rename this unit to something like "Constituent"
    or "Component", but in addition to the overt generality of both names,
    in the future the structure may serve a number of other purposes,
    including as a placeholder for discontinuities and for the individual slots of
    sentence templates.

    'inflections' may correspond, but very often will not, to the grammatical features of a
    particular form on a lexeme.

    * ⚠️ Warning: 'sense' defaults to the first sense of the lexeme if not explicitly provided at creation time. As a result, creating a Catena with a senseless lexeme will fail, something that *will not* be addressed.

    * ⚠️ Warning: 'id' is maintained throughout any calls to Catena.clone or CatenaZipper.modify,
      given that the CatenaZipper structure requires the production of new Catenae
      that may have different values for the Python built-in id() despite representing the same syntax tree nodes.

    Attributes:
        lexeme: The lexeme which the root of this Catena represents.
        language: The language to be applied when this Catena is rendered. Defaults to the language of the lexeme if not provided.
        sense: The specific meaning of the lexeme which the root of this Catena represents.
        inflections: The inflections to be applied in rendering the root of this Catena.
        config: Specific settings for rendering this Catena may be applied here, potentially altering every aspect of the rendering process.
        dependents: The dependents of this Catena.
        id: A unique identifier for the Catena.
    """
    lexeme: LexemeLike
    language: Language
    sense: LexemeSenseLike
    inflections: i.InflectionSet
    config: CatenaConfig
    dependents: DependentList
    id: str

    def clone(
        self,
        lexeme: Optional[LexemeLike] = None,
        language: Optional[Language] = None,
        sense: Optional[LexemeSenseLike] = None,
        inflections: Optional[i.InflectionSet] = None,
        config: Optional[CatenaConfig] = None,
        dependents: Optional[DependentList] = None,
    ) -> "Catena":
        """Clones a Catena while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the Catena
        is preserved in the output.

        * ☞ Information: Note the absence of the Catena identifier as an input to this function,
            since adjusting that is treated as equivalent to defining a new Catena.

        Args:
            lexeme: The lexeme to replace the current lexeme in this Catena.
            language: The language to replace the current language in this Catena.
            sense: The sense to replace the current sense in this Catena.
            inflections: The inflection set to replace the current inflection set in this Catena.
            config: The configuration to replace the current configuration in this Catena.
            dependents: The dependent list to replace the current dependent list in this Catena.

        Returns:
            Catena like the original but with fields modified as per the provided arguments.
        """
        old_lexeme, old_language, old_sense, old_inflections, old_config, old_dependents, old_id = self
        return Catena(
            old_lexeme if lexeme is None else lexeme,
            old_language if language is None else language,
            old_sense if sense is None else sense,
            old_inflections if inflections is None else inflections,
            old_config if config is None else config,
            old_dependents if dependents is None else dependents,
            old_id,
        )

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two catenae.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this catena is equal to the right-hand side.
        """
        if not isinstance(rhs, Catena):
            return False
        checks = (
            self.lexeme == rhs.lexeme,
            self.language == rhs.language,
            self.sense == rhs.sense,
            self.inflections == rhs.inflections,
            self.config == rhs.config,
            self.dependents == rhs.dependents,
        )
        return reduce(lambda x, y: x and y, checks)

    @classmethod
    def next_id(cls) -> str:
        """Gets the next ID to assign to a newly built Catena.

        Returns:
            ID for use in a Catena, in the event no ID has been specified at Catena creation time.
        """
        return str(next(catena_id_source))

    # Actual implementations to test

    def has_inflections(self, inflections: List[tfsli.Qid]) -> bool:
        """Checks the inflection set of this Catena against the provided inflection set.

        Args:
            inflections: Set of inflections to check for in this Catena.

        Returns:
            Whether all inflections in **inflections** are specified for this Catena.
        """
        return all(inflection in self.inflections for inflection in inflections)

    def add_inflections(self, inflections: Iterable[tfsli.Qid]) -> "Catena":
        """Adds the provided inflections to the provided Catena.

        Args:
            inflections: Set of inflections to add to this Catena.

        Returns:
            Catena with the inflections in **inflections** added to its inflection set.
        """
        return self.clone(inflections=self.inflections.union(inflections))

    def remove_inflections(self, inflections: Iterable[tfsli.Qid]) -> "Catena":
        """Removes the provided inflections from this Catena.

        Args:
            inflections: Set of inflections to remove from this Catena.

        Returns:
            Catena with the inflections in **inflections** removed.
        """
        return self.clone(inflections=self.inflections.difference(inflections))

    def add_to_config(self, config: CatenaConfig) -> "Catena":
        """Merges the provided configuration into that of this Catena.

        Args:
            config: Configuration to be added to this Catena.

        Returns:
            Catena with the information in **config** added to its configuration.
        """
        return self.clone(config=self.config.update(config))

    def expand(self, language: Optional[Language] = None) -> "Catena":
        """Splits a lexeme into components based on the "combines lexeme" values of this catena.

        These values must be appropriately qualified with at least P9764 and P9763
        (see `this page <https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Universal_Dependencies>`_
        for examples of such qualification)
        but may also be qualified with P5548 and P5980 to specify the inflections and sense of
        component Catenae directly.

        Args:
            language: Language to apply to the resultant Catena and its dependents.

        Returns:
            Catena for the root of the dependency syntax tree represented by the "combines lexeme" statements.
        """
        current_sense_id = self.sense.id
        components = self.lexeme[c.combines_lexeme]
        if len(components) == 0:
            return self

        base_catena_id = self.id
        base_tracking = self.config.tracking
        root_index = 0
        catena_objects = {}
        left_dependents = defaultdict(list)
        right_dependents = defaultdict(list)

        for component in components:
            current_lexeme_id = component.get_itemvalue()
            current_lexeme = L(current_lexeme_id)

            index_qualifier = component.qualifiers[c.series_ordinal][0]
            index = int(index_qualifier.get_str())

            object_inflections: i.InflectionSet = frozenset()
            if object_form_qualifier := component.qualifiers[c.object_form]:
                object_form_id = object_form_qualifier[0].get_itemvalue().get_lfid()
                object_form = current_lexeme[object_form_id]
                object_inflections = frozenset(object_form.features)

            try:
                object_sense_id = component.qualifiers[c.object_sense][0].get_itemvalue().get_lsid()
                object_sense = L(object_sense_id)[object_sense_id]
            except (IndexError, KeyError):
                object_sense = None

            syntactic_parent_qualifier = component.qualifiers[c.head_position][0]
            syntactic_parent = int(syntactic_parent_qualifier.get_str())
            if syntactic_parent == 0:
                root_index = index

            syntactic_relationship_value = component.qualifiers[c.head_relationship][0]
            syntactic_relationship = syntactic_relationship_value.get_itemvalue().get_qid()

            catena_objects[index] = (current_lexeme, object_sense, object_inflections, syntactic_parent, syntactic_relationship)
            if index < syntactic_parent:
                left_dependents[syntactic_parent].append(index)
            else:
                right_dependents[syntactic_parent].append(index)

        new_catena, _ = build_dependents(catena_objects, left_dependents, right_dependents, root_index, base_catena_id, current_sense_id, language, base_tracking)
        return new_catena

    # Member attributes

    def get_lemma(self, language: Language) -> MonolingualText:
        """Retrieves a lemma.

        Args:
            language: Language of the lemma to be retrieved.

        Returns:
            Lemma, in **language**, of the lexeme of this Catena.
        """
        return self.lexeme[language]

    def get_lexeme_id(self) -> tfsli.Lid:
        """Mostly a convenience wrapper around tfsl.Lexeme.id.

        Returns:
            ID of the lexeme of this Catena, or "L0" if the lexeme lacks an ID.
        """
        return tfsli.Lid("L0") if self.lexeme.id is None else self.lexeme.id

    def get_category(self) -> tfsli.Qid:
        """* ☞ Information: Merely a convenience wrapper around tfsl.LexemeLike.category.

        Returns:
            Lexical category of the Catena's lexeme.
        """
        return self.lexeme.category

    # Calls to members

    def get_forms(self, inflections: Optional[Collection[tfsli.Qid]] = None, exclusions: Optional[Collection[tfsli.Qid]] = None) -> tfsli.LexemeFormLikeList:
        """* ☞ Information: Merely calls tfsl.LexemeFormLike.get_forms on self.lexeme; see that function for more information.

        Args:
            inflections: Any inflections to take into consideration.
            exclusions: Any inflections to exclude from consideration.

        Returns:
            Forms of the Catena's lexeme with regard to the inflections and exclusions.
        """
        return self.lexeme.get_forms(inflections, exclusions)

    def get_dependent_index_left(self, target_dependent: "udiron.base.dependententry.DependentEntry") -> int:
        """* ☞ Information: Merely calls udiron.base.DependentList.get_dependent_index_left on self.dependents; see that function for more information.

        Args:
            target_dependent: Dependent to search for among left dependents.

        Returns:
            Index of the provided target dependent among left dependents.
        """
        return self.dependents.get_dependent_index_left(target_dependent)

    def get_dependent_index_right(self, target_dependent: "udiron.base.dependententry.DependentEntry") -> int:
        """* ☞ Information: Merely calls udiron.base.DependentList.get_dependent_index_right on self.dependents; see that function for more information.

        Args:
            target_dependent: Dependent to search for among right dependents.

        Returns:
            Index of the provided target dependent among right dependents.
        """
        return self.dependents.get_dependent_index_right(target_dependent)

    def get_rel_index_left(self, target_rel: tfsli.Qid) -> int:
        """* ☞ Information: Merely calls udiron.base.DependentList.get_rel_index_left on self.dependents; see that function for more information.

        Args:
            target_rel: Relationship to search for among left dependents.

        Returns:
            Index of the leftmost dependent with the provided relationship among left dependents.
        """
        return self.dependents.get_rel_index_left(target_rel)

    def get_rel_index_right(self, target_rel: tfsli.Qid) -> int:
        """* ☞ Information: Merely calls udiron.base.DependentList.get_rel_index_right on self.dependents; see that function for more information.

        Args:
            target_rel: Relationship to search for among right dependents.

        Returns:
            Index of the leftmost dependent with the provided relationship among right dependents.
        """
        return self.dependents.get_rel_index_right(target_rel)

    def has_rel(self, rel: tfsli.Qid) -> bool:
        """* ☞ Information: Merely calls udiron.base.DependentList.has_rel on self.dependents; see that function for more information.

        Args:
            rel: The relation to check for.

        Returns:
            Whether a dependent with the provided relation is present among the Catena's dependents.
        """
        return self.dependents.has_rel(rel)

    # Printing functions

    def abbr(self) -> str:
        """Prints an abbreviated summary of the top of this Catena.

        Returns:
            Abbreviated summary of the top of this Catena.
        """
        lexeme_string = self.lexeme.repr_lemmata_first()
        sense_string = repr(self.sense.id or "")
        inflection_string = "{" + ", ".join(self.inflections) + "}"
        return f"{lexeme_string} <{sense_string}> {inflection_string}"

    def left_dependent_string(self, rel: Optional[str] = None) -> str:
        """Prints a syntax tree of the (logically) left-hand side dependents of this Catena.

        Args:
            rel: Relationship between dependent and head, used for alignment purposes.

        Returns:
            Syntax tree of the (logically) left-hand side dependents of this Catena.
        """
        left_dependents, _ = self.dependents
        left_dependent_strings = [repr(dependent) for dependent in left_dependents]
        rel_spaces = c.TAB_SPACES if rel is None else " " * (6 + len(rel))
        if left_dependent_strings:
            left_dependent_string = indent("\n".join(left_dependent_strings), rel_spaces + "│", lambda line: not line.startswith("<"))
            left_dependent_string = indent(left_dependent_string, rel_spaces + "╭", lambda line: line.startswith("<"))
            left_dependent_string = re.sub(r"^│" + rel_spaces + r"([^\n]+)\n╭" + c.TAB_HYPHENS, " " + rel_spaces + r"\1\n╭" + c.TAB_HYPHENS, left_dependent_string, flags=re.M)
            left_dependent_string = re.sub(r"^│" + rel_spaces + r"([^\n]+)\n " + rel_spaces, " " + rel_spaces + r"\1\n " + rel_spaces, left_dependent_string, flags=re.M)
            with contextlib.suppress(ValueError):
                if left_dependent_string[0] != "╭":
                    left_dependent_string = re.sub(
                        r"^│" + rel_spaces, " " + rel_spaces,
                        left_dependent_string, flags=re.M,
                        count=left_dependent_string.count(
                            "\n", 0,
                            left_dependent_string.index("\n╭"),
                        ) + 1,
                    )
        else:
            left_dependent_string = ""
        return left_dependent_string

    def right_dependent_string(self, rel: Optional[str] = None) -> str:
        """Prints a syntax tree of the (logically) right-hand side dependents of this Catena.

        Args:
            rel: Relationship between dependent and head, used for alignment purposes.

        Returns:
            Syntax tree of the (logically) right-hand side dependents of this Catena.
        """
        _, right_dependents = self.dependents
        right_dependent_strings = [repr(dependent) for dependent in right_dependents]
        rel_spaces = c.TAB_SPACES if rel is None else " " * (6 + len(rel))
        if right_dependent_strings:
            right_dependent_string = indent("\n".join(right_dependent_strings), rel_spaces + "│", lambda line: not line.startswith("<"))
            right_dependent_string = indent(right_dependent_string, rel_spaces + "╰", lambda line: line.startswith("<"))
            right_dependent_string = re.sub(r"^╰" + c.TAB_HYPHENS + r"([^\n]+)\n│" + rel_spaces, r"╰" + c.TAB_HYPHENS + r"\1\n " + rel_spaces, right_dependent_string, flags=re.M)
            right_dependent_string = re.sub(r"^ " + rel_spaces + r"([^\n]+)\n│" + rel_spaces, r" " + rel_spaces + r"\1\n " + rel_spaces, right_dependent_string, flags=re.M)
            try:
                rds_index = right_dependent_string.rindex("\n" + rel_spaces + "╰")
                right_dependent_string = right_dependent_string[:rds_index] + re.sub(
                    r"\n" + rel_spaces + r"│", "\n" + rel_spaces + " ", right_dependent_string[rds_index:],
                )
            except ValueError:
                pass
        else:
            right_dependent_string = ""
        return right_dependent_string

    def __repr__(self) -> str:
        """Produces string representation of the Catena.

        Returns:
            String representation of the Catena.
        """
        base_string = self.abbr()
        left_dependent_string = self.left_dependent_string()
        right_dependent_string = self.right_dependent_string()
        return "\n".join([left_dependent_string, base_string, right_dependent_string]).strip("\n")

    def __jsonout__(self) -> i.CatenaDict:
        """Produces a JSON serialization of this Catena.

        Returns:
            JSON serialization of this Catena.
        """
        lexeme, language, sense, inflections, config, dependents, _ = self

        lexeme_rep = lexeme.id if lexeme.id is not None else lexeme.__jsonout__()
        language_rep = language.__jsonout__()
        sense_rep = sense.id if sense.id is not None else sense.__jsonout__()
        config_rep = config.__jsonout__()
        dependents_rep = dependents.__jsonout__()
        return {
            "lexeme": lexeme_rep,
            "language": language_rep,
            "sense": sense_rep,
            "inflections": list(inflections),
            "config": config_rep,
            "dependents": dependents_rep,
        }


def build_catena(catena_in: i.CatenaDict) -> Catena:
    """Builds a Catena from a JSON serialization.

    Args:
        catena_in: JSON serialization of a Catena.

    Returns:
        New Catena.

    Todo:
        how to control whether the "id" attribute should be preserved?
    """
    language = tfsl.languages.build_language(catena_in["language"])
    lexeme: LexemeLike
    sense: LexemeSenseLike
    if isinstance(catena_in["lexeme"], str) and isinstance(catena_in["sense"], str) and tfsli.is_lid(catena_in["lexeme"]) and tfsli.is_lsid(catena_in["sense"]) and tfsli.get_lid_string(catena_in["sense"]) == catena_in["lexeme"]:
        lexeme = L(catena_in["lexeme"])
        sense = lexeme[catena_in["sense"]]
    else:
        if isinstance(catena_in["lexeme"], str) and tfsli.is_lid(catena_in["lexeme"]):
            lexeme = L(catena_in["lexeme"])
        elif not isinstance(catena_in["lexeme"], str) and tfsli.is_entitypublishedsettings(catena_in["lexeme"]) and tfsli.is_lexemedict(catena_in["lexeme"]):
            lexeme = tfsl.lexeme.build_lexeme(catena_in["lexeme"])
        if isinstance(catena_in["sense"], str) and tfsli.is_lsid(catena_in["sense"]):
            sense = L(catena_in["sense"])[catena_in["sense"]]
        elif not isinstance(catena_in["sense"], str) and tfsli.is_entitypublishedsettings(catena_in["sense"]) and tfsli.is_lexemesensedict(catena_in["sense"]):
            sense = tfsl.lexemesense.build_sense(catena_in["sense"])
    inflections = frozenset(catena_in["inflections"])
    config = build_catenaconfig(catena_in["config"])
    dependents = build_dependentlist(catena_in["dependents"])
    return Catena(lexeme, language, sense, inflections, config, dependents, Catena.next_id())


def build_dependents(
    catena_objects: Mapping[int, Tuple[LexemeLike, Optional[LexemeSenseLike], i.InflectionSet, int, tfsli.Qid]],
    left_dependents: DefaultDict[int, List[int]],
    right_dependents: DefaultDict[int, List[int]],
    index: int, base_catena_id: str, base_catena_sense_id: tfsli.LSid,
    language: Optional[Language] = None, tracking: Optional["CatenaTracking"] = None,
) -> Tuple[Catena, tfsli.Qid]:
    """Utility function for recursively building a component Catena of a larger Catena given information about all "combines lexeme" statements of the larger Catena.

    Args:
        catena_objects: Mapping from series ordinals to Catena information.
        left_dependents: Mapping from series ordinals to all left dependents.
        right_dependents: Mapping from series ordinals to all right dependents.
        index: Current series ordinal.
        base_catena_id: Identifier of the Catena being expanded.
        base_catena_sense_id: Sense ID of the Catena being expanded.
        language: Language to be set in the Catena.
        tracking: Tracking to be set in the expanded Catena and its dependents.

    Returns:
        Newly built expanded Catena.
    """
    current_left_dependents = left_dependents[index]
    current_right_dependents = right_dependents[index]
    new_left_dependents = []
    new_right_dependents = []

    for dependent_index in current_left_dependents:
        new_catena, new_rel = build_dependents(catena_objects, left_dependents, right_dependents, dependent_index, base_catena_id, base_catena_sense_id, language, tracking)
        new_left_dependents.append(udiron.base.dependententry.DependentEntry(new_catena, new_rel))

    for dependent_index in current_right_dependents:
        new_catena, new_rel = build_dependents(catena_objects, left_dependents, right_dependents, dependent_index, base_catena_id, base_catena_sense_id, language, tracking)
        new_right_dependents.append(udiron.base.dependententry.DependentEntry(new_catena, new_rel))

    lexeme, sense, inflections, _, rel = catena_objects[index]
    if language is not None:
        new_language = language
    else:
        new_language = lexeme.language
    new_config = CatenaConfig.new(tracking=tracking)
    new_config = new_config.set(C_.combined_in_lexeme, base_catena_sense_id)
    if sense is not None:
        new_sense = sense
    else:
        try:
            new_sense = lexeme.get_senses()[0]
        except IndexError:
            logger.warning("No senses found on lexeme %s", lexeme.id)
            raise
        new_config = new_config.set(C_.underspecification, c.wikibase_sense)
    return Catena(
        lexeme,
        new_language,
        new_sense,
        inflections,
        new_config,
        DependentList(new_left_dependents, new_right_dependents),
        base_catena_id + "_" + str(index),
    ), rel
