"""Holds functions pertaining to the Catena rendering process."""

from operator import itemgetter
from typing import TYPE_CHECKING, Tuple

from udiron.langs import (get_build_form, get_select_forms, get_surface_join,
                          get_surface_transform)

if TYPE_CHECKING:
    import udiron.base.interfaces as i
    from udiron.base.catenazipper import CatenaZipper
    from udiron.base.dependententry import DependentEntry


def render(catena: "CatenaZipper") -> "i.OutputEntryList":
    """Renders a Catena rooted at the cursor of this CatenaZipper.

    The Catena rendering process consists of a pre-step, four main steps, and a post-step:

    * Pre-step: if a Catena consists of multiple joined sentences (where each sentence other
        than the first is joined to the first with the dependency relationship "Q41796"), then
        the other sentences are detached and placed in a list to be dealt with in the post-step.
        The following four steps are then performed for the first sentence only.
    * Step 1: select_forms
    * Step 2: collect_forms
    * Step 3: surface_join
    * Step 4: surface_transform
    * Post-step: if any sentences were detached in the pre-step, then this entire rendering process
        is performed for each of those sentences in order, with their outputs appended to the outputs
        from rendering the first sentence.

    (The four steps are described in the documentation for the functions that perform them--search for
    "The (first|second|third|fourth) step of the Catena rendering process." on this page.
    The pre-step and post-step are performed inside udiron.base.catenazipper.rendering.)

    The functions for these four steps must be defined for each language;
    they are expected to be written by contributors to the library.
    These functions may call out to other sub-functions that may need to be written;
    there are a number of sub-functions that are used with multiple languages as well.

    * ⌨ Implementation detail: In earlier revisions of Udiron, this function would simply
        return a string. For further introspection of the origins of a particular output, however,
        it is necessary for parts of the output of this function to maintain some individual
        pieces of information that led to those parts. This is achieved with the `OutputEntry <udiron.base.outputentry>`_
        class.
    * ⌨ Implementation detail: In this Python implementation, there is a function "register" in `<udiron.langs>`_
        that can be used as a decorator to quickly register that function as one that performs one of the four steps
        for a given language, such that it can be used at Catena rendering time:

        .. code-block:: python

            @register("SelectForms", tfsl.Language("de", "Q188"))
            def select_forms_de(catena_in):
            @register("CollectForms", tfsl.Language("de", "Q188"))
            def collect_forms_de(catena_in, index):
            @register("SurfaceJoin", tfsl.Language("de", "Q188"))
            def surface_join_de(formlist, config):
            @register("SurfaceTransform", tfsl.Language("de", "Q188"))
            def surface_transform_de(joined, config):

    Args:
        catena: CatenaZipper being rendered.

    Returns:
        Sequence of OutputEntries whose surface representations, when concatenated, yield a final output string.
    """
    adjusted_catena = select_forms(catena)

    collected_forms: "i.OutputEntryListIndexed"
    collected_forms, _ = collect_forms(adjusted_catena)
    reordered_tokens = sorted(collected_forms, key=itemgetter(1))

    output_string = surface_join(adjusted_catena, reordered_tokens)

    output_string = surface_transform(adjusted_catena, output_string)

    return output_string


def select_forms_dependent(catena: "CatenaZipper", dependent: "DependentEntry") -> "CatenaZipper":
    """Performs form selection on a Catena's dependent.

    Args:
        catena: Catena with at least one dependent.
        dependent: Dependent to be pushed onto the CatenaZipper and on which form selection will be run.

    Returns:
        CatenaZipper with form selection run on the cursor and its dependents.
    """
    new_zipper = catena.push(dependent)
    new_zipper = select_forms(new_zipper)
    _, new_zipper = new_zipper.pop()
    return new_zipper


def select_forms_head(catena: "CatenaZipper") -> "CatenaZipper":
    """Performs form selection on a single Catena.

    Args:
        catena: Catena to be form-selected.

    Returns:
        CatenaZipper with form selection run on its cursor.
    """
    old_catena, _ = catena.top()
    new_inflections, new_config = get_select_forms(old_catena.language)(catena)
    new_zipper = catena.modify(inflections=new_inflections, config=new_config)
    return new_zipper


def select_forms(catena: "CatenaZipper") -> "CatenaZipper":
    """The first step of the Catena rendering process.

    This step goes through each of the Catenae and processes each with the SelectForms defined
    for the language of that Catena, or an appropriate fallback SelectForms if such a function is specified.
    (See the documentation of SelectForms at `<udiron.base.interfaces>`_ to learn more about what
    one does.)
    The end result is a changed Catena, generally not modified in terms of syntactic structure.

    The order in which the Catenae are processed in this step is as follows:
    * From left to right among the left dependents of the Catena, this step is performed for each dependent.
    * The SelectForms defined for a particular language is run on the Catena.
    * From left to right among the right dependents of the Catena, this step is performed for each dependent.

    Thus, as an example, if **self** were the Catena listed under "Adler und Tauben" in `catenazipper_diagrams.md`,
    the order in which the nodes are visited would be (using their lemmata here)
    "langsam", "Adler", "brüten", "Taube", "in", "das", and "Park".
    (See how this basically corresponds to the order of the words in
    "Langsam Adler brüten Tauben im Park"?)

    Args:
        catena: CatenaZipper being rendered.

    Returns:
        CatenaZipper like the original but with changes made by SelectForms.
    """
    new_zipper = catena
    left_dependents, right_dependents = new_zipper.get_dependents()

    for dependent in left_dependents:
        new_zipper = select_forms_dependent(new_zipper, dependent)

    new_zipper = select_forms_head(new_zipper)

    for dependent in right_dependents:
        new_zipper = select_forms_dependent(new_zipper, dependent)

    return new_zipper


def collect_forms(catena: "CatenaZipper", token_position: int = 0, parent_token_position: int = 0) -> Tuple["i.OutputEntryListIndexed", int]:
    """The second step of the Catena rendering process.

    This step goes through each of the Catenae and retrieves a list (usually with one form, though
    multiple forms might be retrieved as well)
    with the CollectForms defined for the language of that Catena,
    or an appropriate fallback CollectForms if such a function is specified.
    (See the documentation of CollectForms at `<udiron.base.interfaces>`_ to learn more about what
    one does.)
    The end result is a list of pairs of the form (`OutputEntry <udiron.base.outputentry>`_, number),
    where the number indicates the relative ordering of an OutputEntry to all other entries in the list.
    Usually each OutputEntry at this stage has one component, with any such entries with multiple
    components originating in the fourth step.

    The traversal used in this step is similar to that used by CatenaZipper.select_forms.
    As an example, if **self** were the Catena listed under "Adler und Tauben" in `catenazipper_diagrams.md`
    (appropriately modified by the previous step),
    then the resultant pairs of (OutputEntry, number) that would result would be the following in this order
    (where all OutputEntries contain one component each and are represented by the output form):

    * ("langsam", 0)
    * ("Adler", 1)
    * ("brüten", 2)
    * ("Tauben", 3)
    * ("in", 4)
    * ("dem", 5)
    * ("Park", 6)

    Args:
        catena: CatenaZipper being rendered.
        token_position: Starting point of OutputEntry indices.
        parent_token_position: Position of the parent token at the time of this form collection.

    Returns:
        List of (OutputEntry, number) tuples representing the order of individual output elements.
    """
    new_zipper = catena
    token_list: "i.OutputEntryListIndexed" = []
    current_token_position = token_position
    left_dependents, right_dependents = new_zipper.get_dependents()
    for dependent in left_dependents:
        new_zipper = new_zipper.push(dependent)
        produced_forms, current_token_position = collect_forms(new_zipper, current_token_position, parent_token_position - 1)
        token_list.extend(produced_forms)
        _, new_zipper = new_zipper.pop()

    old_catena, old_rel = new_zipper.top()
    build_form_lang = get_build_form(old_catena.language)
    new_tokens, current_token_position = build_form_lang(old_catena, old_rel, current_token_position, parent_token_position)
    catena_token_position = current_token_position
    token_list += new_tokens

    for dependent in right_dependents:
        new_zipper = new_zipper.push(dependent)
        produced_forms, current_token_position = collect_forms(new_zipper, current_token_position, parent_token_position - 1)
        token_list.extend(produced_forms)
        _, new_zipper = new_zipper.pop()

    token_list = [(token.set_heads(parent_token_position - 1, catena_token_position), index) for (token, index) in token_list]
    return token_list, current_token_position


def surface_join(catena: "CatenaZipper", tokens_in: "i.OutputEntryListIndexed") -> "i.OutputEntryList":
    """The third step of the Catena rendering process.

    This step runs the SurfaceJoin defined for a particular language,
    or an appropriate fallback SurfaceJoin if such a function is specified,
    on the list of forms returned by CollectForms.
    (See the documentation of SurfaceJoin at `<udiron.base.interfaces>`_ to learn more about what
    one does.)
    The end result is a list of `OutputEntry <udiron.base.outputentry>`_ objects.

    As an example, if **tokens_in** were the following list from the previous step
    (where all OutputEntries contain one component each and are represented by the output form):

    * ("langsam", 0)
    * ("Adler", 1)
    * ("brüten", 2)
    * ("Tauben", 3)
    * ("in", 4)
    * ("dem", 5)
    * ("Park", 6)

    then the simple result from this step would be the following list
    (where all OutputEntries contain one component each and are represented by the output form):

    * "Langsam "
    * "Adler "
    * "brüten "
    * "Tauben "
    * "in "
    * "dem "
    * "Park "

    Args:
        catena: CatenaZipper being rendered.
        tokens_in: The pairs of (OutputEntry, number) obtained from running collect_forms with this CatenaZipper.

    Returns:
        The list of OutputEntries modified to reflect an initial joining step.
    """
    old_catena, _ = catena.top()
    surface_join_lang = get_surface_join(old_catena.language)
    return surface_join_lang(tokens_in, old_catena.config)


def surface_transform(catena: "CatenaZipper", entries_in: "i.OutputEntryList") -> "i.OutputEntryList":
    """The fourth step of the Catena rendering process.

    This step runs the SurfaceTransform defined for a particular language,
    or an appropriate fallback SurfaceTransform if such a function is specified,
    on the list of forms returned by SurfaceJoin.
    (See the documentation of SurfaceTransform at `<udiron.base.interfaces>`_ to learn more about what
    one does.)
    The end result is a list of `OutputEntry <udiron.base.outputentry>`_ objects.

    As an example, if **entries_in** were the following list
    (where all OutputEntries contain one component each and are represented by the output form):

    * "Langsam "
    * "Adler "
    * "brüten "
    * "Tauben "
    * "in "
    * "dem "
    * "Park "

    then the result from this step would be the following list
    (where any OutputEntry with multiple components lists those components in angle brackets afterwards):

    * "Langsam "
    * "Adler "
    * "brüten "
    * "Tauben "
    * "im " <"in ", "dem ">
    * "Park"

    The result from concatenating the surface forms directly is
    in some sense the end result of rendering the Catena introduced
    in the documentation for CatenaZipper.select_forms:
    "Langsam Adler brüten Tauben im Park".

    Args:
        catena: CatenaZipper being rendered.
        entries_in: The list of OutputEntries returned from surface_join.

    Returns:
        The list of OutputEntries modified to reflect any surface changes, phonological or otherwise.
    """
    old_catena, _ = catena.top()
    surface_transform_lang = get_surface_transform(old_catena.language)
    return surface_transform_lang(entries_in, old_catena.config)
