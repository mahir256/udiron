"""Holds tests for functions in udiron.base.catenazipper."""

import tfsl.interfaces as tfsli
from tfsl import L, langs

import udiron.base.constants as c
from tests import WikifunctionsFunctionTest
from udiron.base.catena import Catena
from udiron.base.catenaconfig import CatenaConfig
from udiron.base.catenazipper import CatenaZipper
from udiron.base.dependententry import DependentEntry
from udiron.base.dependentlist import DependentList

hridoy = L(301993)
hridoy_sense = hridoy[tfsli.LSid("L301993-S1")]
hridoy_catena = Catena(hridoy, langs.bn_, hridoy_sense, frozenset(), CatenaConfig.new(), DependentList([], []), "1")
hridoy_dependent = DependentEntry(hridoy_catena, c.syntactic_root)
can = L(1207265)
can_sense = can[tfsli.LSid("L1207265-S1")]
can_catena = Catena(can, langs.tr_, can_sense, frozenset(), CatenaConfig.new(), DependentList([], []), "2")
can_dependent = DependentEntry(can_catena, c.subject)


class Empty(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.empty' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.empty
    cases = [
        ((CatenaZipper([]),), {}, True),
        ((CatenaZipper([hridoy_dependent]),), {}, False),
    ]
    args = (function, cases)


class Top(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.top' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.top
    cases = [
        ((CatenaZipper([hridoy_dependent, can_dependent]),), {}, hridoy_dependent),
        ((CatenaZipper([]),), {}, IndexError),
    ]
    args = (function, cases)


class Root(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.root' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.root
    cases = [
        ((CatenaZipper([hridoy_dependent, can_dependent]),), {}, can_dependent),
        ((CatenaZipper([]),), {}, IndexError),
    ]
    args = (function, cases)


class TopCatena(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.top_catena' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.top_catena
    cases = [
        ((CatenaZipper([hridoy_dependent, can_dependent]),), {}, hridoy_catena),
        ((CatenaZipper([]),), {}, IndexError),
    ]
    args = (function, cases)


class RootCatena(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.root_catena' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.root_catena
    cases = [
        ((CatenaZipper([hridoy_dependent, can_dependent]),), {}, can_catena),
        ((CatenaZipper([]),), {}, IndexError),
    ]
    args = (function, cases)


class Pop(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.pop' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.pop
    cases = [
        ((CatenaZipper([hridoy_dependent, can_dependent]),), {}, (hridoy_dependent, CatenaZipper([can_dependent]))),
        ((CatenaZipper([hridoy_dependent]),), {}, (hridoy_dependent, CatenaZipper([]))),
        ((CatenaZipper([]),), {}, IndexError),
    ]
    args = (function, cases)


class Push(WikifunctionsFunctionTest):
    """Tests for the 'CatenaZipper.push' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = CatenaZipper.push
    cases = [
        ((CatenaZipper([]), hridoy_dependent), {}, CatenaZipper([hridoy_dependent])),
        ((CatenaZipper([hridoy_dependent]), can_dependent), {}, CatenaZipper([can_dependent, hridoy_dependent])),
    ]
    args = (function, cases)


del WikifunctionsFunctionTest
