"""Holds tests for functions in udiron.base.dependentlist."""

from typing import Callable

import tfsl.interfaces as tfsli
from tfsl import L, langs

import udiron.base.constants as c
import udiron.base.interfaces as i
from tests import WikifunctionsFunctionTest
from udiron.base.catena import Catena
from udiron.base.catenaconfig import CatenaConfig
from udiron.base.dependententry import DependentEntry
from udiron.base.dependentlist import DependentList

hridoy = L(301993)
ma_lexeme = L(1472)
pani_lexeme = L(8248)
bodh_lexeme = L(525391)
catena_1 = Catena(hridoy, langs.bn_, hridoy[tfsli.LSid("L301993-S1")], frozenset(), CatenaConfig.new(), DependentList([], []), "1")
catena_2 = Catena(ma_lexeme, langs.bn_, ma_lexeme[tfsli.LSid("L1472-S1")], frozenset(), CatenaConfig.new(), DependentList([], []), "2")
catena_3 = Catena(pani_lexeme, langs.bn_, pani_lexeme[tfsli.LSid("L8248-S1")], frozenset(), CatenaConfig.new(), DependentList([], []), "3")
catena_4 = Catena(bodh_lexeme, langs.bn_, bodh_lexeme[tfsli.LSid("L525391-S1")], frozenset(), CatenaConfig.new(), DependentList([], []), "4")
de1 = DependentEntry(catena_1, c.subject)
de2 = DependentEntry(catena_2, c.adj_attribute)
de3 = DependentEntry(catena_3, c.direct_object)
de4 = DependentEntry(catena_4, c.indirect_object)
left_branch = [de1, de2]
left_branch_2 = [de3]
left_branch_3 = left_branch_2 + left_branch
left_branch_4 = left_branch + left_branch_2
right_branch = [de3, de4]
right_branch_2 = [de2]
right_branch_3 = right_branch + right_branch_2
right_branch_4 = right_branch_2 + right_branch
dl0 = DependentList([], [])
dl1 = DependentList(left_branch, [])
dl2 = DependentList([], right_branch)
dl3 = DependentList(left_branch, right_branch)
dl4 = DependentList(left_branch_2, [])
dl5 = DependentList(left_branch_3, right_branch)
dl6 = DependentList([], right_branch_2)
dl7 = DependentList(left_branch, right_branch_3)
dl8 = DependentList(left_branch_4, right_branch)
dl9 = DependentList(left_branch, right_branch_4)
lb1 = [de1, de2, de3]
rb1 = [de2, de3, de4]
mb1 = [de1, de2, de3, de4]
mb2 = [de2, de1, de3, de4]
mb3 = [de2, de3, de1, de4]
mb4 = [de2, de3, de4, de1]
mb5 = [de4, de1, de2, de3]
mb6 = [de1, de4, de2, de3]
mb7 = [de1, de2, de4, de3]


def check_dependentlist_equality(base: DependentList) -> Callable[[DependentList], bool]:
    """Function object taking a DependentList and comparing it with the base DependentList.

    Args:
        base: Left-hand side of the object's equality.

    Returns:
        Function object that checks the equality when called.
    """
    class OutputFunction:
        """Internal logic for the function object in question."""

        def __init__(self, base: DependentList) -> None:
            """Produces an equality check.

            Args:
                base: Left-hand side of the equality.
            """
            self.base = base

        def __call__(self, target: DependentList) -> bool:
            """Runs the equality check.

            Args:
                target: Output against which to check.

            Returns:
                Whether the base and the target compare equal.
            """
            return self.base == target

        def _repr(self) -> str:
            """Holds the logic for __repr__.

            Returns:
                String representation of the equality being checked.
            """
            return f"' == {self.base}'"

        def __repr__(self) -> str:
            """Produces string representation of the equality being checked.

            Returns:
                String representation of the equality being checked.
            """
            return self._repr()
    return OutputFunction(base)


def check_dependentlistdetach_equality(base: i.DependentListDetachOutput) -> Callable[[i.DependentListDetachOutput], bool]:
    """Function object taking a DependentList and comparing it with the base DependentList.

    Args:
        base: Left-hand side of the object's equality.

    Returns:
        Function object that checks the equality when called.
    """
    class OutputFunction:
        """Internal logic for the function object in question."""

        def __init__(self, base: i.DependentListDetachOutput) -> None:
            """Produces an equality check.

            Args:
                base: Left-hand side of the equality.
            """
            self.base = base

        def __call__(self, target: i.DependentListDetachOutput) -> bool:
            """Runs the equality check.

            Args:
                target: Output against which to check.

            Returns:
                Whether the base and the target compare equal.
            """
            return self.base == target

        def _repr(self) -> str:
            """Holds the logic for __repr__.

            Returns:
                String representation of the equality being checked.
            """
            return f"' == {self.base}'"

        def __repr__(self) -> str:
            """Produces string representation of the equality being checked.

            Returns:
                String representation of the equality being checked.
            """
            return self._repr()
    return OutputFunction(base)


class GetDependentIndexLeft(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.get_dependent_index_left' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.get_dependent_index_left
    cases = [
        ((dl0, DependentEntry(catena_1, c.subject)), {}, ValueError),
        ((dl1, DependentEntry(catena_1, c.subject)), {}, 0),
        ((dl1, DependentEntry(catena_2, c.subject)), {}, ValueError),
        ((dl1, DependentEntry(catena_1, c.direct_object)), {}, ValueError),
        ((dl2, DependentEntry(catena_1, c.subject)), {}, ValueError),
        ((dl3, DependentEntry(catena_2, c.adj_attribute)), {}, 1),
    ]
    args = (function, cases)


class GetDependentIndexRight(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.get_dependent_index_right' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.get_dependent_index_right
    cases = [
        ((dl0, DependentEntry(catena_4, c.indirect_object)), {}, ValueError),
        ((dl1, DependentEntry(catena_4, c.indirect_object)), {}, ValueError),
        ((dl2, DependentEntry(catena_4, c.indirect_object)), {}, 1),
        ((dl2, DependentEntry(catena_3, c.indirect_object)), {}, ValueError),
        ((dl2, DependentEntry(catena_4, c.subject)), {}, ValueError),
        ((dl3, DependentEntry(catena_4, c.indirect_object)), {}, 1),
    ]
    args = (function, cases)


class GetRelIndexLeft(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.get_rel_index_left' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.get_rel_index_left
    cases = [
        ((dl0, c.subject), {}, StopIteration),
        ((dl1, c.subject), {}, 0),
        ((dl1, c.direct_object), {}, StopIteration),
        ((dl2, c.subject), {}, StopIteration),
        ((dl3, c.adj_attribute), {}, 1),
    ]
    args = (function, cases)


class GetRelIndexRight(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.get_rel_index_right' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.get_rel_index_right
    cases = [
        ((dl0, c.indirect_object), {}, StopIteration),
        ((dl1, c.indirect_object), {}, StopIteration),
        ((dl2, c.direct_object), {}, 0),
        ((dl2, c.subject), {}, StopIteration),
        ((dl3, c.indirect_object), {}, 1),
    ]
    args = (function, cases)


class HasRel(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.has_rel' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.has_rel
    cases = [
        ((dl0, c.indirect_object), {}, False),
        ((dl1, c.indirect_object), {}, False),
        ((dl2, c.indirect_object), {}, True),
        ((dl2, c.subject), {}, False),
        ((dl3, c.indirect_object), {}, True),
    ]
    args = (function, cases)


class AttachLeftmost(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_leftmost' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_leftmost
    cases = [
        ((dl0, de3), {}, check_dependentlist_equality(dl4)),
        ((dl3, de3), {}, check_dependentlist_equality(dl5)),
    ]
    args = (function, cases)


class AttachRightmost(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_rightmost' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_rightmost
    cases = [
        ((dl0, de2), {}, check_dependentlist_equality(dl6)),
        ((dl3, de2), {}, check_dependentlist_equality(dl7)),
    ]
    args = (function, cases)


class AttachLeftOfRoot(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_left_of_root' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_left_of_root
    cases = [
        ((dl0, de3), {}, check_dependentlist_equality(dl4)),
        ((dl3, de3), {}, check_dependentlist_equality(dl8)),
    ]
    args = (function, cases)


class AttachRightOfRoot(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_right_of_root' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_right_of_root
    cases = [
        ((dl0, de2), {}, check_dependentlist_equality(dl6)),
        ((dl3, de2), {}, check_dependentlist_equality(dl9)),
    ]
    args = (function, cases)


class AttachAfterIndexLeft(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_after_index_left' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_after_index_left
    cases = [
        ((DependentList(lb1, rb1), de4, 0), {}, check_dependentlist_equality(DependentList(mb6, rb1))),
        ((DependentList(lb1, rb1), de4, 2), {}, check_dependentlist_equality(DependentList(mb1, rb1))),
        ((DependentList(lb1, rb1), de4, 4), {}, ValueError),
        ((DependentList(lb1, rb1), de4, -1), {}, ValueError),
    ]
    args = (function, cases)


class AttachAfterIndexRight(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_after_index_right' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_after_index_right
    cases = [
        ((DependentList(lb1, rb1), de1, 0), {}, check_dependentlist_equality(DependentList(lb1, mb2))),
        ((DependentList(lb1, rb1), de1, 2), {}, check_dependentlist_equality(DependentList(lb1, mb4))),
        ((DependentList(lb1, rb1), de1, 4), {}, ValueError),
        ((DependentList(lb1, rb1), de1, -1), {}, ValueError),
    ]
    args = (function, cases)


class AttachBeforeIndexLeft(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.attach_before_index_left' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_before_index_left
    cases = [
        ((DependentList(lb1, rb1), de4, 0), {}, check_dependentlist_equality(DependentList(mb5, rb1))),
        ((DependentList(lb1, rb1), de4, 2), {}, check_dependentlist_equality(DependentList(mb7, rb1))),
        ((DependentList(lb1, rb1), de4, 4), {}, ValueError),
        ((DependentList(lb1, rb1), de4, -1), {}, ValueError),
    ]
    args = (function, cases)


class AttachBeforeIndexRight(WikifunctionsFunctionTest):
    """Tests for the 'attach_before_index_right' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.attach_before_index_right
    cases = [
        ((DependentList(lb1, rb1), de1, 0), {}, check_dependentlist_equality(DependentList(lb1, mb1))),
        ((DependentList(lb1, rb1), de1, 2), {}, check_dependentlist_equality(DependentList(lb1, mb3))),
        ((DependentList(lb1, rb1), de1, 4), {}, ValueError),
        ((DependentList(lb1, rb1), de1, -1), {}, ValueError),
    ]
    args = (function, cases)


class DetachAtIndexLeft(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.detach_at_index_left' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.detach_at_index_left
    cases = [
        ((DependentList(mb5, rb1), 0), {}, check_dependentlistdetach_equality((de4, DependentList(lb1, rb1)))),
        ((DependentList(mb7, rb1), 2), {}, check_dependentlistdetach_equality((de4, DependentList(lb1, rb1)))),
        ((DependentList(mb5, rb1), 4), {}, IndexError),
        ((DependentList(mb5, rb1), -1), {}, IndexError),
    ]
    args = (function, cases)


class DetachAtIndexRight(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.detach_at_index_right' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.detach_at_index_right
    cases = [
        ((DependentList(lb1, mb1), 0), {}, check_dependentlistdetach_equality((de1, DependentList(lb1, rb1)))),
        ((DependentList(lb1, mb3), 2), {}, check_dependentlistdetach_equality((de1, DependentList(lb1, rb1)))),
        ((DependentList(lb1, rb1), 4), {}, IndexError),
        ((DependentList(lb1, rb1), -1), {}, IndexError),
    ]
    args = (function, cases)


class DetachLeftmost(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.detach_leftmost' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.detach_leftmost
    cases = [
        ((DependentList(mb5, rb1),), {}, check_dependentlistdetach_equality((de4, DependentList(lb1, rb1)))),
        ((dl4,), {}, check_dependentlistdetach_equality((de3, DependentList([], [])))),
        ((DependentList([], mb5),), {}, ValueError),
    ]
    args = (function, cases)


class DetachRightmost(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.detach_rightmost' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.detach_rightmost
    cases = [
        ((DependentList(lb1, mb4),), {}, check_dependentlistdetach_equality((de1, DependentList(lb1, rb1)))),
        ((dl6,), {}, check_dependentlistdetach_equality((de2, DependentList([], [])))),
        ((DependentList(lb1, []),), {}, ValueError),
    ]
    args = (function, cases)


class DetachLeftOfRoot(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.detach_left_of_root' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.detach_left_of_root
    cases = [
        ((DependentList(mb1, rb1),), {}, check_dependentlistdetach_equality((de4, DependentList(lb1, rb1)))),
        ((dl4,), {}, check_dependentlistdetach_equality((de3, DependentList([], [])))),
        ((DependentList([], mb5),), {}, ValueError),
    ]
    args = (function, cases)


class DetachRightOfRoot(WikifunctionsFunctionTest):
    """Tests for the 'DependentList.detach_right_of_root' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = DependentList.detach_right_of_root
    cases = [
        ((DependentList(lb1, mb1),), {}, check_dependentlistdetach_equality((de1, DependentList(lb1, rb1)))),
        ((dl6,), {}, check_dependentlistdetach_equality((de2, DependentList([], [])))),
        ((DependentList(lb1, []),), {}, ValueError),
    ]
    args = (function, cases)


del WikifunctionsFunctionTest
