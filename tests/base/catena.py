"""Holds tests for functions in udiron.base.catena."""

from typing import Callable

import tfsl.interfaces as tfsli
from tfsl import L, langs

import udiron.base.constants as c
from tests import WikifunctionsFunctionTest
from udiron.base.catena import Catena
from udiron.base.catenaconfig import CatenaConfig
from udiron.base.dependentlist import DependentList
from udiron.base.functionconfig import C_


def check_catena_equality(base: Catena) -> Callable[[Catena], bool]:
    """Function object taking a Catena and comparing it with the base Catena.

    Args:
        base: Left-hand side of the object's equality.

    Returns:
        Function object that checks the equality when called.
    """
    class OutputFunction:
        """Internal logic for the function object in question."""

        def __init__(self, base: Catena) -> None:
            """Produces an equality check.

            Args:
                base: Left-hand side of the equality.
            """
            self.base = base

        def __call__(self, target: Catena) -> bool:
            """Runs the equality check.

            Args:
                target: Output against which to check.

            Returns:
                Whether the base and the target compare equal.
            """
            return self.base == target

        def _repr(self) -> str:
            """Holds the logic for __repr__.

            Returns:
                String representation of the equality being checked.
            """
            return f"' == {self.base}'"

        def __repr__(self) -> str:
            """Produces string representation of the equality being checked.

            Returns:
                String representation of the equality being checked.
            """
            return self._repr()
    return OutputFunction(base)


hridoy = L(301993)
hridoy_sense = hridoy[tfsli.LSid("L301993-S1")]
hridoy_catena = Catena(hridoy, langs.bn_, hridoy_sense, frozenset(), CatenaConfig.new(), DependentList([], []), "1")
hridoy_catena_2 = Catena(hridoy, langs.bn_, hridoy_sense, frozenset([c.nominative]), CatenaConfig.new(), DependentList([], []), "2")
hridoy_catena_3 = Catena(hridoy, langs.bn_, hridoy_sense, frozenset([c.nominative, c.genitive]), CatenaConfig.new(), DependentList([], []), "3")
hridoy_catena_4 = Catena(hridoy, langs.bn_, hridoy_sense, frozenset(), CatenaConfig.new(C_.number(c.singular)), DependentList([], []), "4")
hridoy_catena_5 = Catena(hridoy, langs.bn_, hridoy_sense, frozenset(), CatenaConfig.new(C_.number(c.singular) | C_.person(c.first_person)), DependentList([], []), "5")


class AddInflections(WikifunctionsFunctionTest):
    """Tests for the 'Catena.add_inflections' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = Catena.add_inflections
    cases = [
        ((hridoy_catena, {c.nominative}), {}, check_catena_equality(hridoy_catena_2)),
        ((hridoy_catena_2, [c.nominative, c.genitive]), {}, check_catena_equality(hridoy_catena_3)),
    ]
    args = (function, cases)


class RemoveInflections(WikifunctionsFunctionTest):
    """Tests for the 'Catena.remove_inflections' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = Catena.remove_inflections
    cases = [
        ((hridoy_catena, {c.nominative}), {}, check_catena_equality(hridoy_catena)),
        ((hridoy_catena_2, [c.nominative, c.genitive]), {}, check_catena_equality(hridoy_catena)),
    ]
    args = (function, cases)


class HasInflections(WikifunctionsFunctionTest):
    """Tests for the 'Catena.has_inflections' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = Catena.has_inflections
    cases = [
        ((hridoy_catena_2, {c.nominative}), {}, True),
        ((hridoy_catena_2, [c.nominative, c.genitive]), {}, False),
        ((hridoy_catena_3, frozenset([c.nominative, c.genitive])), {}, True),
    ]
    args = (function, cases)


class AddToConfig(WikifunctionsFunctionTest):
    """Tests for the 'Catena.add_to_config' function.

    Attributes:
        function: Function being tested.
        cases: Arguments and desired outputs:
        args: Combination of the above.
    """
    function = Catena.add_to_config
    cases = [
        ((hridoy_catena, CatenaConfig.new(C_.number(c.singular))), {}, check_catena_equality(hridoy_catena_4)),
        ((hridoy_catena_4, CatenaConfig.new(C_.person(c.first_person))), {}, check_catena_equality(hridoy_catena_5)),
    ]
    args = (function, cases)

# TODO: Tests for the 'expand' function.


del WikifunctionsFunctionTest
