"""Holds the base class for all tests in Udiron."""

import unittest
from typing import TYPE_CHECKING, Any, Callable, List, Mapping, Optional, Tuple

if TYPE_CHECKING:
    from tfsl import MonolingualText

FunctionType = Optional[Callable[..., Any]]
ParametersType = List[Tuple[Tuple[Any, ...], Mapping[str, Any], Any]]


def check_mt_equality(base: "MonolingualText") -> Callable[["MonolingualText"], bool]:
    """Function object taking a MonolingualText and comparing it with the base MonolingualText.

    Args:
        base: Left-hand side of the object's equality.

    Returns:
        Function object that checks the equality when called.

    Todo:
        consider just using functools.partial for this and other equivalent classes
    """
    class OutputFunction:
        """Internal logic for the function object in question."""

        def __init__(self, base: "MonolingualText") -> None:
            """Produces an equality check.

            Args:
                base: Left-hand side of the equality.
            """
            self.base = base

        def __call__(self, target: "MonolingualText") -> bool:
            """Runs the equality check.

            Args:
                target: Output against which to check.

            Returns:
                Whether the base and the target compare equal.
            """
            return self.base == target

        def _repr(self) -> str:
            """Holds the logic for __repr__.

            Returns:
                String representation of the equality being checked.
            """
            return f"' == {self.base}'"

        def __repr__(self) -> str:
            """Produces string representation of the equality being checked.

            Returns:
                String representation of the equality being checked.
            """
            return self._repr()
    return OutputFunction(base)


def check_str_equality(base: str) -> Callable[[str], bool]:
    """Function object taking a string and comparing it with the base string.

    Args:
        base: Left-hand side of the object's equality.

    Returns:
        Function object that checks the equality when called.
    """
    class OutputFunction:
        """Internal logic for the function object in question."""

        def __init__(self, base: str) -> None:
            """Produces an equality check.

            Args:
                base: Left-hand side of the equality.
            """
            self.base = base

        def __call__(self, target: str) -> bool:
            """Runs the equality check.

            Args:
                target: Output against which to check.

            Returns:
                Whether the base and the target compare equal.
            """
            return self.base == target

        def _repr(self) -> str:
            """Holds the logic for __repr__.

            Returns:
                String representation of the equality being checked.
            """
            return f"' == {self.base}'"

        def __repr__(self) -> str:
            """Produces string representation of the equality being checked.

            Returns:
                String representation of the equality being checked.
            """
            return self._repr()
    return OutputFunction(base)


class WikifunctionsFunctionTest(unittest.TestCase):
    """Base class for all tests in Udiron.

    Each subclass must provide its own tuple for 'args',
    where the first entry is the actual function to be tested,
    and the second entry is a list of lists of parameters,
    where each list of parameters consists of, in order,
    1) positional arguments,
    2) keyword arguments, and
    3) either an expected return value (against which the actual return value is equality-compared) or a function, taking the actual return value as its only argument, which if returning True passes the test.

    `test` runs subtests by taking the function and, for each parameter list,
    supplying the arguments to the function and then checking that the actual
    return value is the same as the expected return value.

    Attributes:
        args: Function being run and arguments thereto.
    """
    args: Tuple[
        FunctionType,
        ParametersType,
    ] = (None, [])

    def test(self) -> None:
        """Runs a WikifunctionsFunctionTest."""
        if "WikifunctionsFunctionTest" in self.id():
            self.skipTest("_")
        function, cases = self.args
        if len(cases) == 0:
            self.fail("No tests were defined!")
        if callable(function):
            for args, kwargs, ret in cases:
                with self.subTest(args=args, kwargs=kwargs, ret=ret):
                    if isinstance(ret, type) and issubclass(ret, Exception):
                        self.assertRaises(ret, function, *args, **kwargs)
                    elif callable(ret):
                        result = function(*args, **kwargs)
                        self.assertTrue(ret(result))
                    else:
                        self.assertEqual(function(*args, **kwargs), ret)
